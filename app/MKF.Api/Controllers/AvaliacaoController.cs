﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Data.Repository;
using MKF.Domain.Entidades.InscricaoSeletiva;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Avaliacao")]
    public class AvaliacaoController : ApiBaseController<AvaliacaoModels, Avaliacao>
    {
        AvaliacaoRepository rep;

        public AvaliacaoController()
        {
            rep = new AvaliacaoRepository();
        }

        /// <summary>
        /// Exclui todos os jurados vinculados à seletiva
        /// </summary>
        /// <param name="idModelo">Identificados da criança modelo</param>
        /// <param name="idJurado">Identificados do jurado</param>
        /// <returns>1 para sucesso e 0 para falso</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Deletar/Modelo/{idModelo}/{idJurado}")]
        [HttpDelete]
        public virtual HttpResponseMessage DeletarAvaliacaoModelo(int idModelo, int idJurado)
        {
            var ret = rep.DeleteAvaliacaoModelo(idModelo, idJurado);

            if (ret > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar evento
        /// </summary>
        /// <param name="idModelo">Id do modelo</param>
        /// <returns>Retorna lista de avaliações vinculadas a criança</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("Buscar/Modelo/{idModelo}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarAvaliacaoModelo(int idModelo)
        {
            var ret = rep.GetByAvaliacaoModelo(idModelo);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar evento
        /// </summary>
        /// <param name="idModelo">Id do modelo</param>
        /// <param name="idJurado">Id do jurado</param>
        /// <returns>Retorna lista de avaliações vinculadas a criança de acordo com jurado infomrado</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("Buscar/Jurado/{idModelo}/{idJurado}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarAvaliacaoJuradoModelo(int idModelo, int idJurado)
        {
            var ret = rep.GetByAvaliacaoJuradoModelo(idModelo, idJurado);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}
