﻿using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using System.Xml;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/PagSeguro")]
    public class PagSeguroController : ApiController
    {
        private readonly string MensagemEmail = @"<p>Parabéns!</p>
                                            <p></p>
                                            <p>Você acaba de adquirir seu passaporte para participar do MFK EXPERIENCE!</p>
                                            <p></p>
                                            <p>Aguarde contato da organização para agendamento da escolha do look ou calçado.</p>
                                            <p></p>
                                            <p>Importante ler as informações do regulamento.</p>
                                            <p></p>
                                            <p>Super beijo </p>
                                            <p></p>
                                            <p>Equipe MFK</p>
                                            <p><b>Obs: em caso de dúvidas, entre em contato no wattsapp 31 99962-0222 ou ligue para 31 3643-1312.</b></p>
                                            <p><img src='http://lcesarr-001-site2.htempurl.com/Api/Imagem/unnamed.png' /></p>";
        // POST api/Account/Logout
        [HttpGet()]
        [Route("Confirmacao/Pagamento")]
        public IHttpActionResult ConfirmacaoPagamento(string transaction_id)
        {
            if (ConsultaTransacao(transaction_id))
                return Ok();

            return BadRequest();
        }

        [Route("Notificacao")]
        public IHttpActionResult Notificacao()
        {
            string notificationCode = string.Empty;
            HttpContent requestContent = this.Request.Content;

            string jsonContent = requestContent.ReadAsStringAsync().Result;

            string[] ret = jsonContent.Split('&');

            if (ret.Count() > 1)
            {
                notificationCode = ret[0].Split('=')[1].ToString();

                if (ConsultaTransacao(notificationCode))
                    return Ok();

                return BadRequest();
            }

            //var ID = queryString.SingleOrDefault(x => x.Key == "ID").Value;
            //var Latitude = queryString.SingleOrDefault(x => x.Key == "Latitude").Value;
            return Ok();
        }

        [HttpPost()]
        [Route("Notificacao")]
        public IHttpActionResult Notificacao(string notificationCode, string notificationType)
        {
            if (!string.IsNullOrEmpty(notificationCode))
                if (ConsultaTransacao(notificationCode))
                    return Ok();

            return BadRequest();
        }

        [HttpGet()]
        [Route("Transacao/{referencia}/{enviarEmail}")]
        public IHttpActionResult Transacao(string referencia, string enviarEmail)
        {
            try
            {
                if (ConsultaTransacaoRef(referencia, enviarEmail))
                    return Ok("Success");

                return BadRequest("Error");
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        private bool ConsultaTransacao(string transaction_id)
        {
            try
            {
                RepositoryBase<Pagamento> rep = new RepositoryBase<Pagamento>();

                //uri de consulta da transação.
                string uri = string.Format(@"{0}/notifications/{1}?email={2}&token={3}", LeitorConfiguracao.UrlPagSeguro(), transaction_id, LeitorConfiguracao.EmailPagSeguro(), LeitorConfiguracao.TokenPagSeguro());

                //Classe que irá fazer a requisição GET.
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);

                //Método do webrequest.
                request.Method = "GET";

                //String que vai armazenar o xml de retorno.
                string xmlString = null;

                //Obtém resposta do servidor.
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    //Cria stream para obter retorno.
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        //Lê stream.
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            //Xml convertido para string.
                            xmlString = reader.ReadToEnd();

                            //Cria xml document para facilitar acesso ao xml.
                            XmlDocument xmlDoc = new XmlDocument();

                            //Carrega xml document através da string com XML.
                            xmlDoc.LoadXml(xmlString);

                            var codReference = xmlDoc.GetElementsByTagName("reference")[0];

                            var ret = rep.GetBy(new Pagamento { CodigoReferencia = codReference.InnerText }).FirstOrDefault();

                            //Busca elemento status do XML.
                            var status = xmlDoc.GetElementsByTagName("status")[0];

                            var type = xmlDoc.GetElementsByTagName("paymentMethod")[0].FirstChild.InnerText;

                            ret.TipoPagamento = type;
                            ret.CodigoTransacaoPG = transaction_id;

                            //Fecha reader.
                            reader.Close();

                            //Fecha stream.
                            dataStream.Close();

                            //Tipos de pagamento
                            //1   Cartão de crédito: O comprador pagou pela transação com um cartão de crédito. Neste caso, o pagamento é processado imediatamente ou no máximo em algumas horas, dependendo da sua classificação de risco.
                            //2   Boleto: O comprador optou por pagar com um boleto bancário.Ele terá que imprimir o boleto e pagá-lo na rede bancária.Este tipo de pagamento é confirmado em geral de um a dois dias após o pagamento do boleto.O prazo de vencimento do boleto é de 3 dias.
                            //3   Débito online (TEF): O comprador optou por pagar com débito online de algum dos bancos com os quais o PagSeguro está integrado.O PagSeguro irá abrir uma nova janela com o Internet Banking do banco escolhido, onde o comprador irá efetuar o pagamento.Este tipo de pagamento é confirmado normalmente em algumas horas.
                            //4   Saldo PagSeguro: O comprador possuía saldo suficiente na sua conta PagSeguro e pagou integralmente pela transação usando seu saldo.
                            //5   Oi Paggo *: o comprador paga a transação através de seu celular Oi. A confirmação do pagamento acontece em até duas horas.
                            //7   Depósito em conta: o comprador optou por fazer um depósito na conta corrente do PagSeguro.Ele precisará ir até uma agência bancária, fazer o depósito, guardar o comprovante e retornar ao PagSeguro para informar os dados do pagamento.A transação será confirmada somente após a finalização deste processo, que pode levar de 2 a 13 dias úteis.

                            //Status de pagamento
                            //1   Aguardando pagamento: o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.
                            //2   Em análise: o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.
                            //3   Paga: a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.
                            //4   Disponível: a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.
                            //5   Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.
                            //6   Devolvida: o valor da transação foi devolvido para o comprador.
                            //7   Cancelada: a transação foi cancelada sem ter sido finalizada.

                            ret.Status = status.InnerText;
                            ret.DataAlteracao = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
                            rep.Update(ret);

                            switch (status.InnerText)
                            {
                                case "3":
                                    {
                                        DateTime dt = DateTime.Now;
                                        string nomeCrianca = string.Empty;
                                        int intervalo = 0;
                                        string inscricao = string.Empty;
                                        MemoryStream regulamento = new MemoryStream();
                                        MemoryStream autorizacao = new MemoryStream();

                                        string[] emails = RecuperarEmails(ret.IdPessoa.Value, ret.IdSeletiva.Value, out dt, out nomeCrianca, out intervalo, out inscricao, out regulamento, out autorizacao);

                                        //var mensagem = @"<b>Olá {0}</b>
                                        //    <p>Você concluiu sua inscrição com sucesso.</p>
                                        //    <p>Você poderá consultar o status de sua inscrição pelo número <b>{1}</b>.</p>
                                        //    <p>Fique atenta(o) ao regulamento e não esqueça de levar sua Autorização de Imagem assinada por um dos responsáveis.</p>
                                        //    <p>Sua apresentação esta marcada para o dia <b>{2}</b> e seu horário será das <b>{3}</b> as <b>{4}</b></p>
                                        //    <p>Super beijo da equipe MFK</p>
                                        //    <img src='http://lcesarr-001-site2.htempurl.com/Api/Imagem/unnamed.png' />";

                                        //mensagem = string.Format(mensagem, nomeCrianca, inscricao, dt.ToString("dd/MM/yyyy"), dt.ToString("HH:mm"), dt.AddMinutes(intervalo).ToString("HH:mm"));

                                        return SendMail(emails, "Confirmação de inscrição", MensagemEmail, regulamento, autorizacao);
                                    }
                                default:
                                    // Pagamento ainda não realizada.
                                    return true;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool ConsultaTransacaoRef(string referencia, string enviarEmail)
        {
            RepositoryBase<Pagamento> rep = new RepositoryBase<Pagamento>();

            //uri de consulta da transação.
            string uri = string.Format(@"{0}?email={1}&token={2}&reference={3}", LeitorConfiguracao.UrlPagSeguro(), LeitorConfiguracao.EmailPagSeguro(), LeitorConfiguracao.TokenPagSeguro(), referencia);

            //Classe que irá fazer a requisição GET.
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);

            //Método do webrequest.
            request.Method = "GET";

            //String que vai armazenar o xml de retorno.
            string xmlString = null;

            //Obtém resposta do servidor.
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                //Cria stream para obter retorno.
                using (Stream dataStream = response.GetResponseStream())
                {
                    //Lê stream.
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        //Xml convertido para string.
                        xmlString = reader.ReadToEnd();

                        //Cria xml document para facilitar acesso ao xml.
                        XmlDocument xmlDoc = new XmlDocument();

                        //Carrega xml document através da string com XML.
                        xmlDoc.LoadXml(xmlString);

                        var result = xmlDoc.GetElementsByTagName("resultsInThisPage")[0];

                        if (result.InnerText == "1")
                        {
                            var codReference = xmlDoc.GetElementsByTagName("reference")[0];

                            var ret = rep.GetBy(new Pagamento { CodigoReferencia = codReference.InnerText }).FirstOrDefault();

                            //Busca elemento status do XML.
                            var status = xmlDoc.GetElementsByTagName("status")[0];

                            var type = xmlDoc.GetElementsByTagName("paymentMethod")[0].FirstChild.InnerText;

                            ret.TipoPagamento = type;
                            //ret.CodigoTransacaoPG = transaction_id;

                            //Fecha reader.
                            reader.Close();

                            //Fecha stream.
                            dataStream.Close();

                            //Tipos de pagamento
                            //1   Cartão de crédito: O comprador pagou pela transação com um cartão de crédito. Neste caso, o pagamento é processado imediatamente ou no máximo em algumas horas, dependendo da sua classificação de risco.
                            //2   Boleto: O comprador optou por pagar com um boleto bancário.Ele terá que imprimir o boleto e pagá-lo na rede bancária.Este tipo de pagamento é confirmado em geral de um a dois dias após o pagamento do boleto.O prazo de vencimento do boleto é de 3 dias.
                            //3   Débito online (TEF): O comprador optou por pagar com débito online de algum dos bancos com os quais o PagSeguro está integrado.O PagSeguro irá abrir uma nova janela com o Internet Banking do banco escolhido, onde o comprador irá efetuar o pagamento.Este tipo de pagamento é confirmado normalmente em algumas horas.
                            //4   Saldo PagSeguro: O comprador possuía saldo suficiente na sua conta PagSeguro e pagou integralmente pela transação usando seu saldo.
                            //5   Oi Paggo *: o comprador paga a transação através de seu celular Oi. A confirmação do pagamento acontece em até duas horas.
                            //7   Depósito em conta: o comprador optou por fazer um depósito na conta corrente do PagSeguro.Ele precisará ir até uma agência bancária, fazer o depósito, guardar o comprovante e retornar ao PagSeguro para informar os dados do pagamento.A transação será confirmada somente após a finalização deste processo, que pode levar de 2 a 13 dias úteis.

                            //Status de pagamento
                            //1   Aguardando pagamento: o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.
                            //2   Em análise: o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.
                            //3   Paga: a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.
                            //4   Disponível: a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.
                            //5   Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.
                            //6   Devolvida: o valor da transação foi devolvido para o comprador.
                            //7   Cancelada: a transação foi cancelada sem ter sido finalizada.
                            string stAnterior = ret.Status;
                            ret.Status = status.InnerText;
                            ret.DataAlteracao = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
                            if (stAnterior != ret.Status)
                                rep.Update(ret);

                            if (enviarEmail == "N")
                                return status.InnerText.Equals("3") || status.InnerText.Equals("4");

                            switch (status.InnerText)
                            {
                                case "3":
                                case "4":
                                    {
                                        DateTime dt = DateTime.Now;
                                        string nomeCrianca = string.Empty;
                                        int intervalo = 0;
                                        string inscricao = string.Empty;
                                        MemoryStream regulamento = new MemoryStream();
                                        MemoryStream autorizacao = new MemoryStream();

                                        string[] emails = RecuperarEmails(ret.IdPessoa.Value, ret.IdSeletiva.Value, out dt, out nomeCrianca, out intervalo, out inscricao, out regulamento, out autorizacao);

                                        //var mensagem = @"<b>Olá {0}</b>
                                        //    <p>Você concluiu sua inscrição com sucesso.</p>
                                        //    <p>Você poderá consultar o status de sua inscrição pelo número <b>{1}</b>.</p>
                                        //    <p>Fique atenta(o) ao regulamento e não esqueça de levar sua Autorização de Imagem assinada por um dos responsáveis.</p>
                                        //    <p>Sua apresentação esta marcada para o dia <b>{2}</b> e seu horário será das <b>{3}</b> as <b>{4}</b></p>
                                        //    <p>Super beijo da equipe MFK</p>
                                        //    <img src='http://lcesarr-001-site2.htempurl.com/Api/Imagem/unnamed.png' />";

                                        //mensagem = string.Format(mensagem, nomeCrianca, inscricao, dt.ToString("dd/MM/yyyy"), dt.ToString("HH:mm"), dt.AddMinutes(intervalo).ToString("HH:mm"));



                                        return SendMail(emails, "Confirmação de inscrição", MensagemEmail, regulamento, autorizacao);
                                    }
                                default:
                                    // Pagamento ainda não realizada.
                                    return false;
                            }
                        }
                        else
                        {
                            var ret = rep.GetBy(new Pagamento { CodigoReferencia = referencia }).FirstOrDefault();

                            DateTime dt = DateTime.Now;
                            string nomeCrianca = string.Empty;
                            int intervalo = 0;
                            string inscricao = string.Empty;
                            MemoryStream regulamento = new MemoryStream();
                            MemoryStream autorizacao = new MemoryStream();

                            string[] emails = RecuperarEmails(ret.IdPessoa.Value, ret.IdSeletiva.Value, out dt, out nomeCrianca, out intervalo, out inscricao, out regulamento, out autorizacao);

                            //var mensagem = @"<b>Olá {0}</b>
                            //                <p>Você concluiu sua inscrição com sucesso.</p>
                            //                <p>Você poderá consultar o status de sua inscrição pelo número <b>{1}</b>.</p>
                            //                <p>Fique atenta(o) ao regulamento e não esqueça de levar sua Autorização de Imagem assinada por um dos responsáveis.</p>
                            //                <p>Sua apresentação esta marcada para o dia <b>{2}</b> e seu horário será das <b>{3}</b> as <b>{4}</b></p>
                            //                <p>Super beijo da equipe MFK</p>
                            //                <img src='http://lcesarr-001-site2.htempurl.com/Api/Imagem/unnamed.png' />";

                            //mensagem = string.Format(mensagem, nomeCrianca, inscricao, dt.ToString("dd/MM/yyyy"), dt.ToString("HH:mm"), dt.AddMinutes(intervalo).ToString("HH:mm"));

                            return SendMail(emails, "Confirmação de inscrição", MensagemEmail, regulamento, autorizacao);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Envia um email
        /// </summary>
        /// <param name="emails">E-mail do destinatário</param>
        /// <param name="tituloMensagem">Título da mensagem</param>
        /// <param name="mensagem">Corpo da mensagem</param>
        /// <param name="regulamento">Arquivo PDF do regulamento</param>
        /// <param name="autorizacao">Arquivo PDF da autorização</param>
        public bool SendMail(string[] emails, string tituloMensagem, string mensagem, MemoryStream regulamento, MemoryStream autorizacao)
        {
            try
            {
                MailMessage objEmail = new MailMessage();
                foreach (string item in emails)
                {
                    if (!string.IsNullOrEmpty(item))
                        objEmail.To.Add(item);
                }

                objEmail.From = new MailAddress("fashionistas@minasfashionkids.com", "Mundo Fashion Kids");
                objEmail.Subject = tituloMensagem;
                objEmail.Body = mensagem;
                objEmail.CC.Add(new MailAddress(ConfigurationManager.AppSettings["EmailCopia"]));
                objEmail.IsBodyHtml = true;

                Attachment anexReg = new Attachment(regulamento, "regulamento.pdf", "application/pdf; charset=utf-8");
                objEmail.Attachments.Add(anexReg);

                Attachment anexAuto = new Attachment(autorizacao, "AutorizacaoImagem.pdf", "application/pdf; charset=iso-8859-1");
                objEmail.Attachments.Add(anexAuto);

                SmtpClient smtp = new SmtpClient();
                smtp.EnableSsl = true;
                smtp.Send(objEmail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string[] RecuperarEmails(int idPessoa, int idSeletiva, out DateTime dataApresentacao, out string nomeCrianca, out int intervalo, out string inscricao, out MemoryStream regulamento, out MemoryStream autorizacao)
        {
            dataApresentacao = DateTime.Now;
            nomeCrianca = string.Empty;
            intervalo = 0;
            inscricao = string.Empty;

            autorizacao = null;
            RepositoryBase<Seletiva> repSeletiva = new RepositoryBase<Seletiva>();
            RepositoryBase<Pessoa> repPessoa = new RepositoryBase<Pessoa>();
            RepositoryBase<Modelo> repModelo = new RepositoryBase<Modelo>();

            var retSel = repSeletiva.GetById(idSeletiva);

            regulamento = new MemoryStream(HtmlToPdfRegulamento(retSel.IdRegulamento));

            intervalo = retSel.QtdInscricaoHorario;

            var retResp1 = repModelo.GetBy(new Modelo { IdMae = idPessoa }).FirstOrDefault();

            var retResp2 = repModelo.GetBy(new Modelo { IdPai = idPessoa }).FirstOrDefault();

            string[] ret = new string[3];

            if (retResp1 == null && retResp2 != null)
                retResp1 = retResp2;

            if (retResp1 != null)
            {
                inscricao = retResp1.Inscricao;
                dataApresentacao = retResp1.HorarioDesfile.Value;
                var p1 = repPessoa.GetById(retResp1.IdCrianca.Value);

                if (p1 != null)
                {
                    nomeCrianca = p1.Nome;
                    if (!string.IsNullOrEmpty(p1.Email))
                        ret.SetValue(p1.Email, 0);
                }

                var p2 = repPessoa.GetById(retResp1.IdMae.Value);

                if (p2 != null && !string.IsNullOrEmpty(p2.Email) && !ret.Contains(p2.Email)) ret.SetValue(p2.Email, 1);

                var p3 = repPessoa.GetById(retResp1.IdPai.Value);

                if (p3 != null && !string.IsNullOrEmpty(p3.Email) && !ret.Contains(p3.Email)) ret.SetValue(p3.Email, 2);

                autorizacao = new MemoryStream(HtmlToPdfAutorizacao(p1, p2, p3));
            }

            return ret;
        }

        public byte[] HtmlToPdfRegulamento(int idRegulamento)
        {
            RepositoryBase<Regulamento> repRegulamento = new RepositoryBase<Regulamento>();
            Regulamento reg = repRegulamento.GetById(idRegulamento);


            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();

            string html = @"<html>
                            <head>
	                            <meta http-equiv='content-type' content='text/html; charset=utf-8'/>
                            </head>

                            <body>
                                <div>{0}</div>
                            </body>
                            </html>";

            return htmlToPdf.GeneratePdf(html.Replace("{0}", reg.Dados));
        }

        public byte[] HtmlToPdfAutorizacao(Pessoa crianca, Pessoa resp1, Pessoa resp2)
        {
            RepositoryBase<Regulamento> repRegulamento = new RepositoryBase<Regulamento>();
            Regulamento reg = repRegulamento.GetBy(new Regulamento { Tipo = "I" }).FirstOrDefault();

            RepositoryBase<Endereco> repEndereco = new RepositoryBase<Endereco>();

            Endereco endCrianca = repEndereco.GetBy(new Endereco { IdPessoa = crianca.Id.Value }).FirstOrDefault();
            Endereco endResp1 = repEndereco.GetBy(new Endereco { IdPessoa = resp1.Id.Value }).FirstOrDefault();

            string retorno = reg.Dados.Replace("{nomecrianca}", crianca.Nome)
                             .Replace("{nacionalidadecrianc}", crianca.Nacionalidade)
                             .Replace("{identidadecrianca}", crianca.Rg)
                             .Replace("{cpfcrianca}", crianca.Cpf);

            if (endCrianca != null)
            {
                if (string.IsNullOrEmpty(endCrianca.Complemento))
                    retorno = retorno.Replace("{ednerecoCompletoCrianca}", string.Format("{0}, {1}, {2}, {3}", endCrianca.Logradouro, endCrianca.Numero, endCrianca.Bairro, endCrianca.Cidade)).Replace("{cepCrianca}", endCrianca.Cep);
                else
                    retorno = retorno.Replace("{ednerecoCompletoCrianca}", string.Format("{0}, {1}, {2}, {3}, {4}", endCrianca.Logradouro, endCrianca.Numero, endCrianca.Complemento, endCrianca.Bairro, endCrianca.Cidade)).Replace("{cepCrianca}", endCrianca.Cep);
            }
            else
                retorno = retorno.Replace("{ednerecoCompletoCrianca}", string.Empty).Replace("{cepCrianca}", string.Empty);

            retorno = retorno.Replace("{respumnome}", resp1.Nome)
                             .Replace("{respumnasc}", resp1.Nacionalidade)
                             .Replace("{respumci}", resp1.Rg)
                             .Replace("{respumcpf}", resp1.Cpf)
                             .Replace("{emailrespum}", resp1.Email)
                             .Replace("{contatoum}", resp1.Celular);

            if (endResp1 != null)
            {
                if (string.IsNullOrEmpty(endResp1.Complemento))
                    retorno = retorno.Replace("{enderecorespum}", string.Format("{0}, {1}, {2}, {3}", endResp1.Logradouro, endResp1.Numero, endResp1.Bairro, endResp1.Cidade))
                                 .Replace("{ceprespum}", endResp1.Cep);
                else
                    retorno = retorno.Replace("{enderecorespum}", string.Format("{0}, {1}, {2}, {3}, {4}", endResp1.Logradouro, endResp1.Numero, endResp1.Complemento, endResp1.Bairro, endResp1.Cidade))
                             .Replace("{ceprespum}", endResp1.Cep);
            }
            else
                retorno = retorno.Replace("{enderecorespum}", string.Empty).Replace("{ceprespum}", string.Empty);

            if (resp2 != null)
            {
                retorno = retorno.Replace("{respdoisnome}", resp2.Nome)
                             .Replace("{respdoisnasc}", resp2.Nacionalidade)
                             .Replace("{respdoisci}", resp2.Rg)
                             .Replace("{respdocpf}", resp2.Cpf)
                             .Replace("{emailrespdo}", resp2.Email)
                             .Replace("{contatodo}", resp2.Celular);

                Endereco endResp2 = repEndereco.GetBy(new Endereco { IdPessoa = resp2.Id.Value }).FirstOrDefault();

                if (endResp2 != null)
                {
                    if (string.IsNullOrEmpty(endResp2.Complemento))
                        retorno = retorno.Replace("{enderecorespdo}", string.Format("{0}, {1}, {2}, {3}", endResp2.Logradouro, endResp2.Numero, endResp2.Bairro, endResp2.Cidade))
                                     .Replace("{ceprespdo}", endResp2.Cep);
                    else
                        retorno = retorno.Replace("{enderecorespdo}", string.Format("{0}, {1}, {2}, {3}, {4}", endResp2.Logradouro, endResp2.Numero, endResp2.Complemento, endResp2.Bairro, endResp2.Cidade))
                                     .Replace("{ceprespdo}", endResp2.Cep);
                }
            }
            else
            {
                retorno = retorno.Replace("{respdoisnome}", string.Empty)
                             .Replace("{respdoisnasc}", string.Empty)
                             .Replace("{respdoisci}", string.Empty)
                             .Replace("{respdocpf}", string.Empty)
                             .Replace("{emailrespdo}", string.Empty)
                             .Replace("{contatodo}", string.Empty);

                retorno = retorno.Replace("{enderecorespdo}", string.Empty).Replace("{ceprespdo}", string.Empty);
            }

            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();

            string html = @"<html>
                            <head>
	                            <meta http-equiv='content-type' content='text/html; charset=utf-8'/>
                            </head>

                            <body>
                                <div>{0}</div>
                            </body>
                            </html>";

            return htmlToPdf.GeneratePdf(html.Replace("{0}", retorno));
        }
    }
}
