﻿using MKF.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Inscricao")]
    public class InscricaoController : ApiController
    {
        InscricaoSeletivaRepository rep;

        public InscricaoController()
        {
            rep = new InscricaoSeletivaRepository();
        }

        /// <summary>
        /// Buscar dados da inscrição da seletiva
        /// </summary>
        /// <param name="idCrianca">Código identificador da criança</param>
        /// <returns>Dados da inscrição da criança</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Seletiva/{idCrianca}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarInscSeletiva(int idCrianca)
        {
            var ret = rep.GetByModeloInscricaoSeletiva(idCrianca);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar dados da inscrição da seletiva
        /// </summary>
        /// <param name="nomeCrianca"></param>
        /// <param name="nomeResp1"></param>
        /// <param name="nomeResp2"></param>
        /// <param name="idSeletiva"></param>
        /// <param name="inscricao"></param>
        /// <param name="idadeMin"></param>
        /// <param name="idadeMax"></param>
        /// <param name="tamanho"></param>
        /// <param name="altura"></param>
        /// <param name="peso"></param>
        /// <param name="calcado"></param>
        /// <param name="corCabelo"></param>
        /// <param name="sexo"></param>
        /// <returns>Dados da inscrição da criança</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Seletiva/Avaliadas/{nomeCrianca}/{nomeResp1}/{nomeResp2}/{idSeletiva}/{inscricao}/{idadeMin}/{idadeMax}/{tamanho}/{altura}/{peso}/{calcado}/{corCabelo}/{sexo}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarModAvaliadasSeletiva(string nomeCrianca, string nomeResp1, string nomeResp2, int idSeletiva, string inscricao, int idadeMin, int idadeMax, string tamanho, double altura, double peso, int calcado, string corCabelo, string sexo)
        {
            var ret = rep.GetByModeloAvaliadaSeletiva(nomeCrianca, nomeResp1, nomeResp2, idSeletiva, inscricao, idadeMin, idadeMax, tamanho, altura, peso, calcado, corCabelo, sexo);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}

