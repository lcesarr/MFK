﻿using MKF.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Relatorio")]
    public class RelatorioController : ApiController
    {
        RelatorioRepository rep;

        public RelatorioController()
        {
            rep = new RelatorioRepository();
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/InsSeletiva/{nome}/{inscricao}/{evento}/{seletiva}/{status}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarInscSeletiva(string nome, string inscricao, int evento, int seletiva, string status)
        {
            var ret = rep.GetByInscSeletiva(nome, inscricao, evento, seletiva, status);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
        
        /// <summary>
        /// Retorna uma listagem de inscrições de acordo com os filtors informados.
        /// </summary>
        /// <param name="nomeCrianca">Nome da criança</param>
        /// <param name="nomeResp1">Nome do responsável um</param>
        /// <param name="nomeResp2">Nome do responsável dois</param>
        /// <param name="idSeletiva">Id da seletiva</param>
        /// <param name="inscricao">Numero da inscrição</param>
        /// <param name="idadeMin">Idade minima</param>
        /// <param name="idadeMax">Idade máxima</param>
        /// <param name="codigoReferencia">Código de referencia</param>
        /// <param name="statusPagamento">Status do pagamento</param>
        /// <param name="tipoPagamento">Tipo de pagamento</param>
        /// <param name="tamanho">Tamanho do manequim</param>
        /// <param name="altura">Altura da criança</param>
        /// <param name="peso">Peso da criança</param>
        /// <param name="calcado">Calçado da criança</param>
        /// <param name="corCabelo">Cor do cabelo da criança</param>
        /// <param name="dataSeltiva">Data de apresentação na seletiva</param>
        /// <param name="sexo">Sexo da criança</param>
        /// <returns></returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/InscricaoSeletiva/{nomeCrianca}/{nomeResp1}/{nomeResp2}/{idSeletiva}/{inscricao}/{idadeMin}/{idadeMax}/{codigoReferencia}/{statusPagamento}/{tipoPagamento}/{tamanho}/{altura}/{peso}/{calcado}/{corCabelo}/{dataSeltiva}/{sexo}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarInscricaoSeletiva(string nomeCrianca, string nomeResp1, string nomeResp2, int idSeletiva, string inscricao, int idadeMin, int idadeMax, string codigoReferencia, string statusPagamento, string tipoPagamento, string tamanho, double altura, double peso, int calcado, string corCabelo, string dataSeltiva, string sexo)
        {
            var ret = rep.GetByInscricaoSeletiva(nomeCrianca, nomeResp1, nomeResp2, idSeletiva, inscricao, idadeMin, idadeMax, codigoReferencia, statusPagamento, tipoPagamento, tamanho, altura, peso, calcado, corCabelo, dataSeltiva, sexo);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/MapaSeletiva/{idSeletiva}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarMapaSeletiva(int idSeletiva)
        {
            var ret = rep.GetByMapaSeletiva(idSeletiva);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/AprovadosSeletiva/{idSeletiva}/{idademin}/{idademax}")]
        [HttpGet]
        public virtual HttpResponseMessage AprovadosSeletiva(int idSeletiva, int idademin, int idademax)
        {
            var ret = rep.GetByAprovadosSeletiva(idSeletiva, idademin, idademax);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <param name="idSeletiva">Identificador da seletiva</param>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/AprovadosSeletivaPdf/{idSeletiva}/{idademin}/{idademax}")]
        [HttpGet]
        public virtual HttpResponseMessage AprovadosSeletivaPdf(int idSeletiva, int idademin, int idademax)
        {
            var ret = rep.GetByAprovadosSeletivaPdf(idSeletiva, idademin, idademax);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
        
    }
}
