﻿using AutoMapper;
using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Data.Repository;
using MKF.Domain.Entidades.Comum;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Lojista")]
    public class LojistaController : ApiBaseController<LojistaModels, Empresa>
    {
        EmpresaRepository rep;

        public LojistaController()
        {
            rep = new EmpresaRepository();
        }

        /// <summary>
        /// Buscar lojistas
        /// </summary>
        /// <param name="id">Id do lojistas</param>
        /// <returns>Retorna o lojista de acordo com seu Identificadoo</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarLojista/{Id}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarLojista(int id)
        {
            var ret = rep.GetByUnionId(id);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar lojistas
        /// </summary>
        /// <returns>Retorna uma lista de lojistas</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarTodos")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarTodosLojista()
        {
            var ret = rep.GetByUnionAll();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Por")]
        [HttpPost]
        public virtual HttpResponseMessage BuscarPor(LojistaModels model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<LojistaModels, Empresa>(model);
            var ret = rep.GetBy(obj);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}