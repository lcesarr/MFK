﻿using AutoMapper;
using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Data.Repository;
using MKF.Domain.Entidades;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Usuario")]
    public class UsuarioController : ApiBaseController<UsuarioModels, Usuario>
    {
        UsuarioRepository rep;

        public UsuarioController()
        {
            rep = new UsuarioRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        public override IHttpActionResult Incluir(UsuarioModels model)
        {
            return base.Incluir(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        public override IHttpActionResult Alterar(UsuarioModels model)
        {
            return base.Alterar(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        public override Task<IHttpActionResult> Excluir(int id)
        {
            return base.Excluir(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        public override HttpResponseMessage Buscar(int id)
        {
            return base.Buscar(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        public override HttpResponseMessage BuscarTodos()
        {
            return base.BuscarTodos();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Por")]
        [HttpPost]
        public virtual HttpResponseMessage BuscarPor(UsuarioModels model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<UsuarioModels, Usuario>(model);
            var ret = rep.GetBy(obj);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar usuários
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Retorna uma lista de usuários</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarUsuario/{Id}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarUsuario(int id)
        {
            var ret = rep.GetByUnionId(id);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar usuários
        /// </summary>
        /// <returns>Retorna uma lista de usuários</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarTodos")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarTodosUsuario()
        {
            var ret = rep.GetByUnionAll();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar usuários
        /// </summary>
        /// <param name="nome">Nome do usuário</param>
        /// <param name="cpf">Cpf do usuário</param>
        /// <param name="user">Login do usuário</param>
        /// <returns>Retorna uma lista de usuários</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarUsuario/{Nome}/{Cpf}/{User}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarUsuarioPor(string nome, string cpf, string user)
        {
            var ret = rep.GetUserBy(nome, cpf, user);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar usuário de jurados
        /// </summary>
        /// <param name="id">Id do jurado</param>
        /// <returns>Retorna uma lista de usuários</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarUsuarioJurados/{id}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarUsuarioJurados(int id)
        {
            var ret = rep.GetUserJurados(id);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

    }
}