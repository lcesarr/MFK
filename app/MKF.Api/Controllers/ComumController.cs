﻿using MKF.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Comum")]
    public class ComumController : ApiController
    {
        ComumRepository rep;

        public ComumController()
        {
            rep = new ComumRepository();
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/QtdInscricaoHora/{idSeletiva}/{idEvento}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarInscricaoHora(int idSeletiva, int idEvento)
        {
            var ret = rep.GetByInscricaoHora(idSeletiva, idEvento);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}
