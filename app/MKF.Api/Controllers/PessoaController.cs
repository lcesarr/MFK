﻿using AutoMapper;
using MKF.Api.Controllers.Base;
using MKF.Api.Models.Comum;
using MKF.Data.Repository;
using MKF.Domain.Entidades.Comum;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Pessoa")]
    public class PessoaController : ApiBaseController<PessoaModels, Pessoa>
    {
        PessoaRepository rep;

        public PessoaController()
        {
            rep = new PessoaRepository();
        }

        /// <summary>
        /// Incluir vinculo de pessoas com seletiva.
        /// </summary>
        /// <param name="idSeletiva">Identificador da seletiva</param>
        /// <param name="idPessoa">Identificador da pessoa</param>
        /// <returns>True para sucesso e False para erro</returns>
        [AllowAnonymous]
        [Route("Incluir/Pessoa")]
        [HttpPost]
        public virtual IHttpActionResult IncluirPessoa(PessoaModels model)
        {
            var obj = Mapper.Map<PessoaModels, Pessoa>(model);
            var ret = rep.Insert(obj);

            if (ret > 0)
                return Ok(ret);
            else
                return BadRequest("Erro ao salvar registro");
        }

        /// <summary>
        /// Incluir vinculo de pessoas com seletiva.
        /// </summary>
        /// <param name="idSeletiva">Identificador da seletiva</param>
        /// <param name="idPessoa">Identificador da pessoa</param>
        /// <returns>True para sucesso e False para erro</returns>
        [AllowAnonymous]
        [Route("Incluir/SeletivaPessoa")]
        [HttpPost]
        public virtual IHttpActionResult IncluirRelacionamento(int idSeletiva, int idPessoa)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var ret = rep.IncluirRelSeletivaPessoa(idSeletiva, idPessoa);

            if (ret)
                return Ok(ret);
            else
                return BadRequest("Erro ao salvar registro");
        }

        /// <summary>
        /// Deletar todos os vinculos de pessoas com seletiva.
        /// </summary>
        /// <param name="idSeletiva">Identificador da seletiva</param>
        /// <returns>True para sucesso e False para erro</returns>
        [AllowAnonymous]
        [Route("Delete/SeletivaPessoa")]
        [HttpDelete]
        public virtual IHttpActionResult DeletarRelacionamento(int idSeletiva)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var ret = rep.DeleteRelSeletivaPessoa(idSeletiva);

            if (ret)
                return Ok(ret);
            else
                return BadRequest("Erro ao salvar registro");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Por")]
        [HttpPost]
        public virtual HttpResponseMessage BuscarPor(PessoaModels model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<PessoaModels, Pessoa>(model);
            var ret = rep.GetBy(obj);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar dados da inscrição da seletiva
        /// </summary>
        /// <param name="id">Código identificador da criança</param>
        /// <returns>Dados da inscrição da criança</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Union/{id}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarInscSeletiva(int id)
        {
            var ret = rep.GetByUnionId(id);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}