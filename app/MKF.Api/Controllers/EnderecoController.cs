﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models.Comum;
using MKF.Domain.Entidades.Comum;
using System.Web.Mvc;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Endereco")]
    public class EnderecoController : ApiBaseController<EnderecoModels, Endereco>
    {

    }
}