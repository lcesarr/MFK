﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Data.Repository;
using MKF.Domain.Entidades;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Evento")]
    public class EventoController : ApiBaseController<EventoModels, Evento>
    {
        EventoRepository rep;

        public EventoController()
        {
            rep = new EventoRepository();
        }

        /// <summary>
        /// Buscar evento
        /// </summary>
        /// <param name="id">Id do evento</param>
        /// <returns>Retorna o lojista de acordo com seu Identificadoo</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarEvento/{Id}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarEvento(int id)
        {
            var ret = rep.GetByUnionId(id);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar seletivas de um evento
        /// </summary>
        /// <returns>Retorna uma lista de eventos e suas seletivas</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarEventoSeletiva")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarEventoSeletiva()
        {
            var ret = rep.GetByEventosSeletiva();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}