﻿using MKF.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/DashBoard")]
    public class DashBoardController : ApiController
    {
        DashBoardRepository rep;

        public DashBoardController()
        {
            rep = new DashBoardRepository();
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/DashSeletiva")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarDashSeletiva()
        {
            var ret = rep.GetByDashSeletiva();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Retorna um dashboard resumido da inscrição
        /// </summary>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/DashSeletivaTipoPagamento")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarDashSeletivaTpPg()
        {
            var ret = rep.GetByDashSeletivaTpPg();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}
