﻿using AutoMapper;
using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Api.Models.Comum;
using MKF.Data.Repository;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Modelo")]
    public class ModeloController : ApiBaseController<ModeloModels, Modelo>
    {
        ModeloRepository rep;

        public ModeloController()
        {
            rep = new ModeloRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Por")]
        [HttpPost]
        public virtual HttpResponseMessage BuscarPor(ModeloModels model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<ModeloModels, Modelo>(model);
            var ret = rep.GetBy(obj);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Retorna uma listagem de inscrições de acordo com os filtors informados.
        /// </summary>
        /// <param name="nomeCrianca">Nome da criança</param>
        /// <param name="nomeResp1">Nome do responsável um</param>
        /// <param name="nomeResp2">Nome do responsável dois</param>
        /// <param name="idSeletiva">Id da seletiva</param>
        /// <param name="dataSeltiva">Data de apresentação na seletiva</param>
        /// <param name="idJurados">Id do jurado avaliador</param>
        /// <returns></returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Seletiva/{nomeCrianca}/{nomeResp1}/{nomeResp2}/{idSeletiva}/{dataSeltiva}/{idJurados}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarInscricaoSeletiva(string nomeCrianca, string nomeResp1, string nomeResp2, int idSeletiva, string dataSeltiva, int idJurados)
        {
            var ret = rep.GetByModeloSeletiva(nomeCrianca, nomeResp1, nomeResp2, idSeletiva, dataSeltiva, idJurados);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Retorna uma listagem de inscrições de acordo com os filtors informados.
        /// </summary>
        /// <param name="idModelo">Nome da criança</param>
        /// <param name="status">Nome do responsável um</param>
        /// <returns></returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("AlterarStatus")]
        [HttpPut]
        public virtual HttpResponseMessage BuscarInscricaoSeletiva(int idModelo, int status)
        {
            var ret = rep.AtualizaStatus(idModelo, status);

            if (ret)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }


    }
}