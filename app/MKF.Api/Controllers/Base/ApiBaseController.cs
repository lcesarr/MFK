﻿using AutoMapper;
using MKF.Data.Repository.Base;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace MKF.Api.Controllers.Base
{
    public abstract class ApiBaseController<TModel, TEntity> : ApiController where TEntity : class
    {
        RepositoryBase<TEntity> rep;

        public ApiBaseController()
        {
            rep = new RepositoryBase<TEntity>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Incluir")]
        [HttpPost]
        public virtual IHttpActionResult Incluir(TModel model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<TModel, TEntity>(model);
            var ret = rep.Insert(obj);

            if (ret > 0)
                return Ok(ret);
            else
                return BadRequest("Erro ao salvar registro");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Alterar")]
        [HttpPut]
        public virtual IHttpActionResult Alterar(TModel model)
        {
            var obj = Mapper.Map<TModel, TEntity>(model);
            var ret = rep.Update(obj);

            if (ret)
                return Ok(ret);
            else
                return BadRequest("Erro ao salvar registro");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Delete")]
        [HttpDelete]
        public virtual async Task<IHttpActionResult> Excluir(int id)
        {
            var ret = rep.Delete(id);

            if (ret)
                return Ok();
            else
                return BadRequest("Erro ao deletar registro");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Id")]
        [HttpGet]
        public virtual HttpResponseMessage Buscar(int id)
        {
            var ret = rep.GetById(id);
            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/All")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarTodos()
        {
            var ret = rep.GetAll();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}