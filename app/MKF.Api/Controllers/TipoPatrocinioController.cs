﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Domain.Entidades;
using System.Web.Mvc;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/TipoPatrocinio")]
    public class TipoPatrocinioController : ApiBaseController<TipoPatrocinioModels, TipoPatrocinio>
    {

    }
}