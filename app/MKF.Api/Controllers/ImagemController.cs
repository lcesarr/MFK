﻿using AutoMapper;
using MKF.Api.Controllers.Base;
using MKF.Api.Models.Comum;
using MKF.Data.Repository;
using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Imagem")]
    public class ImagemController : ApiBaseController<ImagemModels, Imagem>
    {
        ImagemRepository rep;

        public ImagemController()
        {
            rep = new ImagemRepository();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Por")]
        [HttpPost]
        public virtual HttpResponseMessage BuscarPor(ImagemModels model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<ImagemModels, Imagem>(model);
            var ret = rep.GetBy(obj);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}
