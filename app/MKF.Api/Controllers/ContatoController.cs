﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models.Comum;
using MKF.Domain.Entidades.Comum;
using System.Web.Mvc;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Contato")]
    public class ContatoController : ApiBaseController<ContatoModels, Contato>
    {

    }
}