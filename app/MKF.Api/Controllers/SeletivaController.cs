﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Data.Repository;
using MKF.Domain.Entidades;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/Seletiva")]
    public class SeletivaController : ApiBaseController<SeletivaModels, Seletiva>
    {
        SeletivaRepository rep;

        /// <summary>
        /// 
        /// </summary>
        public SeletivaController()
        {
            rep = new SeletivaRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override IHttpActionResult Incluir(SeletivaModels model)
        {
            return base.Incluir(model);
        }

        /// <summary>
        /// Buscar usuários
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Retorna uma lista de usuários</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarSeletiva/{Id}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarSeletiva(int id)
        {
            var ret = rep.GetByUnionId(id);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Buscar usuários
        /// </summary>
        /// <returns>Retorna uma lista de usuários</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [Route("BuscarTodos")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarTodosSeletiva()
        {
            var ret = rep.GetByUnionAll();

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}