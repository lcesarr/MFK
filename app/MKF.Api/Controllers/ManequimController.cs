﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Domain.Entidades.Comum;
using System.Web.Mvc;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Manequim")]
    public class ManequimController : ApiBaseController<ManequimModels, Manequim>
    {

    }
}