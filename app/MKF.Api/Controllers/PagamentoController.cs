﻿using AutoMapper;
using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Api.Models.Comum;
using MKF.Data.Repository;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Pagamento")]
    public class PagamentoController : ApiBaseController<PagamentoModels, Pagamento>
    {
        PagamentoRepository rep;

        public PagamentoController()
        {
            rep = new PagamentoRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/Por")]
        [HttpPost]
        public virtual HttpResponseMessage BuscarPor(PagamentoModels model)
        {
            //IMPLEMENTATION OF CODE GOES HERE!!
            var obj = Mapper.Map<PagamentoModels, Pagamento>(model);
            var ret = rep.GetBy(obj);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }
    }
}