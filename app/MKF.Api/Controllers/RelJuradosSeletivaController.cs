﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Data.Repository;
using MKF.Domain.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/JuradosSeletiva")]
    public class RelJuradosSeletivaController : ApiBaseController<RelJuradosSeletivaModels, RelJuradosSeletiva>
    {
        RelJuradosSeletivaRepository rep;

        public RelJuradosSeletivaController()
        {
            rep = new RelJuradosSeletivaRepository();
        }

        /// <summary>
        /// Retorna todos os jurados vinculados a seletiva
        /// </summary>
        /// <param name="idSeletiva">Identificados da seletiva</param>
        /// <returns>Lista de somatórias de inscrição por status</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Buscar/{idSeletiva}")]
        [HttpGet]
        public virtual HttpResponseMessage BuscarJuradosSeletiva(int idSeletiva)
        {
            var ret = rep.GetByJuradosSeletiva(idSeletiva);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Exclui todos os jurados vinculados à seletiva
        /// </summary>
        /// <param name="idSeletiva">Identificados da seletiva</param>
        /// <returns>1 para sucesso e 0 para falso</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Deletar/{idSeletiva}")]
        [HttpDelete]
        public virtual HttpResponseMessage DeletarJursadosSeletiva(int idSeletiva)
        {
            var ret = rep.DeleteJuradosSeletiva(idSeletiva);

            if (ret > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Litas todas as seletivas vinculados ao jurado
        /// </summary>
        /// <param name="idJurados">Identificados do jurados</param>
        /// <returns>1 para sucesso e 0 para falso</returns>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error</response>
        [AllowAnonymous]
        [Route("Seletiva/{idJurados}")]
        [HttpGet]
        public virtual HttpResponseMessage GetBySeletivaJurados(int idJurados)
        {
            var ret = rep.GetBySeletivaJurados(idJurados);

            if (ret != null)
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, ret);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
        }        
    }
}
