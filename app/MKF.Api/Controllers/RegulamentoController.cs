﻿using MKF.Api.Controllers.Base;
using MKF.Api.Models;
using MKF.Domain.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKF.Api.Controllers
{
    [RoutePrefix("api/Regulamento")]
    public class RegulamentoController : ApiBaseController<RegulamentoModels, Regulamento>
    {

    }
}