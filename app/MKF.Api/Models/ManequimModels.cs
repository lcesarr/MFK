﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    public class ManequimModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdModelo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Tamanho { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Altura { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Peso { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Calcado { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CorPele { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CorOlho { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CorCabelo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Agenciado { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Agencia { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Resumo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }

        public ModeloModels Crianca { get; set; }
    }
}