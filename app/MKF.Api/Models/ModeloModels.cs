﻿using MKF.Api.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ModeloModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdEmpresa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NomeArtistico { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdCrianca { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdMae { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdPai { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PessoaModels Crianca { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PessoaModels Responsavel1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        public PessoaModels Responsavel2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? HorarioDesfile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Inscricao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? Avaliacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? MediaAvaliacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ManequimModels Manequim { get; set; }
    }
}