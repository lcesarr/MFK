﻿using MKF.Api.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SeletivaModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdEvento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdEndereco { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdRegulamento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataFim { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int QtdInscricaoHorario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UrlImagem { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IntervaloApresentacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EnderecoModels Local { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PessoaModels Jurados { get; set; }
    }
}