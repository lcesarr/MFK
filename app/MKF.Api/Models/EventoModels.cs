﻿using System;

namespace MKF.Api.Models
{
    /// <summary>
    /// Evento
    /// </summary>
    public class EventoModels
    {
        /// <summary>
        /// Identificador do evento
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nome do Evento
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Edição do evento
        /// </summary>
        public string Edicao { get; set; }

        /// <summary>
        /// Descrição do evento
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Data de inicio do evento
        /// </summary>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Data fim do evento
        /// </summary>
        public DateTime DataFim { get; set; }

        /// <summary>
        /// Status do evento
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Telefone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Celular { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Facebook { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Twitter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Instagram { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string URLImagem { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdEndereco { get; set; }
    }
}