﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    /// <summary>
    /// Usuário
    /// </summary>
    public class UsuarioModels
    {
        /// <summary>
        /// Identificador do usuário
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Id da pessoa
        /// </summary>
        public int? IdPessoa { get; set; }

        /// <summary>
        /// Usuário
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Senha
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// Tipo Acesso
        /// </summary>
        public string TipoAcesso { get; set; }

        /// <summary>
        /// Tipo Acesso
        /// </summary>
        public string CodigoAcesso { get; set; }
    }
}