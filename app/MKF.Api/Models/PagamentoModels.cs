﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PagamentoModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdPessoa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdEvento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdSeletiva { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? ValorPagamento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TipoPagamento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataPagamento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DataAlteracao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CodigoTransacaoPG { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CodigoReferencia { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Inscricao { get; set; }
    }
}