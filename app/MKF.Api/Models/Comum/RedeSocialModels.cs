﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models.Comum
{
    /// <summary>
    /// Rede Social
    /// </summary>
    public class RedeSocialModels
    {
        /// <summary>
        /// Identificador da Rede Social
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Tipo de Rede Social
        /// </summary>
        public int Tipo { get; set; }

        /// <summary>
        /// Url da Rede Social
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdEmpresa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdPessoa { get; set; }
    }
}