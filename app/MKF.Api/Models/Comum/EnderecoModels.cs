﻿namespace MKF.Api.Models.Comum
{
    /// <summary>
    /// 
    /// </summary>
    public class EnderecoModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdPessoa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdEmpresa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Logradouro { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Numero { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Complemento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Bairro { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cidade { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cep { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Estado { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LocalSeletiva { get; set; }
    }
}