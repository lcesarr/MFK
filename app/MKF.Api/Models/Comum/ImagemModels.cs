﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models.Comum
{
    /// <summary>
    /// 
    /// </summary>
    public class ImagemModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tipo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? Altura { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? Largura { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string URL { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? IdPessoa { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? IdEmpresa { get; set; }
    }
}