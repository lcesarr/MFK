﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models.Comum
{
    /// <summary>
    /// Contato
    /// </summary>
    public class ContatoModels
    {
        /// <summary>
        /// Identificador do contato
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id da pessoa
        /// </summary>
        public int IdPessoa { get; set; }

        /// <summary>
        /// Id da empresa
        /// </summary>
        public int IdEmpresa { get; set; }

        /// <summary>
        /// Tipo de contato
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// Telefone do contato
        /// </summary>
        public string Dado { get; set; }
    }
}