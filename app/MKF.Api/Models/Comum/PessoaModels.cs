﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models.Comum
{
    public class PessoaModels
    {
        /// <summary>
        /// Identificador da pessoa
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Nome da pessoa
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Cpf da pessoa
        /// </summary>
        public string Cpf { get; set; }

        /// <summary>
        /// RG da pessoa
        /// </summary>
        public string Rg { get; set; }

        /// <summary>
        /// Certidao da pessoa
        /// </summary>
        public string Certidao { get; set; }

        /// <summary>
        /// Sexo da pessoa
        /// </summary>
        public string Sexo { get; set; }

        /// <summary>
        /// Parentesco da pessoa
        /// </summary>
        public string Parentesco { get; set; }

        /// <summary>
        /// Data de nascimento
        /// </summary>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IdEmpresa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Celular { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
    }
}