﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RegulamentoModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Dados { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Tipo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Status { get; set; }
    }
}