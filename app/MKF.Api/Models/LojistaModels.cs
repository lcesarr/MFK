﻿using MKF.Api.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class LojistaModels
    {
        /// <summary>
        /// Identificador do lojista
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// Cnpj do lojista
        /// </summary>
        public string CNPJ { get; set; }
        /// <summary>
        /// Razão social do lojista
        /// </summary>
        public string RazaoSocial { get; set; }
        /// <summary>
        /// Nome Fantasia do lojista
        /// </summary>
        public string NomeFantasia { get; set; }
        /// <summary>
        /// Tipo da empresa
        /// </summary>
        public string Tipo { get; set; }
        /// <summary>
        /// Ramos de atividade
        /// </summary>
        public string RamoAtividade { get; set; }
        /// <summary>
        /// Resumo da empresa
        /// </summary>
        public string Resumo { get; set; }

        /// <summary>
        /// Observação
        /// </summary>
        public string Observacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual EnderecoModels Endereco { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual List<ContatoModels> Contatos { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual PessoaModels Responsavel { get; set; }
        //public virtual List<Imagem> Imagens { get; set; }
        //public virtual List<RedeSocial> RedesSociais { get; set; }
        //public virtual List<Modelo> Criancas { get; set; }
    }
}