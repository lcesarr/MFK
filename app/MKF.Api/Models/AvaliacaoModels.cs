﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.Models
{
    public class AvaliacaoModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdModelo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Item { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Nota { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataAvaliacao { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdJurado { get; set; }
    }
}