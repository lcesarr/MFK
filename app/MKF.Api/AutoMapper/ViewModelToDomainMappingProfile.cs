﻿using AutoMapper;
using MKF.Api.Models;
using MKF.Api.Models.Comum;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using MKF.Domain.Entidades.InscricaoSeletiva;

namespace MKF.Api.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<Empresa, LojistaModels>();
            CreateMap<Evento, EventoModels>();
            CreateMap<Endereco, EnderecoModels>();
            CreateMap<Usuario, UsuarioModels>();
            CreateMap<Contato, ContatoModels>();
            CreateMap<Pessoa, PessoaModels>();
            CreateMap<TipoParceria, TipoParceriaModels>();
            CreateMap<TipoPatrocinio, TipoPatrocinioModels>();
            CreateMap<Regulamento, RegulamentoModels>();
            CreateMap<Seletiva, SeletivaModels>();
            CreateMap<Pagamento, PagamentoModels>();
            CreateMap<Modelo, ModeloModels>();
            CreateMap<Manequim, ManequimModels>();
            CreateMap<Imagem, ImagemModels>();
            CreateMap<Avaliacao, AvaliacaoModels>();
        }
    }
}