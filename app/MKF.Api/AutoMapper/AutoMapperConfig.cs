﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Api.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<bool, string>().ConvertUsing(new BoolToStringTypeConverter());
                x.CreateMap<string, bool>().ConvertUsing(new StringToBooleanTypeConverter());
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDomainMappingProfile>();
            });
        }
    }

    public class BoolToStringTypeConverter : ITypeConverter<bool, string>
    {
        public string Convert(bool source, string destination, ResolutionContext context)
        {
            if (source)
                return "1";
            return "0";
        }
    }

    public class StringToBooleanTypeConverter : ITypeConverter<string, bool>
    {
        public bool Convert(string source, bool destination, ResolutionContext context)
        {
            if (string.IsNullOrEmpty(source) || source.Equals("0"))
                return false;
            return true;
        }
    }
}