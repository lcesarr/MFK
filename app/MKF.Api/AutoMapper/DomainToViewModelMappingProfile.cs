﻿using AutoMapper;
using MKF.Api.Models;
using MKF.Api.Models.Comum;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using MKF.Domain.Entidades.InscricaoSeletiva;

namespace MKF.Api.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<LojistaModels, Empresa>();
            CreateMap<EventoModels, Evento>();
            CreateMap<EnderecoModels, Endereco>();
            CreateMap<UsuarioModels, Usuario>();
            CreateMap<ContatoModels, Contato>();
            CreateMap<PessoaModels, Pessoa>();
            CreateMap<TipoParceriaModels, TipoParceria>();
            CreateMap<TipoPatrocinioModels, TipoPatrocinio>();
            CreateMap<RegulamentoModels, Regulamento>();
            CreateMap<SeletivaModels, Seletiva>();
            CreateMap<PagamentoModels, Pagamento>();
            CreateMap<ModeloModels, Modelo>();
            CreateMap<ManequimModels, Manequim>();
            CreateMap<ImagemModels, Imagem>();
            CreateMap<AvaliacaoModels, Avaliacao>();
        }
    }
}