using System.Web.Http;
using WebActivatorEx;
using MKF.Api;
using Swashbuckle.Application;
using System.Linq;
using MKF.Api.App_Start;
using MKF.Api.Swagger;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace MKF.Api
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.Schemes(new[] { "http", "https" });

                    c.IncludeXmlComments(GetXmlCommentsPath());

                    c.SingleApiVersion("v1", "MFK API")
                            .Description("Uma amostra de API para testes e prototipagem de recursos")
                            .TermsOfService("Alguns termos")
                            .Contact(cc => cc
                                .Name("Contato")
                                .Url("http://tempuri.org/contact")
                                .Email("some.contact@tempuri.org"))
                            .License(lc => lc
                                .Name("Licen�a")
                                .Url("http://tempuri.org/license"));
                    c.OAuth2("oauth2")
                            .Description("OAuth2 Implicit Grant")
                            .Flow("implicit")
                            .AuthorizationUrl("http://petstore.swagger.io/oauth/dialog")
                            //.TokenUrl("https://tempuri.org/token")
                            .Scopes(scopes =>
                            {
                                scopes.Add("read", "Read access to protected resources");
                                scopes.Add("write", "Write access to protected resources");
                            });
                    c.IgnoreObsoleteProperties();
                    c.DescribeAllEnumsAsStrings();
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    c.CustomProvider((defaultProvider) => new CachingSwaggerProvider(defaultProvider));
                    c.OperationFilter<AddRequiredHeaderParameter>();
                })
                .EnableSwaggerUi(c =>
                {
                    c.DocExpansion(DocExpansion.List);
                });

            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
        }

        protected static string GetXmlCommentsPath()
        {
            return System.String.Format(@"{0}\App_Data\Swagger.XML", System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
