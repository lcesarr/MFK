﻿$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
    _title: function (title) {
        var $title = this.options.title || '&nbsp;'
        if (("title_html" in this.options) && this.options.title_html == true)
            title.html($title);
        else title.text($title);
    }
}));

$(".excluir").on('click', function (e) {
    e.preventDefault();
    debugger;
    $("#itemRemover").val($(this)[0].id);
    $("#urlDelete").val($(this).data("url"));
    $("#controller").val($(this).data("control"));

    $("#dialog-confirm").removeClass('hide').dialog({
        resizable: false,
        width: '320',
        modal: true,
        title: "<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Excluir item?</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Sim",
                "class": "btn btn-danger btn-minier",
                click: function () {
                    $(this).dialog("close");
                    $("#loading").show();
                    $.ajax({
                        type: "post",
                        url: $("#urlDelete").val(),
                        ajaxasync: true,
                        data: { id: $('#itemRemover').val() },
                        success: function (e) {
                            window.location.reload();
                            $("#loading").hide();
                        },
                        error: function (data) {
                            $("#loading").hide();
                            alert(data);
                        }
                    });
                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Não",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                    $("#loading").hide();
                }
            }
        ]
    });
});