﻿function ValidarAceiteRegulamento() {
    if ($("#id-disable-check")[0].checked) { return true; }
    else {
        $(".my-body").html("<div>Favor ler e aceitar o regulamento.</div>")
        $("#my-modal").modal('show');
        return false;
    }
};

function ValidarDadosCrianca() {
    var msg = ValDadosCrianca();
    if (msg == '') { return true; }
    else {
        $(".my-body").html(msg)
        $("#my-modal").modal('show');
        return false;
    }
};

function ValidarResponsaveis() {
    var msg = ValDadosResponsavel();
    if (msg == '') { return true; }
    else {
        $(".my-body").html(msg)
        $("#my-modal").modal('show');
        return false;
    }
};

function ValidarManequim() {
    var msg = ValDadosManequim();
    if (msg == '') {
        var endCrianca = "";

        if ($("#Crianca_Endereco_Complemento").val() == "")
            endCrianca = $("#Crianca_Endereco_Logradouro").val() + ", " + $("#Crianca_Endereco_Numero").val() + " - " + $("#Crianca_Endereco_Bairro").val() + " - " + $("#Crianca_Endereco_Cidade").val() + " - " + $("#Crianca_Endereco_Estado").val();
        else
            endCrianca = $("#Crianca_Endereco_Logradouro").val() + ", " + $("#Crianca_Endereco_Numero").val() + " - " + " - " + $("#Crianca_Endereco_Complemento").val() + " - " + $("#Crianca_Endereco_Bairro").val() + " - " + $("#Crianca_Endereco_Cidade").val() + " - " + $("#Crianca_Endereco_Estado").val();

        var endResp1 = "";

        if ($("#Responsavel1_Endereco_Complemento").val() == "")
            endResp1 = $("#Responsavel1_Endereco_Logradouro").val() + ", " + $("#Responsavel1_Endereco_Numero").val() + " - " + $("#Responsavel1_Endereco_Bairro").val() + " - " + $("#Responsavel1_Endereco_Cidade").val() + " - " + $("#Responsavel1_Endereco_Estado").val();
        else
            endResp1 = $("#Responsavel1_Endereco_Logradouro").val() + ", " + $("#Responsavel1_Endereco_Numero").val() + " - " + " - " + $("#Responsavel1_Endereco_Complemento").val() + " - " + $("#Responsavel1_Endereco_Bairro").val() + " - " + $("#Responsavel1_Endereco_Cidade").val() + " - " + $("#Responsavel1_Endereco_Estado").val();

        var endResp2 = " ";

        if ($("#Responsavel2_Endereco_Cep").val() != "") {
            if ($("#Responsavel2_Endereco_Complemento").val() == "")
                endResp2 = $("#Responsavel2_Endereco_Logradouro").val() + ", " + $("#Responsavel2_Endereco_Numero").val() + " - " + $("#Responsavel2_Endereco_Bairro").val() + " - " + $("#Responsavel2_Endereco_Cidade").val() + " - " + $("#Responsavel2_Endereco_Estado").val();
            else
                endResp2 = $("#Responsavel2_Endereco_Logradouro").val() + ", " + $("#Responsavel2_Endereco_Numero").val() + " - " + " - " + $("#Responsavel2_Endereco_Complemento").val() + " - " + $("#Responsavel2_Endereco_Bairro").val() + " - " + $("#Responsavel2_Endereco_Cidade").val() + " - " + $("#Responsavel2_Endereco_Estado").val();
        }

        var contatoResp1 = "";

        if ($("#Responsavel1_Celular").val() != "") {
            contatoResp1 = $("#Responsavel1_Celular").val();
        }
        else if ($("#Responsavel1_Telefone").val() != "") {
            contatoResp1 = $("#Responsavel1_Telefone").val();
        }

        var contatoResp2 = "";

        if ($("#Responsavel2_Celular").val() != "") {
            contatoResp2 = $("#Responsavel2_Celular").val();
        }
        else if ($("#Responsavel2_Telefone").val() != "") {
            contatoResp2 = $("#Responsavel2_Telefone").val();
        }

        PopularAutorizacao(endCrianca, endResp1, endResp2, contatoResp1, contatoResp2);
        return true;
    }
    else {
        $(".my-body").html(msg)
        $("#my-modal").modal('show');
        return false;
    }
};

function ValidarAceiteAutImagem() {

    if ($("#idautorizacao")[0].checked) {
        var myObject = new Object();

        myObject.Modelo = new Object();
        myObject.Modelo.Nome = $("#Crianca_Nome").val();
        myObject.Modelo.IdEvento = $("#IdEvento").val();
        myObject.Modelo.IdSeletiva = $("#IdSeletiva").val();
        myObject.Modelo.Idade = $("#Idade").val();
        myObject.Modelo.NomeArtistico = $("#NomeArtistico").val();
        myObject.Modelo.ValorSeletiva = $("#ValorSeletiva").val();
        myObject.Modelo.HorarioDesfile = $("#HorarioDesfile").val();

        myObject.Modelo.Crianca = new Object();
        myObject.Modelo.Crianca.Nome = $("#Crianca_Nome").val();
        myObject.Modelo.Crianca.Rg = $("#Crianca_Rg").val();
        myObject.Modelo.Crianca.Cpf = $("#Crianca_Cpf").val();
        myObject.Modelo.Crianca.DataNascimento = $("#Crianca_DataNascimento").val();
        myObject.Modelo.Crianca.Instagram = $("#Crianca_Instagram").val();
        myObject.Modelo.Crianca.Email = $("#Crianca_Email").val();
        myObject.Modelo.Crianca.Nacionalidade = $("#Crianca_Nacionalidade").val();
        myObject.Modelo.Crianca.Sexo = $("#Crianca_Sexo").val();

        myObject.Modelo.Crianca.Endereco = new Object();
        myObject.Modelo.Crianca.Endereco.Id = $("#Crianca_Endereco_Cep").val();
        myObject.Modelo.Crianca.Endereco.Logradouro = $("#Crianca_Endereco_Logradouro").val();
        myObject.Modelo.Crianca.Endereco.Numero = $("#Crianca_Endereco_Numero").val();
        myObject.Modelo.Crianca.Endereco.Complemento = $("#Crianca_Endereco_Complemento").val();
        myObject.Modelo.Crianca.Endereco.Bairro = $("#Crianca_Endereco_Bairro").val();
        myObject.Modelo.Crianca.Endereco.Cidade = $("#Crianca_Endereco_Cidade").val();
        myObject.Modelo.Crianca.Endereco.Cep = $("#Crianca_Endereco_Cep").val();
        myObject.Modelo.Crianca.Endereco.Estado = $("#Crianca_Endereco_Estado").val();
        myObject.Modelo.Crianca.Endereco.Tipo = "R";

        myObject.Modelo.Responsavel1 = new Object();
        myObject.Modelo.Responsavel1.Nome = $("#Responsavel1_Nome").val();
        myObject.Modelo.Responsavel1.Rg = $("#Responsavel1_Rg").val();
        myObject.Modelo.Responsavel1.Cpf = $("#Responsavel1_Cpf").val();
        myObject.Modelo.Responsavel1.Parentesco = $("Responsavel1_Parentesco").val();
        myObject.Modelo.Responsavel1.Email = $("#Responsavel1_Email").val();
        myObject.Modelo.Responsavel1.Celular = $("#Responsavel1_Celular").val();
        myObject.Modelo.Responsavel1.Telefone = $("#Responsavel1_Telefone").val();
        myObject.Modelo.Responsavel1.Nacionalidade = $("#Responsavel1_Nacionalidade").val();

        myObject.Modelo.Responsavel1.Endereco = new Object();
        myObject.Modelo.Responsavel1.Endereco.Id = $("#Responsavel1_Endereco_Cep").val();
        myObject.Modelo.Responsavel1.Endereco.Logradouro = $("#Responsavel1_Endereco_Logradouro").val();
        myObject.Modelo.Responsavel1.Endereco.Numero = $("#Responsavel1_Endereco_Numero").val();
        myObject.Modelo.Responsavel1.Endereco.Complemento = $("#Responsavel1_Endereco_Complemento").val();
        myObject.Modelo.Responsavel1.Endereco.Bairro = $("#Responsavel1_Endereco_Bairro").val();
        myObject.Modelo.Responsavel1.Endereco.Cidade = $("#Responsavel1_Endereco_Cidade").val();
        myObject.Modelo.Responsavel1.Endereco.Cep = $("#Responsavel1_Endereco_Cep").val();
        myObject.Modelo.Responsavel1.Endereco.Estado = $("#Responsavel1_Endereco_Estado").val();
        myObject.Modelo.Responsavel1.Endereco.Tipo = "R";

        myObject.Modelo.Responsavel2 = new Object();
        myObject.Modelo.Responsavel2.Nome = $("#Responsavel2_Nome").val();
        myObject.Modelo.Responsavel2.Rg = $("#Responsavel2_Rg").val();
        myObject.Modelo.Responsavel2.Cpf = $("#Responsavel2_Cpf").val();
        myObject.Modelo.Responsavel2.Parentesco = $("Responsavel2_Parentesco").val();
        myObject.Modelo.Responsavel2.Email = $("#Responsavel2_Email").val();
        myObject.Modelo.Responsavel2.Celular = $("#Responsavel2_Celular").val();
        myObject.Modelo.Responsavel2.Telefone = $("#Responsavel2_Telefone").val();
        myObject.Modelo.Responsavel2.Nacionalidade = $("#Responsavel2_Nacionalidade").val();

        myObject.Modelo.Responsavel2.Endereco = new Object();
        myObject.Modelo.Responsavel2.Endereco.Id = $("#Responsavel2_Endereco_Cep").val();
        myObject.Modelo.Responsavel2.Endereco.Logradouro = $("#Responsavel2_Endereco_Logradouro").val();
        myObject.Modelo.Responsavel2.Endereco.Numero = $("#Responsavel2_Endereco_Numero").val();
        myObject.Modelo.Responsavel2.Endereco.Complemento = $("#Responsavel2_Endereco_Complemento").val();
        myObject.Modelo.Responsavel2.Endereco.Bairro = $("#Responsavel2_Endereco_Bairro").val();
        myObject.Modelo.Responsavel2.Endereco.Cidade = $("#Responsavel2_Endereco_Cidade").val();
        myObject.Modelo.Responsavel2.Endereco.Cep = $("#Responsavel2_Endereco_Cep").val();
        myObject.Modelo.Responsavel2.Endereco.Estado = $("#Responsavel2_Endereco_Estado").val();
        myObject.Modelo.Responsavel2.Endereco.Tipo = "R";

        myObject.Modelo.Manequim = new Object();
        myObject.Modelo.Manequim.Tamanho = $("#Manequim_Tamanho").val();
        myObject.Modelo.Manequim.Altura = $("#Manequim_Altura").val();
        myObject.Modelo.Manequim.Peso = $("#Manequim_Peso").val();
        myObject.Modelo.Manequim.Calcado = $("#Manequim_Calcado").val();
        myObject.Modelo.Manequim.CorPele = $("#Manequim_CorPele").val();
        myObject.Modelo.Manequim.CorOlho = $("#Manequim_CorOlho").val();
        myObject.Modelo.Manequim.CorCabelo = $("#Manequim_CorCabelo").val();
        myObject.Modelo.Manequim.Agenciado = $("#Manequim_Agenciado")[0].checked;
        myObject.Modelo.Manequim.Agencia = $("#Manequim_Agencia").val();
        myObject.Modelo.Manequim.Resumo = $("#Manequim_Resumo").val();

        myObject.Modelo.Autorizado = $("#idautorizacao")[0].checked;

        return SalvarRegistro(myObject);
    }
    else {
        $(".my-body").html("<div>Favor aceitar e imprimir o termo de autoriza&ccedil;&atilde;o.</div>")
        $("#my-modal").modal('show');
        return false;
    }
};

function ValidarPagamento() {
    var ret = false;
    if ($("#idResp1")[0].checked || $("#idResp2")[0].checked) {

        var myObject = new Object();

        myObject.IdSeletiva = $("#IdSeletiva").val();
        myObject.ValorPagamento = $("#ValorPagamento").val();

        if ($("#idResp1")[0].checked)
            myObject.IdPessoa = $("#idResp1").data("id");
        else if ($("#idResp2")[0].checked)
            myObject.IdPessoa = $("#idResp2").data("id");

        myObject.TipoPagamento = $("#TipoPagamento").val();
        myObject.Inscricao = $("#Modelo_Inscricao").val();

        ret = ConfirmarInscricao(myObject);
    }
    else {
        $(".my-body").html("<div>Favor selecione um respons&aacute;vel para o pagamento.</div>")
        $("#my-modal").modal('show');
    }
    return ret;
};

function ValDadosCrianca() {
    var msg = '';

    if ($("#HorarioDesfile").val() == '')
        msg += "<div>Favor informe o Hor&aacute;rio apresenta&ccedil;&atilde;o.</div>";

    if ($("#Crianca_Nome").val() == '')
        msg += "<div>Favor informe o Nome.</div>";

    //if ($("#Crianca_Nome").val() != '') {
    //    var filter = /^[A-Z\u00C0-\u00DD][A-z\u00C0-\u00FF']+\s([A-z\u00C0-\u00FF']\s?)*[A-Z\u00C0-\u00DD][A-z\u00C0-\u00FF']+$/;
    //    if (!filter.test($("#Crianca_Nome").val()))
    //        msg += "<div>Nome inv&aacute;lido. Favor verificar se sobrenome n&atilde;o informado ou iniciais em min&uacute;sculo.</div>";
    //}

    if ($("#Crianca_DataNascimento").val() == '')
        msg += "<div>Favor informe a Data de nascimento.</div>";

    if ($("#Idade").val() == '' || $("#Idade").val() == '0')
        msg += "<div>Favor informe a Idade.</div>";

    if ($("#Crianca_Email").val() != '') {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test($("#Crianca_Email").val()))
            msg += "<div>Favor informe um E-mail v&aacute;lido.</div>";
    }

    if ($("#Crianca_Sexo").val() == '') {
        msg += "<div>Favor informe o Sexo.</div>";
    }

    if ($("#Crianca_Cpf").val() != '') {
        if (!ValidarCpf($("#Crianca_Cpf").val()))
            msg += "<div>Cpf informado inv&aacute;lido.</div>";
    }

    if ($('.imgCrianca').find('img').length != 2)
        msg += "<div>Favor informe as duas imagens obrigat&oacute;rias.</div>";

    if ($("#Crianca_Endereco_Cep").val() != '' && $("#Crianca_Endereco_Numero").val() == '') {
        msg += "<div>Favor informe o N&uacute;mero do endere&ccedil;o</div>";
    }

    return msg;
}

function ValDadosResponsavel() {
    var msgResp1 = '';

    if ($("#Responsavel1_Parentesco").val() == '')
        msgResp1 += "<div>Favor informe o Parentesco do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Nome").val() == '')
        msgResp1 += "<div>Favor informe o Nome do primeiro respons&aacute;vel.</div>";

    //if ($("#Responsavel1_Nome").val() != '') {
    //    var filter = /^[A-Z\u00C0-\u00DD][A-z\u00C0-\u00FF']+\s([A-z\u00C0-\u00FF']\s?)*[A-Z\u00C0-\u00DD][A-z\u00C0-\u00FF']+$/;
    //    if (!filter.test($("#Responsavel1_Nome").val()))
    //        msgResp1 += "<div>Nome inv&aacute;lido do primeiro respons&aacute;vel. Favor verificar se sobrenome não informado ou iniciais em min&uacute;sculo.</div>";
    //}

    if ($("#Responsavel1_Cpf").val() == '')
        msgResp1 += "<div>Favor informe o Cpf do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Rg").val() == '')
        msgResp1 += "<div>Favor informe o Rg do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Cpf").val() != '') {
        if (!ValidarCpf($("#Responsavel1_Cpf").val()))
            msgResp1 += "<div>Cpf do primeiro respons&aacute;vel informado inv&aacute;lido.</div>";
    }

    if ($("#Responsavel1_Email").val() == '')
        msgResp1 += "<div>Favor informe o E-mail do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Email").val() != '') {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test($("#Responsavel1_Email").val()))
            msgResp1 += "<div>Favor informe um E-mail do primeiro respons&aacute;vel v&aacute;lido.</div>";
    }

    if ($("#Responsavel1_Celular").val() == '')
        msgResp1 += "<div>Favor informe o Celular do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Endereco_Cep").val() == '')
        msgResp1 += "<div>Favor informe o Cep do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Endereco_Logradouro").val() == '')
        msgResp1 += "<div>Favor informe o Logradouro do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Endereco_Numero").val() == '')
        msgResp1 += "<div>Favor informe o N&uacute;mero do endere&ccedil;o do primeiro respons&aacute;vel</div>";

    if ($("#Responsavel1_Endereco_Bairro").val() == '')
        msgResp1 += "<div>Favor informe o Bairro do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Endereco_Cidade").val() == '')
        msgResp1 += "<div>Favor informe o Cidade do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel1_Endereco_Estado").val() == '')
        msgResp1 += "<div>Favor informe o Estado do primeiro respons&aacute;vel.</div>";

    if ($("#Responsavel2_Parentesco").val() != '' || $("#Responsavel2_Nome").val() != '' || $("#Responsavel2_Cpf").val() != '' || $("#Responsavel2_Rg").val() != '' ||
        $("#Responsavel2_Email").val() != '' || $("#Responsavel2_Celular").val() != '' || $("#Responsavel2_Endereco_Cep").val() != '' || $("#Responsavel2_Endereco_Logradouro").val() != '' ||
        $("#Responsavel2_Endereco_Numero").val() != '' || $("#Responsavel2_Endereco_Bairro").val() != '' || $("#Responsavel2_Endereco_Bairro").val() != '' || $("#Responsavel2_Endereco_Cidade").val() != '' ||
        $("#Responsavel2_Endereco_Estado").val() != '') {
        if ($("#Responsavel2_Parentesco").val() == '')
            msgResp1 += "<div>Favor informe o Parentesco do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Nome").val() == '')
            msgResp1 += "<div>Favor informe o Nome do segundo respons&aacute;vel.</div>";

        //if ($("#Responsavel2_Nome").val() != '') {
        //    var filter = /^[A-Z\u00C0-\u00DD][A-z\u00C0-\u00FF']+\s([A-z\u00C0-\u00FF']\s?)*[A-Z\u00C0-\u00DD][A-z\u00C0-\u00FF']+$/;
        //    if (!filter.test($("#Responsavel2_Nome").val()))
        //        msgResp1 += "<div>Nome inv&aacute;lido do segundo respons&aacute;vel. Favor verificar se sobrenome não informado ou iniciais em min&uacute;sculo.</div>";
        //}

        if ($("#Responsavel2_Cpf").val() == '')
            msgResp1 += "<div>Favor informe o Cpf do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Rg").val() == '')
            msgResp1 += "<div>Favor informe o Rg do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Cpf").val() != '') {
            if (!ValidarCpf($("#Responsavel2_Cpf").val()))
                msgResp1 += "<div>Cpf do primeiro respons&aacute;vel informado inv&aacute;lido.</div>";
        }

        if ($("#Responsavel2_Email").val() == '')
            msgResp1 += "<div>Favor informe o E-mail do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Email").val() != '') {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test($("#Responsavel2_Email").val()))
                msgResp1 += "<div>Favor informe um E-mail do segundo respons&aacute;vel v&aacute;lido.</div>";
        }

        if ($("#Responsavel2_Celular").val() == '')
            msgResp1 += "<div>Favor informe o Celular do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Endereco_Cep").val() == '')
            msgResp1 += "<div>Favor informe o Cep do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Endereco_Logradouro").val() == '')
            msgResp1 += "<div>Favor informe o Logradouro do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Endereco_Numero").val() == '')
            msgResp1 += "<div>Favor informe o N&uacute;mero do endere&ccedil;o do segundo respons&aacute;vel</div>";

        if ($("#Responsavel2_Endereco_Bairro").val() == '')
            msgResp1 += "<div>Favor informe o Bairro do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Endereco_Cidade").val() == '')
            msgResp1 += "<div>Favor informe o Cidade do segundo respons&aacute;vel.</div>";

        if ($("#Responsavel2_Endereco_Estado").val() == '')
            msgResp1 += "<div>Favor informe o Estado do segundo respons&aacute;vel.</div>";
    }

    return msgResp1;
}

function ValDadosManequim() {
    var msgManequim = '';

    if ($("#NomeArtistico").val() == '')
        msgManequim += "<div>Favor informe o Nome art&Iacute;stico.</div>";

    if ($("#Manequim_Manequim").val() == '')
        msgManequim += "<div>Favor informe o Manequim.</div>";

    if ($("#Manequim_Altura").val() == '')
        msgManequim += "<div>Favor informe a Altura.</div>";

    if ($("#Manequim_Peso").val() == '')
        msgManequim += "<div>Favor informe o Peso.</div>";

    if ($("#Manequim_Calcado").val() == '')
        msgManequim += "<div>Favor informe o Cal&ccedil;ado.</div>";

    if ($("#Manequim_CorPele").val() == '')
        msgManequim += "<div>Favor informe a Cor da pele.</div>";

    if ($("#Manequim_CorOlho").val() == '')
        msgManequim += "<div>Favor informe a Cor do olho.</div>";

    if ($("#Manequim_CorCabelo").val() == '')
        msgManequim += "<div>Favor informe a Cor do cabelo.</div>";

    if ($("#Manequim_Agenciado")[0].checked && $("#Manequim_Agencia").val() == '')
        msgManequim += "<div>Favor informe a Ag&ecirc;ncia.</div>";

    return msgManequim;
};

function SalvarRegistro(obj) {
    var v = true;
    $.ajax({
        url: "Inscricao",
        data: JSON.stringify(obj),
        cache: false,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (data) {
            if (data.Status == "Success") {
                $("#respId1").html($("#Responsavel1_Nome").val());
                $("#idResp1").data("id", data.IdPessoa1);
                $("#Modelo_Inscricao").val(data.Inscricao);

                if ($("#Responsavel2_Cpf").val() != '') {
                    $("#resp2Div").show();
                    $("#respId2").html($("#Responsavel2_Nome").val());
                    $("#idResp2").data("id", data.IdPessoa2);
                }
                else {
                    $("#resp2Div").hide();
                }

                if (SalvarImagemCrianca()) {
                    v = true;
                }
                else {
                    $(".my-body").html("<div>" + "Falha ao salvar a imagem da criança!" + "</div>")
                    $("#my-modal").modal('show');
                    v = false;
                }
            }
            else {
                v = false;
                $(".my-body").html("<div>" + data.Data + "</div>")
                $("#my-modal").modal('show');
            }
        },
        error: function (response) {
            v = false;
            alert(JSON.stringify(response.errors));
        }
    });
    return v;
};

function SalvarImagemCrianca() {
    var v = false;
    var data = new FormData();
    $.each($("input[type='file']"), function (i, file) {
        data.append(file.files[0].name, file.files[0]);
    });
    $.ajax({
        type: 'POST',
        url: $("#UrlImagem").val(),
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        async: true,
        success: function (result) {
            v = true;
        },
        error: function (err) {
            v = false;
        }
    });

    return v;
};

function ConfirmarInscricao(obj) {
    var v = true;
    $.ajax({
        url: $("#UrlPagamento").val(),
        data: JSON.stringify(obj),
        cache: false,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (data) {
            if (data.Status == "Success") {
                if (data.Data != null) {
                    PagSeguroLightbox({
                        code: data.Data
                    }, {
                        success: function (transactionCode) {
                            v = true;
                        },
                        abort: function () {
                            v = false;
                            $(".my-body").html("<div>Inscri&ccedil;&atilde;o n&atilde;o confirmada! Pagamento n&atilde;o realizado.</div>")
                            $("#my-modal").modal('show');
                        }
                    });
                }
            }
            else {
                $(".my-body").html("<div>" + data.Data + "</div>")
                $("#my-modal").modal('show');
                v = false;
            }
        },
        error: function (response) {
            v = false;
            alert(JSON.stringify(response.errors));
        }
    });
    return v;
};

function PopularAutorizacao(endCrianca, endResp1, endResp2, contatoResp1, contatoResp2) {
    $("#autorizacao").html($("#autorizacao").html()
        .replace("{nomecrianca}", $("#Crianca_Nome").val()).replace("{nacionalidadecrianc}", $("#Crianca_Nacionalidade").val()).replace("{identidadecrianca}", $("#Crianca_Rg").val()).replace("{cpfcrianca}", $("#Crianca_Cpf").val()).replace("{ednerecoCompletoCrianca}", endCrianca).replace("{cepCrianca}", $("#Crianca_Endereco_Cep").val())
        .replace("{respumnome}", $("#Responsavel1_Nome").val()).replace("{respumnasc}", $("#Responsavel1_Nacionalidade").val()).replace("{respumci}", $("#Responsavel1_Rg").val()).replace("{respumcpf}", $("#Responsavel1_Cpf").val()).replace("{enderecorespum}", endResp1).replace("{ceprespum}", $("#Responsavel1_Endereco_Cep").val()).replace("{emailrespum}", $("#Responsavel1_Email").val()).replace("{contatoum}", contatoResp1)
        .replace("{respdoisnome}", $("#Responsavel2_Nome").val()).replace("{respdoisnasc}", $("#Responsavel2_Nacionalidade").val()).replace("{respdoisci}", $("#Responsavel2_Rg").val()).replace("{respdocpf}", $("#Responsavel2_Cpf").val()).replace("{enderecorespdo}", endResp2).replace("{ceprespdo}", $("#Responsavel2_Endereco_Cep").val()).replace("{emailrespdo}", $("#Responsavel2_Email").val()).replace("{contatodo}", contatoResp2).replace("{nomecrianca}", $("#Crianca_Nome").val()));
};

function ValidarCpf(valor) {
    var invalidos = [
        '111.111.111-11',
        '222.222.222-22',
        '333.333.333-33',
        '444.444.444-44',
        '555.555.555-55',
        '666.666.666-66',
        '777.777.777-77',
        '888.888.888-88',
        '999.999.999-99',
        '000.000.000-00'
    ];
    for (i = 0; i < invalidos.length; i++) {
        if (invalidos[i] == valor) {
            return false;
        }
    }

    valor = valor.replace("-", "");
    valor = valor.replace(/\./g, "");

    //validando primeiro digito
    add = 0;
    for (i = 0; i < 9; i++) {
        add += parseInt(valor.charAt(i), 10) * (10 - i);
    }
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11) {
        rev = 0;
    }
    if (rev != parseInt(valor.charAt(9), 10)) {
        return false;
    }

    //validando segundo digito
    add = 0;
    for (i = 0; i < 10; i++) {
        add += parseInt(valor.charAt(i), 10) * (11 - i);
    }
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11) {
        rev = 0;
    }
    if (rev != parseInt(valor.charAt(10), 10)) {
        return false;
    }

    return true;
}