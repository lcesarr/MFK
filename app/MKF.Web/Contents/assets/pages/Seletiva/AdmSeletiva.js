﻿$(document).ready(function () {
    $('.cep').mask('99999-999');

    jQuery.datetimepicker.setLocale('pt-BR');
    $('#DataInicio').datetimepicker({
        timepicker: true,
        format: 'd/m/Y H:i',
        step: 10
    });

    $('#DataFim').datetimepicker({
        timepicker: true,
        format: 'd/m/Y H:i',
        step: 10
    });

    $("#btnSalvar").on('click', function (e) {
        $("#loading").show();
        var t = $("#basic_validate").serialize();
        url = $("#Url").val();
        
        if (url == "")
            url = "Create";
        $.ajax({
            type: "POST",
            url: url,
            data: t, // serializes the form's elements.
            success: function (data) {
                if (data.Status == "Success") Sucesso();
                else if (data.Status == "Error") Erro(data.Data);
                else Alerta(data.Data);

                $("#loading").hide();
            }
        });
    });

    $(document).on('keyup', '.fone', function (e) {
        this.value = FormatTelefone(this.value);
    });

    $(document).on('change', '.cep', function (e) {
        $("#loading").show();
        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.erro) {
                    $("#Local_Logradouro").val("");
                    $("#Local_Numero").val("");
                    $("#Local_Complemento").val("");
                    $("#Local_Bairro").val("");
                    $("#Local_Cidade").val("");
                    $("#Local_Estado").val("");
                }
                else {
                    $("#Local_Logradouro").val(data.logradouro);
                    $("#Local_Numero").val("");
                    $("#Local_Complemento").val("");
                    $("#Local_Bairro").val(data.bairro);
                    $("#Local_Cidade").val(data.localidade);
                    $("#Local_Estado").val(data.uf);
                }

                $("#loading").hide();
            },
            error: function (response) {
                $("#Local_Logradouro").val("");
                $("#Local_Numero").val("");
                $("#Local_Complemento").val("");
                $("#Local_Bairro").val("");
                $("#Local_Cidade").val("");
                $("#Local_Estado").val("");

                $("#loading").hide();
            }
        });
    });
});