﻿function Sucesso() {
    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
            var $title = this.options.title || '&nbsp;'
            if (("title_html" in this.options) && this.options.title_html == true)
                title.html($title);
            else title.text($title);
        }
    }));

    $("#msgSucesso").removeClass('hide').dialog({
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-thumbs-up'></i> Sucesso!</h4></div>",
        title_html: true,
        buttons: [
            {
                text: "Sim",
                "class": "btn btn-primary btn-minier",
                click: function () {
                    window.location.href = urlCreate;
                }
            },
            {
                text: "Não",
                "class": "btn btn-minier",
                click: function () {
                    window.location.href = urlIndex;
                }
            }
        ]
    });
}

function Sucesso(dados) {
    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
            var $title = this.options.title || '&nbsp;'
            if (("title_html" in this.options) && this.options.title_html == true)
                title.html($title);
            else title.text($title);
        }
    }));

    $("#msgSucessoParam").find("#msgDescMsg").html(dados);
    $("#msgSucessoParam").removeClass('hide').dialog({
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-thumbs-up'></i> Sucesso!</h4></div>",
        title_html: true,
        buttons: [
            {
                text: "OK",
                "class": "btn btn-primary btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function Erro(dados) {
    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
            var $title = this.options.title || '&nbsp;'
            if (("title_html" in this.options) && this.options.title_html == true)
                title.html($title);
            else title.text($title);
        }
    }));

    $("#msgErro").find("#msgDescErro").html(dados);
    var dialog = $("#msgErro").removeClass('hide').dialog({
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='red'><i class='ace-icon fa fa-thumbs-down'></i> Falha ao realizar o cadastro!</h4></div>",
        title_html: true,
        width: '400',
        heigth: 'auto',
        buttons: [
            {
                text: "OK",
                "class": "btn btn-primary btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function Alerta(dados) {
    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
            var $title = this.options.title || '&nbsp;'
            if (("title_html" in this.options) && this.options.title_html == true)
                title.html($title);
            else title.text($title);
        }
    }));

    $("#msgErro").find("#msgDescErro").html(dados);
    var dialog = $("#msgErro").removeClass('hide').dialog({
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='text-warning bigger-110 orange'><i class='ace-icon fa fa-lightbulb-o'></i> Atenção!</h4></div>",
        title_html: true,
        width: '400',
        heigth: 'auto',
        buttons: [
            {
                text: "OK",
                "class": "btn btn-primary btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}