﻿$(document).ready(function () {
    $('#CNPJ').mask('99.999.999/9999-99');
    $('#Responsavel_Cpf').mask('999.999.999-99');
    $('.cep').mask('99999-999');

    $("#btnSalvar").on('click', function (e) {
        $("#loading").show();
        var t = $("#basic_validate").serialize();
        url = $("#Url").val();
        
        if (url == "")
            url = "Create";
        $.ajax({
            type: "POST",
            url: url,
            data: t, // serializes the form's elements.
            success: function (data) {
                if (data.Status == "Success") Sucesso();
                else if (data.Status == "Error") Erro(data.Data);
                else Alerta(data.Data);

                $("#loading").hide();
            }
        });
    });

    $(document).on('keyup', '.fone', function (e) {
        this.value = FormatTelefone(this.value);
    });

    $(document).on('change', '.cep', function (e) {
        $("#loading").show();
        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.erro) {
                    $("#Endereco_Logradouro").val("");
                    $("#Endereco_Numero").val("");
                    $("#Endereco_Complemento").val("");
                    $("#Endereco_Bairro").val("");
                    $("#Endereco_Cidade").val("");
                    $("#Endereco_Estado").val("");
                }
                else {
                    $("#Endereco_Logradouro").val(data.logradouro);
                    $("#Endereco_Numero").val("");
                    $("#Endereco_Complemento").val("");
                    $("#Endereco_Bairro").val(data.bairro);
                    $("#Endereco_Cidade").val(data.localidade);
                    $("#Endereco_Estado").val(data.uf);
                }

                $("#loading").hide();
            },
            error: function (response) {
                $("#Endereco_Logradouro").val("");
                $("#Endereco_Numero").val("");
                $("#Endereco_Complemento").val("");
                $("#Endereco_Bairro").val("");
                $("#Endereco_Cidade").val("");
                $("#Endereco_Estado").val("");

                $("#loading").hide();
            }
        });
    });
});