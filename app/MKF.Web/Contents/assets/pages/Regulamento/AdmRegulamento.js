﻿$(document).ready(function () {
    $("#btnSalvar").on('click', function (e) {
        $("#loading").show();
        var t = $("#basic_validate").serialize();
        url = $("#Url").val();

        if (url == "")
            url = "Create";
        $.ajax({
            type: "POST",
            url: url,
            data: t, // serializes the form's elements.
            success: function (data) {
                if (data.Status == "Success") Sucesso("Manutenção realizada com sucesso.");
                else if (data.Status == "Error") Erro(data.Data);
                else Alerta(data.Data);

                $("#loading").hide();
            }
        });
    });
});