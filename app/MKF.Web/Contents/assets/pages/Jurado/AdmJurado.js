﻿$(document).ready(function () {
    $('#Usuario_DadosPessoais_Cpf').mask('999.999.999-99');
    $('.cep').mask('99999-999');

    $("#btnSalvar").on('click', function (e) {
        $("#loading").show();
        var t = $("#basic_validate").serialize();
        url = $("#Url").val();

        if (url == "")
            url = "Create";
        $.ajax({
            type: "POST",
            url: url,
            data: t, // serializes the form's elements.
            success: function (data) {
                if (data.Status == "Success") Sucesso();
                else if (data.Status == "Error") Erro(data.Data);
                else Alerta(data.Data);

                $("#loading").hide();
            }
        });
    });

    $(document).on('keyup', '.fone', function (e) {
        this.value = FormatTelefone(this.value);
    });

    $(document).on('change', '.cep', function (e) {
        $("#loading").show();
        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";

        $.ajax({
            url: url,
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.erro) {
                    $("#Usuario_DadosPessoais_Endereco_Logradouro").val("");
                    $("#Usuario_DadosPessoais_Endereco_Numero").val("");
                    $("#Usuario_DadosPessoais_Endereco_Complemento").val("");
                    $("#Usuario_DadosPessoais_Endereco_Bairro").val("");
                    $("#Usuario_DadosPessoais_Endereco_Cidade").val("");
                    $("#Usuario_DadosPessoais_Endereco_Estado").val("");
                }
                else {
                    $("#Usuario_DadosPessoais_Endereco_Logradouro").val(data.logradouro);
                    $("#Usuario_DadosPessoais_Endereco_Numero").val("");
                    $("#Usuario_DadosPessoais_Endereco_Complemento").val("");
                    $("#Usuario_DadosPessoais_Endereco_Bairro").val(data.bairro);
                    $("#Usuario_DadosPessoais_Endereco_Cidade").val(data.localidade);
                    $("#Usuario_DadosPessoais_Endereco_Estado").val(data.uf);
                }

                $("#loading").hide();
            },
            error: function (response) {
                $("#Usuario_DadosPessoais_Endereco_Logradouro").val("");
                $("#Usuario_DadosPessoais_Endereco_Numero").val("");
                $("#Usuario_DadosPessoais_Endereco_Complemento").val("");
                $("#Usuario_DadosPessoais_Endereco_Bairro").val("");
                $("#Usuario_DadosPessoais_Endereco_Cidade").val("");
                $("#Usuario_DadosPessoais_Endereco_Estado").val("");

                $("#loading").hide();
            }
        });
    });
});