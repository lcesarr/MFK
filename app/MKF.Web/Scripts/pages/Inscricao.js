﻿$(document).ready(function () {
    AtualizarWizard(0);
    jsonObjDados = {}
    jsonObjEndereco = {}
    jsonObjSocio = [];
    jsonObjImagem = [];

    $("#btnEventoDadosNext").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: 'Step1',
            data: $("form").serialize(),
            cache: false,
            type: 'POST',
            success: function (data) {
                
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    $("#step1").hide();
                    $("#step2").show();
                    AtualizarWizard(1);
                }
                else
                    $("#dadosEvento").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });


    $(document).on('change', '.cep', function (e) {
        
        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";
        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.erro) {
                    $("#dadosEnderecoEvento").find('input[type=text],select').each(function () {
                        switch ($(this).attr("name")) {
                            case "Logradouro":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "Bairro":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "Cidade":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "Estado":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            default:
                        }
                    });
                }
                else {
                    $("#dadosEnderecoEvento").find('input[type=text],select').each(function () {
                        switch ($(this).attr("name")) {
                            case "Logradouro":
                                $(this).val(data.logradouro);
                                if (data.logradouro != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "Bairro":
                                $(this).val(data.bairro);
                                if (data.bairro != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "Cidade":
                                $(this).val(data.localidade);
                                if (data.localidade != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "Estado":
                                $(this).val(data.uf);
                                $(this).prop('disabled', true);
                                break;
                            default:
                        }
                    });
                }
            },
            error: function (response) {
                $("#dadosEnderecoEvento").find('input[type=text],select').each(function () {
                    switch ($(this).attr("name")) {
                        case "Logradouro":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "Bairro":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "Cidade":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "Estado":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        default:
                    }
                });
            }
        });
    });

    $("#btnEventoEnderecoNext").click(function (e) {
        e.preventDefault();
        createJSONEndereco();
        
        $.ajax({
            url: 'Step2',
            data: jsonObjEndereco,
            //data: retJson,
            cache: false,
            type: 'POST',
            success: function (data) {
                
                if (data.Status == "Success") {
                    SalvarRegistro();
                }
                else {
                    $("#dadosEnderecoEvento").html(data);
                }
            },
            error: function (response) {
                
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnEventoEnderecoBack").click(function (e) {
        e.preventDefault();
        $("#step2").hide();
        $("#step1").show();
        AtualizarWizard(0);
    });
});

function SalvarRegistro() {
    $.ajax({
        url: 'Step3',
        data: createJSONFinal(),
        //data: retJson,
        cache: false,
        type: 'POST',
        success: function (data) {
            
            if (data.Status == "Success") {
                $("#step2").hide();
                $("#step3").show();

                AtualizarWizard(2);
                //$("#JsonForm").val(data.Data);
            }
            else {
                $("#dadosEnderecoEvento").html(data);
            }
        },
        error: function (response) {
            
            alert(JSON.stringify(response.errors));
        }
    });
}

function createJSONFinal() {
    var myObject = new Object();
    createJSONDados();
    myObject = jsonObjDados;

    createJSONEndereco();
    myObject.Local = jsonObjEndereco;

    return myObject;
}

function createJSONDados() {
    jsonObjDados = {}
    $('#dadosEvento').find('input[type=text],select,textarea').each(function () {
        
        switch ($(this).attr("name")) {
            case "IdEvento":
                jsonObjDados["IdEvento"] = $(this).val();
                break;
            case "Nome":
                jsonObjDados["Nome"] = $(this).val();
                break;
            case "Descricao":
                jsonObjDados["Descricao"] = $(this).val();
                break;
            case "DataInicio":
                jsonObjDados["DataInicio"] = $(this).val();
                break;
            case "DataFim":
                jsonObjDados["DataFim"] = $(this).val();
                break;
            case "IdImagem":
                jsonObjDados["IdImagem"] = $(this).val();
                break;
            case "IdRegulamento":
                jsonObjDados["IdRegulamento"] = $(this).val();
                break;
            default:
        }
    });
}

function createJSONEndereco() {
    jsonObjEndereco = {}
    $('#dadosEnderecoEvento').find('input[type=text], input[type=hidden], select').each(function () {
        switch ($(this).attr("name")) {
            case "Cep":
                jsonObjEndereco["Cep"] = $(this).val();
                break;
            case "Logradouro":
                jsonObjEndereco["Logradouro"] = $(this).val();
                break;
            case "Numero":
                jsonObjEndereco["Numero"] = $(this).val();
                break;
            case "Complemento":
                jsonObjEndereco["Complemento"] = $(this).val();
                break;
            case "Bairro":
                jsonObjEndereco["Bairro"] = $(this).val();
                break;
            case "Cidade":
                jsonObjEndereco["Cidade"] = $(this).val();
                break;
            case "Estado":
                jsonObjEndereco["Estado"] = $(this).val();
                break;
            case "Tipo":
                jsonObjEndereco["Tipo"] = $(this).val();
                break;
            default:
        }
    });
}

function AtualizarWizard(index) {
    var navigation = $(".steps");
    var total = navigation.find('li').length;
    var current = index + 1;
    // set wizard title
    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
    // set done steps
    jQuery('li', $('#form_wizard_1')).removeClass("done");
    jQuery('li', $('#form_wizard_1')).removeClass("active");
    var li_list = navigation.find('li');
    if (index == 0) {
        jQuery(li_list[0]).addClass("active")
    }

    for (var i = 0; i < index; i++) {
        jQuery(li_list[i]).addClass("done");
        jQuery(li_list[i + 1]).addClass("active")
    }

    var total = navigation.find('li').length;
    var current = index + 1;
    var $percent = (current / total) * 100;
    $('#form_wizard_1').find('.progress-bar').css({
        width: $percent + '%'
    });
}