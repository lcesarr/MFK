﻿$(function () {
    // Declare a proxy to reference the hub.
    var DashSeletivaHub = $.connection.DashSeletivaHub;
    registraChamadosExecucao(DashSeletivaHub);
    $.connection.hub.start().done(function () {
        DashSeletivaHub.server.connect();
    });

    $.connection.hub.disconnected(function () {
        console.log("disconnected");
        setTimeout(function () {
            $.connection.hub.start();
        }, 5000); // Restart connection after 5 seconds.you can set the time based your requirement
    });
});

function registraChamadosExecucao(DashSeletivaHub) {
    // Calls when user successfully logged in
    DashSeletivaHub.client.onConnected = function (stocks) {

        LimparAreasDashboard();
        MontarAreaTotalizadorSeletiva(stocks);
    }

    DashSeletivaHub.client.onUpdated = function (stocks) {

        LimparAreasDashboard();
        MontarAreaTotalizadorSeletiva(stocks);
    }
}

function LimparAreasDashboard() {
    $("#InscricaoTotal").html('0');
    $("#InscricaoPagas").html('0');
    $("#InscricaoAnalise").html('0');
    $("#InscricaoAguardando").html('0');
};

function MontarAreaTotalizadorSeletiva(excecucoes) {

    $('.easy-pie-chart.percentage').each(function () {
        var $box = $(this).closest('.infobox');
        var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
        var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
        var size = parseInt($(this).data('size')) || 50;
        $(this).easyPieChart({
            barColor: barColor,
            trackColor: trackColor,
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: parseInt(size / 10),
            animate: ace.vars['old_ie'] ? false : 1000,
            size: size
        });
    })

    $('.sparkline').each(function () {
        var $box = $(this).closest('.infobox');
        var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
        $(this).sparkline('html',
                         {
                             tagValuesAttribute: 'data-values',
                             type: 'bar',
                             barColor: barColor,
                             chartRangeMin: $(this).data('min') || 0
                         });
    });


    //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
    //but sometimes it brings up errors with normal resize event handlers
    $.resize.throttleWindow = false;

    var placeholder = $('#piechart-placeholder').css({ 'width': '90%', 'min-height': '250px' });
    var data = [];
    $.each(excecucoes.StatusPagamento, function (i, item) {
        debugger;
        data = [
          { label: "Pagas", data: item.Pagas, color: "#68BC31" },
          { label: "Em análise", data: item.EmAnalise, color: "#2091CF" },
          { label: "Aguardando pagamento", data: item.AguardandoPagamento, color: "#FEB902" },
          { label: "Canceladas", data: item.NaoConfirmadas, color: "#DA5430" }
        ];

        $("#hpagas").html("R$ " + (item.Pagas * item.Valor).toLocaleString('pt-BR'));
        $("#hanalise").html("R$ " + (item.EmAnalise * item.Valor).toLocaleString('pt-BR'));
        $("#hagpagamento").html("R$ " + (item.AguardandoPagamento * item.Valor).toLocaleString('pt-BR'));
        $("#hcanceladas").html("R$ " + (item.NaoConfirmadas * item.Valor).toLocaleString('pt-BR'));
    });
    $("#tdPagamentos").html("");
    var tr = "";
    $.each(excecucoes.TipoPagamento, function (i, item) {

        tr = "<tr><td>Cartão crédito</td><td class=\"hidden-480\"><span class=\"label label-warning arrowed-right arrowed-in\">Aguardando pagamento</span></td><td><b class=\"green\">" + item.AgCartao + "</b></td></tr>";
        tr += "<tr><td>Cartão crédito</td><td class=\"hidden-480\"><span class=\"label label-success arrowed-right arrowed-in\">Pago</span></td><td><b class=\"green\">" + item.PgCartao + "</b></td></tr>";
        tr += "<tr><td>Cartão crédito</td><td class=\"hidden-480\"><span class=\"label label-info arrowed-right arrowed-in\">Em análise</span></td><td><b class=\"green\">" + item.EaCartao + "</b></td></tr>";
        tr += "<tr><td>Cartão crédito</td><td class=\"hidden-480\"><span class=\"label label-danger arrowed-right arrowed-in\">Cancelado</span></td><td><b class=\"green\">" + item.CaCartao + "</b></td></tr>";

        tr += "<tr><td>Boleto</td><td class=\"hidden-480\"><span class=\"label label-warning arrowed-right arrowed-in\">Aguardando pagamento</span></td><td><b class=\"green\">" + item.AgBoleto + "</b></td></tr>";
        tr += "<tr><td>Boleto</td><td class=\"hidden-480\"><span class=\"label label-success arrowed-right arrowed-in\">Pago</span></td><td><b class=\"green\">" + item.PgBoleto + "</b></td></tr>";
        tr += "<tr><td>Boleto</td><td class=\"hidden-480\"><span class=\"label label-info arrowed-right arrowed-in\">Em análise</span></td><td><b class=\"green\">" + item.EaBoleto + "</b></td></tr>";
        tr += "<tr><td>Boleto</td><td class=\"hidden-480\"><span class=\"label label-danger arrowed-right arrowed-in\">Cancelado</span></td><td><b class=\"green\">" + item.CaBoleto + "</b></td></tr>";

        tr += "<tr><td>Outros</td><td class=\"hidden-480\"><span class=\"label label-warning arrowed-right arrowed-in\">Aguardando pagamento</span></td><td><b class=\"green\">" + item.AgOutros + "</b></td></tr>";
        tr += "<tr><td>Outros</td><td class=\"hidden-480\"><span class=\"label label-success arrowed-right arrowed-in\">Pago</span></td><td><b class=\"green\">" + item.PgOutros + "</b></td></tr>";
        tr += "<tr><td>Outros</td><td class=\"hidden-480\"><span class=\"label label-info arrowed-right arrowed-in\">Em análise</span></td><td><b class=\"green\">" + item.EaOutros + "</b></td></tr>";
        tr += "<tr><td>Outros</td><td class=\"hidden-480\"><span class=\"label label-danger arrowed-right arrowed-in\">Cancelado</span></td><td><b class=\"green\">" + item.CaOutros + "</b></td></tr>";
    });

    $("#tdPagamentos").append(tr);

    function drawPieChart(placeholder, data, position) {

        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    tilt: 0.8,
                    highlight: {
                        opacity: 0.25
                    },
                    stroke: {
                        color: '#fff',
                        width: 2
                    },
                    startAngle: 2
                }
            },
            legend: {
                show: true,
                position: position || "ne",
                labelBoxBorderColor: null,
                margin: [-30, 15]
            }
          ,
            grid: {
                hoverable: true,
                clickable: true
            }
        })
    }
    drawPieChart(placeholder, data);

    /**
    we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
    so that's not needed actually.
    */
    placeholder.data('chart', data);
    placeholder.data('draw', drawPieChart);


    //pie chart tooltip example
    var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
    var previousPoint = null;

    placeholder.on('plothover', function (event, pos, item) {
        if (item) {
            if (previousPoint != item.seriesIndex) {
                previousPoint = item.seriesIndex;
                var tip = item.series['label'] + " : " + item["datapoint"][1][0][1] + " | " + item.series['percent'].toFixed(2) + '%';
                $tooltip.show().children(0).text(tip);
            }
            $tooltip.css({ top: pos.pageY + 10, left: pos.pageX + 10 });
        } else {
            $tooltip.hide();
            previousPoint = null;
        }

    });

    /////////////////////////////////////
    $(document).one('ajaxloadstart.page', function (e) {
        $tooltip.remove();
    });
};