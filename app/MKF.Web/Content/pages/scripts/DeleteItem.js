﻿$(".excluir").on("click", function (e) {
    
    e.preventDefault();
    $("#itemRemover").val($(this)[0].id);
    $("#urlDelete").val($(this).data("url"));
    $("#controller").val($(this).data("control"));
});

$("#btnExcluir").on("click", function (e) {
    e.preventDefault();
    
    $.ajax({
        type: "post",
        url: $("#urlDelete").val(),
        ajaxasync: true,
        data: { id: $('#itemRemover').val() },
        success: function (e) {
            
            window.location.reload();
        },
        error: function (data) {
            
            alert(data);
        }
    });
});