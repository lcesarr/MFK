﻿$(document).ready(function () {
    AtualizarWizard(0);
    jsonObjDados = {}

    $("#btnTipoParceriaDadosNext").click(function (e) {
        e.preventDefault();
        createJSONObjeto();
        $.ajax({
            url: 'Create',
            data: jsonObjDados,
            cache: false,
            type: 'POST',
            success: function (data) {
                if (data.Status == "Success") {
                    $("#step1").hide();
                    $("#step2").show();

                    AtualizarWizard(1);
                }
                else
                    $("#dadosTipoParceria").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });
});

function createJSONObjeto() {
    jsonObjDados = {}
    $('#dadosTipoParceria').find('input[type=text],select').each(function () {
        
        switch ($(this).attr("name")) {
            case "Id":
                jsonObjDados["Id"] = $(this).val();
                break;
            case "Nome":
                jsonObjDados["Nome"] = $(this).val();
                break;
            case "Status":
                jsonObjDados["Status"] = $(this).val();
                item = {};
                break;
            default:
        }
    });
}

function AtualizarWizard(index) {
    var navigation = $(".steps");
    var total = navigation.find('li').length;
    var current = index + 1;
    // set wizard title
    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
    // set done steps
    jQuery('li', $('#form_wizard_1')).removeClass("done");
    jQuery('li', $('#form_wizard_1')).removeClass("active");
    var li_list = navigation.find('li');
    if (index == 0) {
        jQuery(li_list[0]).addClass("active")
    }

    for (var i = 0; i < index; i++) {
        jQuery(li_list[i]).addClass("done");
        jQuery(li_list[i + 1]).addClass("active")
    }

    var total = navigation.find('li').length;
    var current = index + 1;
    var $percent = (current / total) * 100;
    $('#form_wizard_1').find('.progress-bar').css({
        width: $percent + '%'
    });
}