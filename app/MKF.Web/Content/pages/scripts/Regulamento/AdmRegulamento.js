﻿$(document).ready(function () {
    AtualizarWizard(0);
    jsonObjDados = {}

    $("#btnRegulamentoDadosNext").click(function (e) {
        e.preventDefault();
        createJSONObjeto();
        $.ajax({
            url: RetornarUrl('Create', 'Create'),
            data: JSON.stringify(jsonObjDados),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Status == "Success") {
                    $("#step1").hide();
                    $("#step2").show();

                    AtualizarWizard(1);
                }
                else if (data.Status == "Error") {

                    $("#step1").hide();
                    $("#step3").show();

                    $("#msgErro").html(data.Data);

                    AtualizarWizard(3);
                }
                else {
                    $("#dadosRegulamento").html(data);
                }
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnRegulamentoDadosBack").click(function (e) {
        e.preventDefault();
        $("#step3").hide();
        $("#step1").show();
        AtualizarWizard(0);
    });
});

function createJSONObjeto() {
    jsonObjDados = {}
    $('#dadosRegulamento').find('input[type=text],input[type=hidden],textarea,select').each(function () {
        switch ($(this).attr("name")) {
            case "Id":
                jsonObjDados["Id"] = $(this).val();
                break;
            case "Nome":
                jsonObjDados["Nome"] = $(this).val();
                break;
            case "Dados":
                jsonObjDados["Dados"] = $(".note-editable").html();
                break;
            case "Status":
                jsonObjDados["Status"] = $(this).val();
                item = {};
                break;
            case "Tipo":
                jsonObjDados["Tipo"] = $(this).val();
                break;
            default:
        }
    });
}

function AtualizarWizard(index) {
    var navigation = $(".steps");
    var total = navigation.find('li').length;
    var current = index + 1;
    // set wizard title
    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
    // set done steps
    jQuery('li', $('#form_wizard_1')).removeClass("done");
    jQuery('li', $('#form_wizard_1')).removeClass("active");
    var li_list = navigation.find('li');
    if (index == 0) {
        jQuery(li_list[0]).addClass("active")
    }

    for (var i = 0; i < index; i++) {
        jQuery(li_list[i]).addClass("done");
        jQuery(li_list[i + 1]).addClass("active")
    }

    var total = navigation.find('li').length;
    var current = index + 1;
    var $percent = (current / total) * 100;
    $('#form_wizard_1').find('.progress-bar').css({
        width: $percent + '%'
    });
}

function RetornarUrl(step, stepAnt) {
    var url = $("#Url").val();

    if (url == "")
        url = step;
    else {
        var pos = url.indexOf(stepAnt);

        url = url.substring(0, pos) + step;
    }

    return url;
}