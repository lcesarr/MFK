﻿$(document).ready(function () {
    AtualizarWizard(0);
    jsonObjEndereco = [];
    jsonObjSocio = [];
    jsonObjContato = [];

    $("#btnContinueDados").click(function (e) {
        e.preventDefault();
        var dat = JSON.stringify($("#dadosLoja").serialize());

        url = $("#Url").val();

        if (url == "")
            url = "Step1";
        $.ajax({
            url: url,
            data: $("form").serialize(),
            cache: false,
            type: 'POST',
            success: function (data) {

                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    $("#step1").hide();
                    $("#step2").show();
                    AtualizarWizard(1);
                }
                else
                    $("#dadosLoja").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('change', '.cep', function (e) {
        
        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";
        var item = $(this).data("id");
        var it = parseInt(item.substring(5)) - 1;
        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.erro) {
                    $("." + item).find('input[type=text],select').each(function () {
                        switch ($(this).attr("name")) {
                            case "[" + it + "].Logradouro":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "[" + it + "].Bairro":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "[" + it + "].Cidade":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "[" + it + "].Estado":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            default:
                        }
                    });
                }
                else {
                    $("." + item).find('input[type=text],select').each(function () {
                        switch ($(this).attr("name")) {
                            case "[" + it + "].Logradouro":
                                $(this).val(data.logradouro);
                                if (data.logradouro != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "[" + it + "].Bairro":
                                $(this).val(data.bairro);
                                if (data.bairro != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "[" + it + "].Cidade":
                                $(this).val(data.localidade);
                                if (data.localidade != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "[" + it + "].Estado":
                                $(this).val(data.uf);
                                $(this).prop('disabled', true);
                                break;
                            default:
                        }
                    });
                }
            },
            error: function (response) {
                $("." + item).find('input[type=text],select').each(function () {
                    switch ($(this).attr("name")) {
                        case "[" + it + "].Logradouro":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "[" + it + "].Bairro":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "[" + it + "].Cidade":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "[" + it + "].Estado":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        default:
                    }
                });
            }
        });
    });

    $("#btnContinueEndereco").click(function (e) {
        e.preventDefault();
        createJSONEndereco();

        url = $("#Url").val();

        if (url == "")
            url = "Step2";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "Step2";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            //data: retJson,
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    $("#step2").hide();
                    $("#step3").show();
                    AtualizarWizard(2);
                }
                else {
                    $("#dadosEndereco").html(data);
                }
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnVoltarEndereco").click(function (e) {
        e.preventDefault();
        $("#step2").hide();
        $("#step1").show();
        AtualizarWizard(0);
    });

    $("#btnNovoEndereco").click(function (e) {
        e.preventDefault();
        createJSONEndereco();

        url = $("#Url").val();

        if (url == "")
            url = "NovoEndereco";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "NovoEndereco";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".field-validation-error").hide();
                $("#dadosEndereco").html("");
                $("#dadosEndereco").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('click', '.btn-remove', function (e) {

        e.preventDefault();
        createJSONEndereco();
        var item = $(this).data("id");
        delete jsonObjEndereco[item - 1];

        url = $("#Url").val();

        if (url == "")
            url = "RemoverEndereco";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "RemoverEndereco";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosEndereco").html("");
                $("#dadosEndereco").html(data);
            },
            error: function (response) {

                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnContinueResponsavel").click(function (e) {
        e.preventDefault();
        createJSONResponsavel();

        url = $("#Url").val();

        if (url == "")
            url = "Step4";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "Step4";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjSocio),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    SalvarRegistro();
                }
                else
                    $("#dadosResponsavel").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnVoltarResponsavel").click(function (e) {
        e.preventDefault();
        $("#step4").hide();
        $("#step3").show();
        AtualizarWizard(2);
    });

    $("#btnNovoResponsavel").click(function (e) {
        e.preventDefault();
        createJSONResponsavel();

        url = $("#Url").val();

        if (url == "")
            url = "NovoResponsavel";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "NovoResponsavel";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjSocio),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosResponsavel").html("");
                $("#dadosResponsavel").html(data);
            },
            error: function (response) {

                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('click', '.btn-remove-resp', function (e) {
        e.preventDefault();
        createJSONResponsavel();
        var item = $(this).data("id");
        delete jsonObjSocio[item - 1];

        url = $("#Url").val();

        if (url == "")
            url = "RemoverResponsavel";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "RemoverResponsavel";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjSocio),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosResponsavel").html("");
                $("#dadosResponsavel").html(data);
            },
            error: function (response) {

                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnNovoContatoLojista").click(function (e) {
        e.preventDefault();
        createJSONContato();


        url = $("#Url").val();

        if (url == "")
            url = "NovoContato";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "NovoContato";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjContato),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosContatoLojista").html("");
                $("#dadosContatoLojista").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('click', '.btnRemover', function (e) {

        e.preventDefault();
        createJSONContato();
        var item = $(this).data("id");
        delete jsonObjContato[item - 1];

        url = $("#Url").val();

        if (url == "")
            url = "RemoverContato";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "RemoverContato";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjContato),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosContatoJurado").html("");
                $("#dadosContatoJurado").html(data);
            },
            error: function (response) {

                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnLojistaContatoNext").click(function (e) {
        e.preventDefault();
        createJSONContato();


        url = $("#Url").val();

        if (url == "")
            url = "Step3";
        else {
            var pos = url.indexOf('Step1');

            url = url.substring(0, pos) + "Step3";
        }

        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjContato),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Status == "Success") {
                    $("#step3").hide();
                    $("#step4").show();
                    AtualizarWizard(3);
                }
                else
                    $("#dadosContatoJurado").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnLojistaContatoBack").click(function (e) {
        e.preventDefault();
        $("#step3").hide();
        $("#step2").show();
        AtualizarWizard(1);
    });

    $("#btnLojistaDadosBack").click(function (e) {
        e.preventDefault();
        $("#step6").hide();
        $("#step4").show();
        AtualizarWizard(3);
    });

    $(document).on('change', '.select', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var obj = $("#" + id)
        var tipo = "";
        obj.find('input[type=text],select').each(function () {
            switch ($(this).attr("name")) {
                case "item.Dado":
                    if (tipo == "T") {
                        $(this).inputmask("(99) 9999-9999", {
                            autoUnmask: true
                        });
                    }
                    else if (tipo == "C") {
                        $(this).inputmask("(99) 99999-9999", {
                            autoUnmask: true
                        });
                    }
                    else {
                        $(this).inputmask('remove');;
                    }
                    break;
                case "item.Tipo":
                    tipo = $(this).val();
                    break;
            }
        });
    });
});

function createJSONFinal() {
    var myObject = new Object();
    createJSONDados();
    myObject = jsonObjDados;

    createJSONEndereco();
    myObject.Enderecos = jsonObjEndereco;

    createJSONResponsavel();
    myObject.Socios = jsonObjSocio;

    createJSONContato();
    myObject.Contatos = jsonObjContato;


    return myObject;
}

function createJSONDados() {
    jsonObjDados = {}
    $('#dadosLoja').find('input[type=text],input[type=hidden],select,textarea').each(function () {
        switch ($(this).attr("name")) {
            case "CNPJ":
                jsonObjDados["CNPJ"] = $(this).val();
                break;
            case "RazaoSocial":
                jsonObjDados["RazaoSocial"] = $(this).val();
                break;
            case "NomeFantasia":
                jsonObjDados["NomeFantasia"] = $(this).val();
                break;
            case "RamoAtividade":
                jsonObjDados["RamoAtividade"] = $(this).val();
                break;
            case "telefone":
                jsonObjDados["telefone"] = $(this).val();
                break;
            case "Resumo":
                jsonObjDados["Resumo"] = $(this).val();
                break;
            case "Tipo":
                jsonObjDados["Tipo"] = $(this).val();
                break;
            case "Id":
                jsonObjDados["Id"] = $(this).val();
                break;
            case "Url":
                jsonObjDados["Url"] = $(this).val();
                break;
            default:
        }
    });
}

function createJSONEndereco() {
    jsonObjEndereco = [];
    item = {}
    var cont = 0;
    $('#dadosEndereco').find('input[type=text],input[type=hidden],select').each(function () {
        
        switch ($(this).attr("name")) {
            case "[" + cont + "].Id":
                item["Id"] = $(this).val();
                break;
            case "[" + cont + "].IdEmpresa":
                item["IdEmpresa"] = $(this).val();
                break;
            case "[" + cont + "].Cep":
                item["Cep"] = $(this).val();
                break;
            case "[" + cont + "].Tipo":
                item["Tipo"] = $(this).val();
                break;
            case "[" + cont + "].Logradouro":
                item["Logradouro"] = $(this).val();
                break;
            case "[" + cont + "].Numero":
                item["Numero"] = $(this).val();
                break;
            case "[" + cont + "].Complemento":
                item["Complemento"] = $(this).val();
                break;
            case "[" + cont + "].Bairro":
                item["Bairro"] = $(this).val();
                break;
            case "[" + cont + "].Cidade":
                item["Cidade"] = $(this).val();
                break;
            case "[" + cont + "].Estado":
                item["Estado"] = $(this).val();
                jsonObjEndereco.push(item);
                item = {};
                cont++;
                break;
            default:
        }
    });
}

function createJSONResponsavel() {
    jsonObjSocio = [];
    item = {}
    $('#dadosResponsavel').find('input[type=text],input[type=hidden],select').each(function () {

        switch ($(this).attr("name")) {
            case "item.Tipo":
                item["Tipo"] = $(this).val();
                break;
            case "item.Id":
                item["Id"] = $(this).val();
                break;
            case "item.IdEmpresa":
                item["IdEmpresa"] = $(this).val();
                break;
            case "item.Nome":
                item["Nome"] = $(this).val();
                break;
            case "item.Cpf":
                item["Cpf"] = $(this).val();
                break;
            case "item.Celular":
                item["Celular"] = $(this).val();
                jsonObjSocio.push(item);
                item = {};
                break;
            default:
        }
    });
}

function createJSONContato() {

    jsonObjContato = [];
    item = {}
    $('#dadosContatoLojista').find('input[type=text],select').each(function () {
        switch ($(this).attr("name")) {
            case "item.Tipo":
                item["Tipo"] = $(this).val();
                break;
            case "item.Dado":
                item["Dado"] = $(this).val();
                jsonObjContato.push(item);
                item = {
                };
                break;
            default:
        }
    });
}

function SalvarRegistro() {

    url = $("#Url").val();

    if (url == "")
        url = "Step5";
    else {
        var pos = url.indexOf('Step1');

        url = url.substring(0, pos) + "Step5";
    }

    $.ajax({
        url: url,
        data: createJSONFinal(),
        //data: retJson,
        cache: false,
        type: 'POST',
        success: function (data) {
            if (data.Status == "Success") {
                $("#step4").hide();
                $("#step5").show();

                AtualizarWizard(4);
                //$("#JsonForm").val(data.Data);
            }
            else if (data.Status == "Error") {

                $("#step4").hide();
                $("#step6").show();

                $("#msgErro").html(data.Data);

                AtualizarWizard(4);
            }
            else {
                $("#dadosEnderecoEvento").html(data);
            }
        },
        error: function (response) {

            alert(JSON.stringify(response.errors));
        }
    });
}

function AtualizarWizard(index) {
    var navigation = $(".steps");
    var total = navigation.find('li').length;
    var current = index + 1;
    // set wizard title
    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
    // set done steps
    jQuery('li', $('#form_wizard_1')).removeClass("done");
    jQuery('li', $('#form_wizard_1')).removeClass("active");
    var li_list = navigation.find('li');
    if (index == 0) {
        jQuery(li_list[0]).addClass("active")
    }

    for (var i = 0; i < index; i++) {
        jQuery(li_list[i]).addClass("done");
        jQuery(li_list[i + 1]).addClass("active")
    }

    var total = navigation.find('li').length;
    var current = index + 1;
    var $percent = (current / total) * 100;
    $('#form_wizard_1').find('.progress-bar').css({
        width: $percent + '%'
    });
}