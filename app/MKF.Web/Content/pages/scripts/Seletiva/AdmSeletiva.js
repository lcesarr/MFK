﻿$(document).ready(function () {
    AtualizarWizard(0);
    jsonObjEndereco = {};
    jsonObjJurado = {};
    jsonObjImagem = [];

    $("#btnSeletivaDadosNext").click(function (e) {
        e.preventDefault();

        $.ajax({
            url: RetornarUrl("Step1", "Step1"),
            data: $("form").serialize(),
            cache: false,
            type: 'POST',
            success: function (data) {
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    $("#step1").hide();
                    $("#step2").show();
                    AtualizarWizard(1);
                }
                else
                    $("#dadosSeletiva").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('change', '.cep', function (e) {

        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";
        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.erro) {
                    $("#dadosEnderecoSeletiva").find('input[type=text],select').each(function () {
                        switch ($(this).attr("name")) {
                            case "Logradouro":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "Bairro":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "Cidade":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            case "Estado":
                                $(this).val("");
                                $(this).prop('disabled', false);
                                break;
                            default:
                        }
                    });
                }
                else {
                    $("#dadosEnderecoSeletiva").find('input[type=text],select').each(function () {
                        switch ($(this).attr("name")) {
                            case "Logradouro":
                                $(this).val(data.logradouro);
                                if (data.logradouro != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "Bairro":
                                $(this).val(data.bairro);
                                if (data.bairro != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "Cidade":
                                $(this).val(data.localidade);
                                if (data.localidade != "")
                                    $(this).prop('disabled', true);
                                break;
                            case "Estado":
                                $(this).val(data.uf);
                                $(this).prop('disabled', true);
                                break;
                            default:
                        }
                    });
                }
            },
            error: function (response) {
                $("#dadosEnderecoSeletiva").find('input[type=text],select').each(function () {
                    switch ($(this).attr("name")) {
                        case "Logradouro":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "Bairro":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "Cidade":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        case "Estado":
                            $(this).val("");
                            $(this).prop('disabled', false);
                            break;
                        default:
                    }
                });
            }
        });
    });

    $("#btnContinueEndereco").click(function (e) {
        e.preventDefault();
        createJSONEndereco();
        $.ajax({
            url: RetornarUrl("Step2", "Step1"),
            data: jsonObjEndereco,
            cache: false,
            type: 'POST',
            success: function (data) {
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    $("#step2").hide();
                    $("#step3").show();
                    AtualizarWizard(2);
                }
                else {
                    $("#dadosEnderecoSeletiva").html(data);
                }
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnVoltarSeletiva").click(function (e) {
        e.preventDefault();
        $("#step2").hide();
        $("#step1").show();
        AtualizarWizard(0);
    });

    $("#btnContinueJurado").click(function (e) {
        e.preventDefault();

        createJSONFinal();
        $.ajax({
            url: RetornarUrl("Step3", "Step1"),
            data: JSON.stringify(jsonObjDados),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    SalvarRegistro();
                }
                else
                    $("#dadosResponsavel").html(data);
            },
            error: function (response) {

                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnVoltarJurado").click(function (e) {
        e.preventDefault();
        $("#step3").hide();
        $("#step2").show();
        AtualizarWizard(1);
    });

    $("#btnSeletivaDadosBack").click(function (e) {
        e.preventDefault();
        $("#step5").hide();
        $("#step3").show();
        AtualizarWizard(2);
    });
});

function createJSONDados() {
    jsonObjDados = {}
    $('#dadosSeletiva').find('input[type=text],input[type=hidden],select,textarea').each(function () {

        switch ($(this).attr("name")) {
            case "IdEvento":
                jsonObjDados["IdEvento"] = $(this).val();
                break;
            case "IdEndereco":
                jsonObjDados["IdEndereco"] = $(this).val();
                break;
            case "IdRegulamento":
                jsonObjDados["IdRegulamento"] = $(this).val();
                break;
            case "Nome":
                jsonObjDados["Nome"] = $(this).val();
                break;
            case "Descricao":
                jsonObjDados["Descricao"] = $(this).val();
                break;
            case "DataInicio":
                jsonObjDados["DataInicio"] = $(this).val();
                break;
            case "DataFim":
                jsonObjDados["DataFim"] = $(this).val();
                break;
            case "Valor":

                jsonObjDados["Valor"] = $(this).val();
                break;
            case "QtdInscricaoHorario":
                jsonObjDados["QtdInscricaoHorario"] = $(this).val();
                break;
            case "Id":
                jsonObjDados["Id"] = $(this).val();
                break;
            case "Url":
                jsonObjDados["Url"] = $(this).val();
                break;
            default:
        }
    });
}

function createJSONEndereco() {
    jsonObjEndereco = {};

    $("#dadosEnderecoSeletiva").find('input[type=text], input[type=hidden], select').each(function () {
        switch ($(this).attr("name")) {
            case "Tipo":
                jsonObjEndereco["Tipo"] = $(this).val();
                break;
            case "Cep":
                jsonObjEndereco["Cep"] = $(this).val();
                break;
            case "Logradouro":
                jsonObjEndereco["Logradouro"] = $(this).val();
                break;
            case "Numero":
                jsonObjEndereco["Numero"] = $(this).val();
                break;
            case "Complemento":
                jsonObjEndereco["Complemento"] = $(this).val();
                break;
            case "Bairro":
                jsonObjEndereco["Bairro"] = $(this).val();
                break;
            case "Cidade":
                jsonObjEndereco["Cidade"] = $(this).val();
                break;
            case "Estado":
                jsonObjEndereco["Estado"] = $(this).val();
                break;
            case "Id":
                jsonObjEndereco["Id"] = $(this).val();
                break;
            case "LocalSeletiva":
                jsonObjEndereco["LocalSeletiva"] = $(this).val();
                break;
            default:
        }
    });
}

function createJSONJurado() {
    var myObject = new Object();
    myObject.Jurado = new Object();
    myObject.Jurado.Pessoa = new Object();
    $('#dadosJuradosSeletiva').find('select').each(function () {

        switch ($(this).attr("name")) {
            case "IdJurado":
                myObject.Jurado.Pessoa.Nome = $(this).find('option:selected').text();;
                myObject.Jurado.Pessoa.Id = $(this).val();
                break;
            default:
        }
    });

    myObject.Jurados = new Object();

    $('#sample_1').find('tbody tr').each(function () {

    });

    return myObject;
}

function createJSONImagem() {
    jsonObjImagem = [];
    item = {}
    $('#dadosImagem').find('input[type=text],select').each(function () {
        switch ($(this).attr("name")) {
            case "item.Nome":
                item["Nome"] = $(this).val();
                break;
            case "item.Img":
                item["Img"] = $(this).val();
                break;
            case "item.Url":
                item["Url"] = $(this).val();
                jsonObjJurado.push(item);
                item = {};
                break;
            default:
        }
    });
}

function AtualizarWizard(index) {
    var navigation = $(".steps");
    var total = navigation.find('li').length;
    var current = index + 1;
    // set wizard title
    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
    // set done steps
    jQuery('li', $('#form_wizard_1')).removeClass("done");
    jQuery('li', $('#form_wizard_1')).removeClass("active");
    var li_list = navigation.find('li');
    if (index == 0) {
        jQuery(li_list[0]).addClass("active")
    }

    for (var i = 0; i < index; i++) {
        jQuery(li_list[i]).addClass("done");
        jQuery(li_list[i + 1]).addClass("active")
    }

    var total = navigation.find('li').length;
    var current = index + 1;
    var $percent = (current / total) * 100;
    $('#form_wizard_1').find('.progress-bar').css({
        width: $percent + '%'
    });
}

function createJSONFinal() {
    var myObject = new Object();
    createJSONDados();
    myObject = jsonObjDados;

    createJSONEndereco();
    myObject.Local = jsonObjEndereco;

    return myObject;
}

function SalvarRegistro() {
    $.ajax({
        url: RetornarUrl("Step4", "Step1"),
        data: createJSONFinal(),
        //data: retJson,
        cache: false,
        type: 'POST',
        success: function (data) {
            if (data.Status == "Success") {
                $(".field-validation-error").hide();
                $("#step3").hide();
                $("#step4").show();
                AtualizarWizard(3);
            }
            else if (data.Status == "Error") {

                $("#step3").hide();
                $("#step5").show();

                $("#msgErro").html(data.Data);

                AtualizarWizard(3);
            }
            else {
                $("#dadosEnderecoEvento").html(data);
            }
        },
        error: function (response) {

            alert(JSON.stringify(response.errors));
        }
    });
}

function RetornarUrl(step, stepAnt) {

    var url = $("#Url").val();

    if (url == "")
        url = step;
    else {
        var pos = url.indexOf(stepAnt);

        url = url.substring(0, pos) + step;
    }

    return url;
}