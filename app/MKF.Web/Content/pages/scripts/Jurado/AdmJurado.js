﻿$(document).ready(function () {
    AtualizarWizard(0);
    jsonObjDadosPessoa = {}
    jsonObjDadosUsuario = {}
    jsonObjEndereco = {}
    jsonObjContato = [];

    $("#btnJuradoDadosNext").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: 'Step1',
            data: $("form").serialize(),
            cache: false,
            type: 'POST',
            success: function (data) {
                if (data.Status == "Success") {
                    $(".field-validation-error").hide();
                    $("#step1").hide();
                    $("#step2").show();
                    AtualizarWizard(1);
                }
                else
                    $("#dadosJurado").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('change', '.select', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var obj = $("#" + id)
        var tipo = "";
        obj.find('input[type=text],select').each(function () {
            switch ($(this).attr("name")) {
                case "item.Dado":
                    if (tipo == "T") {
                        $(this).inputmask("(99) 9999-9999", {
                            autoUnmask: true
                        });
                    }
                    else if (tipo == "C") {
                        $(this).inputmask("(99) 99999-9999", {
                            autoUnmask: true
                        });
                    }
                    else {
                        $(this).inputmask('remove');;
                    }
                    break;
                case "item.Tipo":
                    tipo = $(this).val();
                    break;
            }
        });
    });

    $(document).on('change', '.cep', function (e) {
        e.preventDefault();
        var url = "https://viacep.com.br/ws/" + $(this).val() + "/json/";
        var item = $(this).data("id");
        $.ajax({
            url: url,
            data: JSON.stringify(jsonObjEndereco),
            cache: false,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosEnderecoJurado").find('input[type=text],select').each(function () {
                    switch ($(this).attr("name")) {
                        case "Cep":
                            $(this).prop('disabled', true);
                            break;
                        case "Logradouro":
                            $(this).val(data.logradouro);
                            $(this).prop('disabled', true);
                            break;
                        case "Bairro":
                            $(this).val(data.bairro);
                            $(this).prop('disabled', true);
                            break;
                        case "Cidade":
                            $(this).val(data.localidade);
                            $(this).prop('disabled', true);
                            break;
                        case "Estado":
                            $(this).val(data.uf);
                            $(this).prop('disabled', true);
                            break;
                        default:
                    }
                });
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnJuradoEnderecoNext").click(function (e) {
        e.preventDefault();
        createJSONEndereco();
        $.ajax({
            url: 'Step2',
            data: jsonObjEndereco,
            cache: false,
            type: 'POST',
            success: function (data) {
                if (data.Status == "Success") {
                    $("#step2").hide();
                    $("#step3").show();
                    AtualizarWizard(2);
                }
                else {
                    $("#dadosEndereco").html(data);
                }
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnJuradoEnderecoBack").click(function (e) {
        e.preventDefault();
        $("#step2").hide();
        $("#step1").show();
        AtualizarWizard(0);
    });

    $("#btnNovoContatoJurado").click(function (e) {
        e.preventDefault();
        createJSONContato();

        $.ajax({
            url: 'NovoContato',
            data: JSON.stringify(jsonObjContato),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosContatoJurado").html("");
                $("#dadosContatoJurado").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $(document).on('click', '.btnRemover', function (e) {

        e.preventDefault();
        createJSONContato();
        var item = $(this).data("id");
        delete jsonObjContato[item - 1];

        $.ajax({
            url: 'RemoverContato',
            data: JSON.stringify(jsonObjContato),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#dadosContatoJurado").html("");
                $("#dadosContatoJurado").html(data);
            },
            error: function (response) {

                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnJuradoContatoNext").click(function (e) {
        e.preventDefault();
        createJSONContato();
        $.ajax({
            url: 'Step3',
            data: JSON.stringify(jsonObjContato),
            cache: false,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.Status == "Success") {
                    SalvarRegistro();
                }
                else
                    $("#dadosContatoJurado").html(data);
            },
            error: function (response) {
                alert(JSON.stringify(response.errors));
            }
        });
    });

    $("#btnJuradoContatoBack").click(function (e) {
        e.preventDefault();
        $("#step3").hide();
        $("#step2").show();
        AtualizarWizard(1);
    });
});

function SalvarRegistro() {
    $.ajax({
        url: 'Step4',
        data: createJSONFinal(),
        cache: false,
        type: 'POST',
        success: function (data) {

            if (data.Status == "Success") {
                $("#step3").hide();
                $("#step4").show();

                AtualizarWizard(3);
            }
            else {
                $("#dadosEnderecoEvento").html(data);
            }
        },
        error: function (response) {

            alert(JSON.stringify(response.errors));
        }
    });
}

function createJSONFinal() {

    var myObject = new Object();
    createJSONDadosPessoa();
    myObject.Pessoa = jsonObjDadosPessoa;

    createJSONDadosUsuario();
    myObject.Usuario = jsonObjDadosUsuario;

    createJSONEndereco();
    myObject.Endereco = jsonObjEndereco;

    createJSONContato();
    myObject.Contatos = jsonObjContato;

    return myObject;
}

function createJSONEndereco() {
    jsonObjEndereco = {};
    $('#dadosEnderecoJurado').find('input[type=text],select').each(function () {
        
        switch ($(this).attr("name")) {
            case "Tipo":
                jsonObjEndereco["Tipo"] = $(this).val();
                break;
            case "Cep":
                jsonObjEndereco["Cep"] = $(this).val();
                break;
            case "Logradouro":
                jsonObjEndereco["Logradouro"] = $(this).val();
                break;
            case "Numero":
                jsonObjEndereco["Numero"] = $(this).val();
                break;
            case "Complemento":
                jsonObjEndereco["Complemento"] = $(this).val();
                break;
            case "Bairro":
                jsonObjEndereco["Bairro"] = $(this).val();
                break;
            case "Cidade":
                jsonObjEndereco["Cidade"] = $(this).val();
                break;
            case "Estado":
                jsonObjEndereco["Estado"] = $(this).val();
                break;
            default:
        }
    });
}

function createJSONDadosPessoa() {
    jsonObjDadosPessoa = {}
    $('#dadosJurado').find('input[type=text],input[type=hidden],select').each(function () {

        switch ($(this).attr("name")) {
            case "Pessoa.Nome":
                jsonObjDadosPessoa["Nome"] = $(this).val();
                break;
            case "Pessoa.Cpf":
                jsonObjDadosPessoa["Cpf"] = $(this).val();
                break;
            case "Pessoa.Id":
                jsonObjDadosPessoa["Id"] = $(this).val();
            case "Pessoa.Tipo":
                jsonObjDadosPessoa["Tipo"] = $(this).val();
                break;
            default:
        }
    });
}

function createJSONDadosUsuario() {
    jsonObjDadosUsuario = {}
    $('#dadosJurado').find('input[type=text],input[type=hidden],select,input[type=password]').each(function () {
        
        switch ($(this).attr("name")) {
            case "Usuario.User":
                jsonObjDadosUsuario["User"] = $(this).val();
                break;
            case "Usuario.Password":
                jsonObjDadosUsuario["Password"] = $(this).val();
                break;
            case "Usuario.TipoAcesso":
                jsonObjDadosUsuario["TipoAcesso"] = $(this).val();
                item = {};
                break;
            case "Usuario.Senha":
                jsonObjDadosUsuario["Senha"] = $(this).val();
                item = {};
                break;
            default:
        }
    });
}

function createJSONContato() {

    jsonObjContato = [];
    item = {}
    $('#dadosContatoJurado').find('input[type=text],select').each(function () {
        switch ($(this).attr("name")) {
            case "item.Tipo":
                item["Tipo"] = $(this).val();
                break;
            case "item.Dado":
                item["Dado"] = $(this).val();
                jsonObjContato.push(item);
                item = {};
                break;
            default:
        }
    });
}

function AtualizarWizard(index) {
    var navigation = $(".steps");
    var total = navigation.find('li').length;
    var current = index + 1;
    // set wizard title
    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
    // set done steps
    jQuery('li', $('#form_wizard_1')).removeClass("done");
    jQuery('li', $('#form_wizard_1')).removeClass("active");
    var li_list = navigation.find('li');
    if (index == 0) {
        jQuery(li_list[0]).addClass("active")
    }

    for (var i = 0; i < index; i++) {
        jQuery(li_list[i]).addClass("done");
        jQuery(li_list[i + 1]).addClass("active")
    }

    var total = navigation.find('li').length;
    var current = index + 1;
    var $percent = (current / total) * 100;
    $('#form_wizard_1').find('.progress-bar').css({
        width: $percent + '%'
    });
}