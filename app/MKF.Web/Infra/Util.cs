﻿using System;
using System.Security.Claims;
using System.Text;
using System.Threading;

namespace MKF.Web.Infra
{
    public static class Util
    {
        public static string CriptografaSenha(string senha)
        {
            using (System.Security.Cryptography.SHA256 sha256Hash = System.Security.Cryptography.SHA256.Create())
            {
                // Convert the valor string to a byte array and compute the hash.
                byte[] data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(senha));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        public static string RecuperarUsuario()
        {
            ClaimsPrincipal currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            foreach (Claim ci in currentPrincipal.Claims)
            {
                if (ci.ToString().Contains("name"))
                    return ci.Value;
            }

            return string.Empty;
        }

        public static string RecuperarIdUsuario()
        {
            ClaimsPrincipal currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            foreach (Claim ci in currentPrincipal.Claims)
            {
                if (ci.ToString().Contains("nameidentifier"))
                    return ci.Value;
            }

            return string.Empty;
        }

        public static int? RecuperarIdEmpresa()
        {
            ClaimsPrincipal currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            foreach (Claim ci in currentPrincipal.Claims)
            {
                if (ci.ToString().Contains("Sid"))
                    return int.Parse(ci.Value);
            }

            return null;
        }

        public static int? RecuperarIdPessoa()
        {
            ClaimsPrincipal currentPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            foreach (Claim ci in currentPrincipal.Claims)
            {
                if (ci.ToString().Contains("IdPessoa"))
                    return int.Parse(ci.Value);
            }

            return null;
        }

        public static double ValidaSeveridadeAplicada(DateTime dtAbertura, int tempoAtendimento)
        {
            TimeSpan prox = dtAbertura.AddMinutes(tempoAtendimento).Subtract(dtAbertura);
            TimeSpan por = dtAbertura.AddMinutes(tempoAtendimento).Subtract(DateTime.Now);
            var c = prox.TotalDays - por.TotalDays;

            return (c * 100) / prox.TotalDays;
        }

        public static string FormatarCPFCNPJ(string valor, string tipo)
        {
            if (string.IsNullOrEmpty(valor)) return string.Empty;

            if (tipo.Equals("CNPJ"))
                return Convert.ToUInt64(Funcoes.LimparFormatacao(valor)).ToString(@"00\.000\.000\/0000-00");
            else
                return Convert.ToUInt64(Funcoes.LimparFormatacao(valor)).ToString(@"000\.000\.000\-00");
        }

        public static string RetornarFormaPagamento(string valor)
        {
            switch (valor)
            {
                case "1":
                    return "Cartão de crédito";
                case "2":
                    return "Boleto";
                case "3":
                    return "Débito online (TEF)";
                case "4":
                    return "Saldo PagSeguro";
                case "5":
                    return "Oi Paggo";
                case "7":
                    return "Depósito em conta";
                case "8":
                    return "Cortesia";
                default:
                    return string.Empty;
            }
        }

        public static string RetornarIconFormaPagamento(string valor)
        {
            switch (valor)
            {
                case "1":
                    return "fa fa-credit-card";
                case "2":
                    return "fa fa-barcode ";
                case "3":
                    return "fa fa-random";
                case "4":
                    return "fa fa-plus";
                case "5":
                    return "fa fa-mobile-phone";
                case "7":
                    return "fa fa-bank";
                case "8":
                    return "fa fa-bank";
                default:
                    return string.Empty;
            }
        }

        public static string RetornarStatusPagamento(string valor)
        {
            switch (valor)
            {
                case "1":
                    return "Aguardando pagamento";
                case "2":
                    return "Em análise";
                case "3":                    
                case "4":
                    return "Paga";
                case "5":
                    return "Em disputa";
                case "6":
                    return "Devolvida";
                case "7":
                    return "Cancelada";
                case "8":
                    return "Cortesia";
                default:
                    return string.Empty;
            }
        }

        public static string RetornarColorStatusPagamento(string valor)
        {
            switch (valor)
            {
                case "1":
                case "2":
                    return " orange2";
                case "3":
                case "4":
                    return " green";
                case "5":
                case "6":
                case "8":
                    return " blue";
                case "7":
                    return " red";
                default:
                    return string.Empty;
            }
        }

        public static string RetornarIconStatusPagamento(string valor)
        {
            switch (valor)
            {
                case "1":
                case "2":
                    return " fa fa-minus";
                case "3":
                case "4":
                    return " fa fa-check";
                case "5":
                case "6":
                case "8":
                    return " fa fa-asterisk";
                case "7":
                    return " fa fa-remove";
                default:
                    return string.Empty;
            }
        }

        public static string FormatarData(DateTime? data)
        {
            return data.HasValue ? data.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
        }

        public static int CalculateIdade(DateTime DataNascimento)
        {

            int anos = DateTime.Now.Year - DataNascimento.Year;

            if (DateTime.Now.Month < DataNascimento.Month || (DateTime.Now.Month == DataNascimento.Month && DateTime.Now.Day < DataNascimento.Day))
                anos--;

            return anos;
        }

        public static string RetornarSexo(string valor)
        {
            switch (valor)
            {
                case "F":
                    return "Feminino";
                case "M":
                    return "Masculino";
                default:
                    return string.Empty;
            }
        }
    }
}