﻿using MKF.Domain.Entidades.Util;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Infra
{
    public class Funcoes
    {
        public static SelectListItem[] Ufs(string valor)
        {
            string[] estados = new String[]
            {
                "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA",
                "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO"
            };

            return estados.Select(a => new SelectListItem() { Value = a, Text = a, Selected = valor != null && valor.Equals(a) }).ToArray();
        }

        public static SelectListItem[] StatusList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Ativo", Value = "0" });
            list.Add(new SelectListItem { Text = "Inativo", Value = "1" });

            return list.ToArray();
        }

        public static SelectListItem[] EnderecoList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Residencial", Value = "R" });
            list.Add(new SelectListItem { Text = "Comercial", Value = "C" });
            list.Add(new SelectListItem { Text = "Outros", Value = "O" });

            return list.ToArray();
        }

        public static SelectListItem[] ContatoList(string item)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Telefone", Value = "T", Selected = item != null && item.Equals("T") });
            list.Add(new SelectListItem { Text = "Celular", Value = "C", Selected = item != null && item.Equals("C") });
            list.Add(new SelectListItem { Text = "E-mail", Value = "E", Selected = item != null && item.Equals("E") });

            return list.ToArray();
        }

        public static SelectListItem[] RedeSocialList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Facebook", Value = "F" });
            list.Add(new SelectListItem { Text = "Twitter", Value = "T" });
            list.Add(new SelectListItem { Text = "Google +", Value = "G" });
            list.Add(new SelectListItem { Text = "Instagram", Value = "I" });

            return list.ToArray();
        }

        public static SelectListItem[] TiposRegulamentosList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Evento", Value = "E" });
            list.Add(new SelectListItem { Text = "Seletiva", Value = "S" });
            list.Add(new SelectListItem { Text = "Autorização de Imagem", Value = "I" });
            list.Add(new SelectListItem { Text = "Pagamento", Value = "P" });

            return list.ToArray();
        }

        public static SelectListItem[] EventosList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var ev = Comum.Listar(string.Empty, "Evento");
            List<EventoModels> eventos = JsonConvert.DeserializeObject<List<EventoModels>>(ev.Result);

            foreach (EventoModels item in eventos)
            {
                if (item.Status == 0)
                    list.Add(new SelectListItem { Text = item.Nome, Value = item.Id.ToString() });
            }

            list.Insert(0, new SelectListItem { Text = "Selecione", Value = "0" });

            return list.ToArray();
        }

        public static SelectListItem[] JuradosList(int idSeletiva)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            var jurado = Comum.ListarPor<PessoaModels>(new PessoaModels { Tipo = "J" }, "Pessoa");

            List<PessoaModels> jurados = JsonConvert.DeserializeObject<List<PessoaModels>>(jurado);

            var jurSeletiva = RelJuradosSeletiva.ListarJuradosSeletiva(idSeletiva);

            List<RelJuradosSeletivaModel> jurSel = JsonConvert.DeserializeObject<List<RelJuradosSeletivaModel>>(jurSeletiva.Result);

            foreach (PessoaModels item in jurados)
            {
                if (item.Status == 0)
                    list.Add(new SelectListItem { Text = item.Nome, Value = item.Id.ToString(), Selected = jurSel != null && jurSel.Count > 0 ? jurSel.Count(a => a.IdPessoa == item.Id) > 0 : false });
            }

            return list.ToArray();
        }

        public static SelectListItem[] RegulamentosList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            var regulamento = Comum.Listar("", "Regulamento");

            List<PessoaModels> regulamentos = JsonConvert.DeserializeObject<List<PessoaModels>>(regulamento.Result);

            foreach (PessoaModels item in regulamentos)
            {
                if (item.Status == 0)
                    list.Add(new SelectListItem { Text = item.Nome, Value = item.Id.ToString() });
            }

            return list.ToArray();
        }

        public static SelectListItem[] ParentescoList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Mãe", Value = "M" });
            list.Add(new SelectListItem { Text = "Pai", Value = "P" });
            list.Add(new SelectListItem { Text = "Responsável Legal", Value = "R" });

            return list.ToArray();
        }

        public static SelectListItem[] HorarioApresentacaoList(DateTime dtInicio, DateTime dtFim, int tempoApresentacao, int idSeletiva, int idEvento, string tipo)
        {
            DateTime dtControle = dtInicio;
            List<SelectListItem> list = new List<SelectListItem>();

            List<QuantidadeApresentacaoHora> registros = new List<QuantidadeApresentacaoHora>();

            var ret = Servicos.InscricaoSeletivaSrv.ListarQtdInscricao(idSeletiva, idEvento);

            registros = JsonConvert.DeserializeObject<List<QuantidadeApresentacaoHora>>(ret.Result);

            registros = registros.Where(a => a.Total >= tempoApresentacao).ToList();

            while (dtControle < dtFim)
            {
                if (registros != null && registros.Count() > 0)
                {
                    var dado = registros.Where(a => a.HorarioDesfile == dtControle).FirstOrDefault();
                    if (dado != null)
                    {
                        dtControle = dtControle.AddMinutes(tempoApresentacao);
                        continue;
                    }
                }

                if (dtControle.ToString("HH").Equals("12") && tipo.Equals("I"))
                {
                    dtControle = dtControle.AddMinutes(tempoApresentacao);
                    continue;
                }

                list.Add(new SelectListItem { Text = dtControle.ToString("dd/MM/yyyy HH:mm"), Value = dtControle.ToString("dd/MM/yyyy HH:mm:ss") });
                dtControle = dtControle.AddMinutes(tempoApresentacao);
            }

            return list.ToArray();
        }

        public static SelectListItem[] HorarioFiltroList(int idSeletiva)
        {
            var sel = Comum.Listar(string.Empty, "Seletiva");
            List<SeletivaViewModels> lstSeletiva = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(sel.Result);

            SeletivaViewModels Seletiva = lstSeletiva.Count() > 0 ? lstSeletiva.FirstOrDefault(a => a.Id == idSeletiva) : new Models.SeletivaViewModels();
            int tempoApresentacao = Seletiva.QtdInscricaoHorario;
            DateTime dtControle = Seletiva.DataInicio.Value;
            List<SelectListItem> list = new List<SelectListItem>();

            List<QuantidadeApresentacaoHora> registros = new List<QuantidadeApresentacaoHora>();

            var ret = Servicos.InscricaoSeletivaSrv.ListarQtdInscricao(Seletiva.Id, Seletiva.IdEvento);

            registros = JsonConvert.DeserializeObject<List<QuantidadeApresentacaoHora>>(ret.Result);

            registros = registros.Where(a => a.Total >= tempoApresentacao).ToList();

            while (dtControle < Seletiva.DataFim)
            {
                if (registros != null && registros.Count() > 0)
                {
                    var dado = registros.Where(a => a.HorarioDesfile == dtControle).FirstOrDefault();
                    if (dado != null)
                    {
                        dtControle = dtControle.AddMinutes(tempoApresentacao);
                        continue;
                    }
                }

                list.Add(new SelectListItem { Text = dtControle.ToString("dd/MM/yyyy HH:mm"), Value = dtControle.ToString("dd/MM/yyyy HH:mm:ss") });
                dtControle = dtControle.AddMinutes(tempoApresentacao);
            }

            return list.ToArray();
        }

        public static SelectListItem[] SexoList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Feminino", Value = "F" });
            list.Add(new SelectListItem { Text = "Masculino", Value = "M" });

            return list.ToArray();
        }

        public static SelectListItem[] TipoEmpresaList(string item)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Grife", Value = "G", Selected = item != null && item.Equals("G") });
            list.Add(new SelectListItem { Text = "Lojista", Value = "L", Selected = item != null && item.Equals("L") });
            list.Add(new SelectListItem { Text = "Parceiro", Value = "P", Selected = item != null && item.Equals("P") });
            list.Add(new SelectListItem { Text = "Patrocinador", Value = "T", Selected = item != null && item.Equals("T") });

            return list.ToArray();
        }

        public static SelectListItem[] StatusPagamentoList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Pendente", Value = "P" });
            list.Add(new SelectListItem { Text = "Aguardando pagamento", Value = "1" });
            list.Add(new SelectListItem { Text = "Em análise", Value = "2" });
            list.Add(new SelectListItem { Text = "Paga", Value = "3" });
            list.Add(new SelectListItem { Text = "Disponível", Value = "4" });
            list.Add(new SelectListItem { Text = "Em disputa", Value = "5" });
            list.Add(new SelectListItem { Text = "Devolvida", Value = "6" });
            list.Add(new SelectListItem { Text = "Cancelada", Value = "7" });
            list.Add(new SelectListItem { Text = "Cortesia", Value = "8" });

            return list.ToArray();
        }

        public static string LimparFormatacao(string valor)
        {
            return string.IsNullOrEmpty(valor) ? string.Empty : valor.Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("_", "").Replace(" ", "").Replace(".", "").Trim();
        }

        public static bool ValidarEmail(string valor, bool requerido)
        {
            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            if (requerido && string.IsNullOrEmpty(valor)) return false;

            if (!string.IsNullOrEmpty(valor) && !rg.IsMatch(valor))
                return false;

            return true;
        }

        public static bool ValidarTelefone(string valor, bool requerido)
        {
            valor = LimparFormatacao(valor);
            if (requerido && string.IsNullOrEmpty(valor)) return false;

            try
            {
                Int64.Parse(valor);
            }
            catch
            {
                return false;
            }

            if (!string.IsNullOrEmpty(valor) && (valor.Length < 10 || valor.Length > 11))
                return false;

            return true;
        }
    }
}