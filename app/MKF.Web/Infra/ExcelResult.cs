﻿using MKF.Web.Infra.Servicos;
using MKF.Web.Models.Relatorios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Infra
{
    public class ExcelResult : ActionResult
    {
        public string FileName { get; set; }

        public string Item { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Buffer = true;

            context.HttpContext.Response.Clear();

            context.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + FileName);

            context.HttpContext.Response.ContentType = "application/vnd.ms-excel";
                        
            context.HttpContext.Response.Write(Item);
        }
    }
}