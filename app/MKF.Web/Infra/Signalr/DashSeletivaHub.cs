﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MKF.Domain.Entidades.DashBoards;
using MKF.Web.Infra.Servicos;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace MKF.Web.Infra.Signalr
{
    [HubName("DashSeletivaHub")]
    public class DashSeletivaHub : Hub
    {
        static List<UserHub> ConnectedUsers = new List<UserHub>();
        private readonly ConcurrentDictionary<string, InscricaoSeletiva> _dadosSeletiva = new ConcurrentDictionary<string, InscricaoSeletiva>();

        static Timer _timerTransacoes;
        private readonly TimeSpan _intervaloTransacoes = TimeSpan.FromMilliseconds(10000);

        public void Connect()
        {
            var id = Context.ConnectionId;

            var _tempUsers = ConnectedUsers; //Cria uma variavel temp para manipular os dados e depois atribuir novamente o valor a ConnectedUsers 
            UserHub usuario = _tempUsers.Where(x => x.ConnectionId == id).FirstOrDefault();

            //Trata o usuário na coleção de usuários conectados
            if (usuario == null)
                usuario = new UserHub(); //Novo usuário
            else
                ConnectedUsers.Remove(usuario); //Alteração de org

            usuario.ConnectionId = id;
            ConnectedUsers.Add(usuario);

            ConnectedUsers = _tempUsers;

            //Consulta as transações da org
            var dados = ObterDadosSeletiva();
            if (!_dadosSeletiva.ContainsKey(id))
                _dadosSeletiva.TryAdd(id.ToString(), dados);

            //Retorna as informações pro usuário
            Clients.Caller.onConnected(dados);

            if (_timerTransacoes == null)
                _timerTransacoes = new Timer(AtualizarDashboard, null, _intervaloTransacoes, _intervaloTransacoes);
        }

        public void AtualizarDashboard(object obj)
        {
            try
            {
                if (!ConnectedUsers.Any())
                    return;

                var _tempUsers = ConnectedUsers; //Cria uma variavel temp para manipular os dados de usuário
                var _situacao = _tempUsers.Select(x => x.ConnectionId).Distinct();

                foreach (string situacao in _situacao)
                {
                    var dadosAtualizados = ObterDadosSeletiva();

                    try
                    {

                        var ultimoPeriodoAtualizado = dadosAtualizados;

                        if (!_dadosSeletiva.ContainsKey(situacao))//Adiciona os dados atualizados em uma lista de informações da org.
                            _dadosSeletiva.TryAdd(situacao, dadosAtualizados);
                        else
                        {
                            var dados = _dadosSeletiva[situacao];

                            //Caso os dados atualizados estejam iguais aos valores anteriores, nada acontece.
                            var sel = dadosAtualizados.StatusPagamento.Sum(a => a.Total);
                            var selDados = dados.StatusPagamento.Sum(a => a.Total);

                            var selPg = dadosAtualizados.StatusPagamento.Sum(a => a.Pagas);
                            var selPgA = dados.StatusPagamento.Sum(a => a.Pagas);

                            var selEa = dadosAtualizados.StatusPagamento.Sum(a => a.EmAnalise);
                            var selEaA = dados.StatusPagamento.Sum(a => a.EmAnalise);

                            var selNc = dadosAtualizados.StatusPagamento.Sum(a => a.NaoConfirmadas);
                            var selNcA = dados.StatusPagamento.Sum(a => a.NaoConfirmadas);

                            if (dados.StatusPagamento.Count() == dadosAtualizados.StatusPagamento.Count() && sel == selDados && selPg == selPgA && selEa == selEaA && selNc == selNcA)
                                continue;
                            else
                            {
                                _dadosSeletiva.TryRemove(situacao, out dados);
                                _dadosSeletiva.TryAdd(situacao, dadosAtualizados);
                            }
                        }

                        //Filtra todos os usuários conectados pela org
                        var usuariosConectados = ConnectedUsers.Where(x => x.ConnectionId == situacao);
                        //Envia atualização dos dados
                        Clients.Clients(usuariosConectados.Select(p => p.ConnectionId).ToList()).onUpdated(ultimoPeriodoAtualizado);
                    }
                    catch (Exception ex)
                    {
                        //Em caso de erro, atualiza todos os períodos, para os usuários conectados a org                    
                        var usuariosConectados = ConnectedUsers.Where(x => x.ConnectionId == situacao);
                        Clients.Clients(usuariosConectados.Select(p => p.ConnectionId).ToList()).onConnected(dadosAtualizados);

                        continue;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public InscricaoSeletiva ObterDadosSeletiva()
        {
            InscricaoSeletiva stocks = new InscricaoSeletiva();

            var dashRet = DashSeletivaSrv.Listar();

            stocks.StatusPagamento = JsonConvert.DeserializeObject<List<StatusPgInscricaoSeletiva>>(dashRet.Result);

            var dashRetTpPagamento = DashSeletivaSrv.ListarTipoPagamento();

            stocks.TipoPagamento = JsonConvert.DeserializeObject<List<TipoPgInscricaoSeletiva>>(dashRetTpPagamento.Result);

            return stocks;
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                ConnectedUsers.Remove(item);
            }

            return base.OnDisconnected(stopCalled);
        }
    }

    public class UserHub
    {
        public string ConnectionId { get; set; }
    }
}