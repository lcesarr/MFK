﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace MKF.Web.Infra.Servicos
{
    public class RelJuradosSeletiva
    {
        public static dynamic ListarJuradosSeletiva(int idSeletiva)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/JuradosSeletiva/Buscar/{1}", url, idSeletiva);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static bool DeletarJuradosSeletiva(int idSeletiva)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/JuradosSeletiva/Deletar/{1}", url, idSeletiva);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "leandro");

                //POST
                HttpResponseMessage response = client.DeleteAsync(api).Result;
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
        }
        
        public static dynamic ListarSeletivasJurado(int idJurados)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/JuradosSeletiva/Seletiva/{1}", url, idJurados);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }
    }
}