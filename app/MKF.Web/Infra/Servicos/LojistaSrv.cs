﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace MKF.Web.Infra.Servicos
{
    public class LojistaSrv
    {
        public static dynamic ListarTodos()
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}/BuscarTodos", url, "Lojista");

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarPorId(int id)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/Lojista/BuscarLojista/{1}", url, id);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }
    }
}