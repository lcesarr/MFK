﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace MKF.Web.Infra.Servicos
{
    public class RelSeletivaSrv
    {
        public static dynamic ListarInscSeletiva(string nome, string inscricao, int evento, int seletiva, string status)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/Relatorio/Buscar/InsSeletiva/{1}/{2}/{3}/{4}/{5}", url, nome, inscricao, evento, seletiva, status);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarMapaSeletiva(int idSeletiva)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/Relatorio/Buscar/MapaSeletiva/{1}", url, idSeletiva);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarAprovadosSeletiva(int idSeletiva, int idademin, int idademax)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/Relatorio/Buscar/AprovadosSeletiva/{1}/{2}/{3}", url, idSeletiva, idademin, idademax);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarAprovadosSeletivaPdf(int idSeletiva, int idademin, int idademax)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/Relatorio/Buscar/AprovadosSeletivaPdf/{1}/{2}/{3}", url, idSeletiva, idademin, idademax);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }        
    }
}