﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MKF.Web.Infra.Servicos
{
    public class Comum
    {
        public static int Gravar<T>(T obj, string control)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}", url, control);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "leandro");

                //POST
                HttpResponseMessage response = client.PostAsJsonAsync(api, obj).Result;
                if (response.IsSuccessStatusCode)
                    return int.Parse(response.Content.ReadAsStringAsync().Result);
                else
                    return 0;
            }
        }

        public static dynamic Listar(string obj, string control)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}", url, control);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            //var content = new StringContent(obj, Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var ProdutoJsonString = response.Content.ReadAsStringAsync();
                return ProdutoJsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarPorId(int id, string control)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}/{2}", url, control, id);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            //var content = new StringContent(obj, Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var ProdutoJsonString = response.Content.ReadAsStringAsync();
                return ProdutoJsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarPor<T>(T obj, string control)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}/Buscar/Por", url, control);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "leandro");

                //POST
                HttpResponseMessage response = client.PostAsJsonAsync(api, obj).Result;
                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsStringAsync().Result;
                else
                    return "";
            }
        }

        public static bool Excluir(int id, string control)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}/{2}", url, control, id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "leandro");

                //POST
                HttpResponseMessage response = client.DeleteAsync(api).Result;
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
        }

        public static bool Alterar<T>(T obj, string control)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}", url, control);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "leandro");

                //POST
                HttpResponseMessage response = client.PutAsJsonAsync(api, obj).Result;
                if (response.IsSuccessStatusCode)
                    return Boolean.Parse(response.Content.ReadAsStringAsync().Result);
                else
                    return false;
            }
        }

        public static dynamic ListarPorIdUnion(int id, string control, string metodo)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}/{2}/{3}", url, control, metodo, id);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }

        public static dynamic ListarTodosUnion(string control, string metodo)
        {
            string url = ConfigurationManager.AppSettings["urlapi"];
            string api = string.Format("{0}/api/{1}/{2}", url, control, metodo);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "leandro");

            HttpResponseMessage response = client.GetAsync(api).Result;

            //se retornar com sucesso busca os dados
            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString;
            }

            //Se der erro na chamada, mostra o status do código de erro.
            else
                return string.Empty;
        }
    }
}