﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace MKF.Web.Controllers
{
    public class InscricaoController : Controller
    {
        private string caminhoImagensCrianca = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Content\\ImagensCrianca\\";

        public JsonResult SalvarImagemCrianca()
        {
            try
            {

                int idCrianca = Convert.ToInt32(TempData["IdCrianca"]);
                bool ok = false;
                // Cria o diretório que irá armazenar as imagens das crianças
                if (!Directory.Exists(caminhoImagensCrianca))
                    Directory.CreateDirectory(caminhoImagensCrianca);

                string diretorioCrianca = caminhoImagensCrianca + idCrianca;

                // Cria a pasta epecífica da criança
                if (!Directory.Exists(diretorioCrianca))
                    Directory.CreateDirectory(diretorioCrianca);

                int cont = 0;

                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase postedFile = Request.Files[fileName];

                    if (postedFile.ContentLength > 0)
                    {
                        // UnitOfWorkBLL.EmpresaBLL.AlterarLogo(Convert.ToInt32(CookieHelper.CODIGO), postedFile.FileName);

                        // Salva a imagaem da criança em sua respectiva pasta
                        postedFile.SaveAs(diretorioCrianca + "\\" + postedFile.FileName);

                        string url = string.Format("http://www.minasfashionkids.com.br/mfk/Content/ImagensCrianca/{0}/{1}", idCrianca, postedFile.FileName);

                        Comum.Gravar<ImagemModels>(new ImagemModels { Altura = 0, IdPessoa = idCrianca, Largura = 0, Nome = postedFile.FileName, Tipo = cont == 0 ? "0" : "1", URL = url }, "Imagem");
                        cont++;
                    }
                }

                return Json(new { Success = true });
            }
            catch (Exception)
            {

                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do endereço da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Inscricao
        public ActionResult Index()
        {
            List<SeletivaViewModels> seletivas = new List<SeletivaViewModels>();

            var ev = Comum.ListarTodosUnion("Seletiva", "BuscarTodos");

            seletivas = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(ev.Result);

            return View(seletivas.Where(s => s.Status == 0).ToList());
        }

        // GET: Inscricao
        public ActionResult Create(int id, string url)
        {
            InscricaoViewModel inscricao = new Models.InscricaoViewModel();
            inscricao.Modelo.Url = url;
            SeletivaViewModels seletiva = new Models.SeletivaViewModels();

            var ev = Comum.ListarPorIdUnion(id, "Seletiva", "BuscarSeletiva");

            seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(ev.Result);

            var reg = Comum.ListarPorId(seletiva.IdRegulamento, "Regulamento");

            seletiva.Regulamento = JsonConvert.DeserializeObject<RegulamentoViewModels>(reg.Result);
            seletiva.Regulamento.Dados = seletiva.Regulamento.Dados.Replace("{datainicio}", seletiva.DataInicio.Value.ToString("dd/MM/yyyy HH:mm")).Replace("{localseletiva}", seletiva.Local.LocalSeletiva).Replace("{datafim}", seletiva.DataFim.Value.ToString("dd/MM/yyyy HH:mm"));
            inscricao.Seletiva = seletiva;

            var aut = Comum.Listar("", "Regulamento");

            List<RegulamentoViewModels> ret = JsonConvert.DeserializeObject<List<RegulamentoViewModels>>(aut.Result);
            inscricao.Modelo.Autorizacao = ret.Where(a => a.Tipo == "I").FirstOrDefault();
            inscricao.Modelo.ValorSeletiva = seletiva.Valor;
            inscricao.Modelo.SeletivaNome = seletiva.Nome;
            inscricao.Modelo.IdSeletiva = seletiva.Id;
            inscricao.Modelo.IdEvento = seletiva.IdEvento;

            inscricao.Modelo.DataInicioSeletiva = seletiva.DataInicio.Value;
            inscricao.Modelo.DataFimSeletiva = seletiva.DataFim.Value;
            inscricao.Modelo.IntervaloApresentacao = seletiva.QtdInscricaoHorario;

            inscricao.Pagamento.ValorPagamento = seletiva.Valor;
            inscricao.Pagamento.IdSeletiva = seletiva.Id;

            return View(inscricao);
        }

        // Post: Inscricao
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(InscricaoViewModel model)
        {
            int idCrianca = 0;
            int idCriancaEndereco = 0;
            int idModelo = 0;
            int idManequim = 0;
            int idContatoCrianca = 0;
            int idRedeSocialCrianca = 0;

            int idResp1 = 0;
            int idResp1Endereco = 0;
            int idResp1Telefone = 0;
            int idResp1RedeSocial = 0;

            int idResp2 = 0;
            int idResp2Endereco = 0;
            int idResp2Telefone = 0;
            int idResp2RedeSocial = 0;

            try
            {
                if (model.Modelo.Id == null)
                {
                    // Grava a criança
                    #region
                    model.Modelo.Crianca.Tipo = "C";
                    idCrianca = Comum.Gravar<PessoaModels>(model.Modelo.Crianca, "Pessoa");
                    if (idCrianca > 0)
                    {
                        // Armazena o Id da criança para usar quando for salvar as imagens da mesma no método SalvarImagemCrianca
                        TempData["IdCrianca"] = idCrianca;
                        if (model.Modelo.Crianca.Endereco != null && !string.IsNullOrEmpty(model.Modelo.Crianca.Endereco.Cep))
                        {
                            model.Modelo.Crianca.Endereco.IdPessoa = idCrianca;
                            model.Modelo.Crianca.Endereco.Tipo = "R";
                            idCriancaEndereco = Comum.Gravar<EnderecoModels>(model.Modelo.Crianca.Endereco, "Endereco");

                            if (idCriancaEndereco == 0)
                            {
                                Comum.Excluir(idCrianca, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do endereço da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        model.Modelo.IdCrianca = idCrianca;

                        if (!string.IsNullOrEmpty(model.Modelo.Crianca.Instagram))
                        {
                            RedeSocialModels redesocial = new RedeSocialModels { Tipo = "I", Url = model.Modelo.Crianca.Instagram, IdPessoa = idCrianca };

                            idRedeSocialCrianca = Comum.Gravar<RedeSocialModels>(redesocial, "RedeSocial");

                            if (idRedeSocialCrianca == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idCrianca, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                    #endregion

                    // Grava o primeiro responsável
                    #region
                    model.Modelo.Responsavel1.Tipo = "P";
                    model.Modelo.Responsavel1.Celular = Funcoes.LimparFormatacao(model.Modelo.Responsavel1.Celular);
                    idResp1 = Comum.Gravar<PessoaModels>(model.Modelo.Responsavel1, "Pessoa");
                    if (idResp1 > 0)
                    {
                        model.Modelo.IdMae = idResp1;

                        if (model.Modelo.Responsavel1.Endereco != null && !string.IsNullOrEmpty(model.Modelo.Responsavel1.Endereco.Cep))
                        {
                            model.Modelo.Responsavel1.Endereco.IdPessoa = idResp1;
                            idResp1Endereco = Comum.Gravar<EnderecoModels>(model.Modelo.Responsavel1.Endereco, "Endereco");

                            if (idResp1Endereco == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");
                                Comum.Excluir(idResp1, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do endereço do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (!string.IsNullOrEmpty(model.Modelo.Responsavel1.Telefone))
                        {
                            ContatoModels contato = new Models.Comum.ContatoModels { Dado = Funcoes.LimparFormatacao(model.Modelo.Responsavel1.Telefone), Tipo = "T", IdPessoa = idResp1 };

                            idResp1Telefone = Comum.Gravar<ContatoModels>(contato, "Contato");

                            if (idResp1Telefone == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");

                                Comum.Excluir(idResp1Endereco, "Endereco");
                                Comum.Excluir(idResp1, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do telefone do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (!string.IsNullOrEmpty(model.Modelo.Responsavel1.Instagram))
                        {
                            RedeSocialModels redesocial = new RedeSocialModels { Tipo = "I", Url = model.Modelo.Responsavel1.Instagram, IdPessoa = idResp1 };

                            idResp1RedeSocial = Comum.Gravar<RedeSocialModels>(redesocial, "RedeSocial");

                            if (idResp1RedeSocial == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");

                                Comum.Excluir(idResp1Endereco, "Endereco");
                                Comum.Excluir(idResp1Telefone, "Contato");
                                Comum.Excluir(idResp1, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do instagram do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        Comum.Excluir(idContatoCrianca, "Contato");
                        Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                        Comum.Excluir(idCrianca, "Pessoa");
                        return Json(new { Status = "Error", Data = "Falha ao gravar os dados do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    #endregion

                    // Grava o segundo responsável
                    #region
                    if (model.Modelo.Responsavel2.Cpf != null)
                    {
                        model.Modelo.Responsavel2.Tipo = "P";
                        model.Modelo.Responsavel2.Celular = Funcoes.LimparFormatacao(model.Modelo.Responsavel2.Celular);
                        idResp2 = Comum.Gravar<PessoaModels>(model.Modelo.Responsavel2, "Pessoa");
                        if (idResp2 > 0)
                        {
                            model.Modelo.IdPai = idResp2;

                            if (model.Modelo.Responsavel2.Endereco != null && !string.IsNullOrEmpty(model.Modelo.Responsavel2.Endereco.Cep))
                            {
                                model.Modelo.Responsavel2.Endereco.IdPessoa = idResp2;
                                idResp2Endereco = Comum.Gravar<EnderecoModels>(model.Modelo.Responsavel2.Endereco, "Endereco");

                                if (idResp2Endereco == 0)
                                {
                                    Comum.Excluir(idContatoCrianca, "Contato");
                                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                    Comum.Excluir(idCrianca, "Pessoa");

                                    Comum.Excluir(idResp1Endereco, "Endereco");
                                    Comum.Excluir(idResp1Telefone, "Contato");
                                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                    Comum.Excluir(idResp1, "Pessoa");

                                    Comum.Excluir(idResp2, "Pessoa");
                                    return Json(new { Status = "Error", Data = "Falha ao gravar os dados do endereço do segundo Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                                }
                            }

                            if (!string.IsNullOrEmpty(model.Modelo.Responsavel2.Telefone))
                            {
                                ContatoModels contato = new Models.Comum.ContatoModels { Dado = Funcoes.LimparFormatacao(model.Modelo.Responsavel2.Telefone), Tipo = "T", IdPessoa = idResp2 };

                                idResp2Telefone = Comum.Gravar<ContatoModels>(contato, "Contato");

                                if (idResp2Telefone == 0)
                                {
                                    Comum.Excluir(idContatoCrianca, "Contato");
                                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                    Comum.Excluir(idCrianca, "Pessoa");

                                    Comum.Excluir(idResp1Endereco, "Endereco");
                                    Comum.Excluir(idResp1Telefone, "Contato");
                                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                    Comum.Excluir(idResp1, "Pessoa");

                                    Comum.Excluir(idResp2Endereco, "Endereco");
                                    Comum.Excluir(idResp2, "Pessoa");
                                    return Json(new { Status = "Error", Data = "Falha ao gravar os dados do telefone do segundo responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                                }
                            }

                            if (!string.IsNullOrEmpty(model.Modelo.Responsavel2.Instagram))
                            {
                                RedeSocialModels redesocial = new RedeSocialModels { Tipo = "I", Url = model.Modelo.Responsavel2.Instagram, IdPessoa = idResp2 };

                                idResp2RedeSocial = Comum.Gravar<RedeSocialModels>(redesocial, "RedeSocial");

                                if (idResp2RedeSocial == 0)
                                {
                                    Comum.Excluir(idContatoCrianca, "Contato");
                                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                    Comum.Excluir(idCrianca, "Pessoa");

                                    Comum.Excluir(idResp1Endereco, "Endereco");
                                    Comum.Excluir(idResp1Telefone, "Contato");
                                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                    Comum.Excluir(idResp1, "Pessoa");

                                    Comum.Excluir(idResp2Endereco, "Endereco");
                                    Comum.Excluir(idResp2Telefone, "Contato");
                                    Comum.Excluir(idResp2, "Pessoa");
                                    return Json(new { Status = "Error", Data = "Falha ao gravar os dados do instagram do segundo responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                        else
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");

                            Comum.Excluir(idResp1Endereco, "Endereco");
                            Comum.Excluir(idResp1Telefone, "Contato");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    #endregion

                    model.Modelo.IdCrianca = idCrianca;
                    model.Modelo.IdMae = idResp1;
                    model.Modelo.IdPai = idResp2;
                    idModelo = Comum.Gravar<ModeloViewModels>(model.Modelo, "Modelo");
                    if (idModelo != 0)
                    {
                        model.Modelo.Id = idModelo;
                        model.Modelo.Inscricao = string.Format("S{0}{1}{2}{3}", idModelo, idCrianca, idResp1, idResp2);

                        if (!Comum.Alterar<ModeloViewModels>(model.Modelo, "Modelo"))
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");

                            Comum.Excluir(idResp1Endereco, "Endereco");
                            Comum.Excluir(idResp1Telefone, "Contato");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Modelo. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }

                        model.Modelo.Manequim.IdModelo = idModelo;
                        idManequim = Comum.Gravar<ManequimViewModels>(model.Modelo.Manequim, "Manequim");
                        if (idManequim == 0)
                        {
                            Comum.Excluir(idModelo, "Modelo");
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");

                            Comum.Excluir(idResp1Endereco, "Endereco");
                            Comum.Excluir(idResp1Telefone, "Contato");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Manequim. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        Comum.Excluir(idContatoCrianca, "Contato");
                        Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                        Comum.Excluir(idCrianca, "Pessoa");

                        Comum.Excluir(idResp1Endereco, "Endereco");
                        Comum.Excluir(idResp1Telefone, "Contato");
                        Comum.Excluir(idResp1, "Pessoa");
                        return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Modelo. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { Status = "Success", IdPessoa1 = idResp1, IdPessoa2 = idResp2, IdCrianca = idCrianca, Inscricao = model.Modelo.Inscricao }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Comum.Excluir(idContatoCrianca, "Contato");
                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                Comum.Excluir(idCrianca, "Pessoa");

                Comum.Excluir(idResp1Endereco, "Endereco");
                Comum.Excluir(idResp1Telefone, "Contato");
                Comum.Excluir(idResp1, "Pessoa");
                return Json(new { Status = "Error", Data = "Falha ao finalizar o cadastro. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Pagamento(PagamentoViewModels model)
        {
            var pagamento = 0;
            try
            {
                var ev = Comum.ListarPorId(model.IdPessoa.Value, "Pessoa");

                PessoaModels pessoa = JsonConvert.DeserializeObject<PessoaModels>(ev.Result);

                var sel = Comum.ListarPorId(model.IdSeletiva.Value, "Seletiva");

                SeletivaViewModels seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(sel.Result);

                model.IdSeletiva = seletiva.Id;
                model.IdEvento = seletiva.IdEvento;
                model.DataPagamento = DateTime.Now;
                model.TipoPagamento = "P";
                model.Status = "P";

                pagamento = Comum.Gravar<PagamentoViewModels>(model, "Pagamento");

                model.CodigoReferencia = Guid.NewGuid().ToString();
                model.Id = pagamento;

                if (Comum.Alterar<PagamentoViewModels>(model, "Pagamento"))
                {
                    System.Collections.Specialized.NameValueCollection postData = new System.Collections.Specialized.NameValueCollection();
                    postData.Add("email", ConfigurationManager.AppSettings["Email"]);
                    postData.Add("token", ConfigurationManager.AppSettings["Token"]);
                    postData.Add("currency", "BRL");
                    postData.Add("itemId1", "0001");
                    postData.Add("itemDescription1", seletiva.Nome);
                    postData.Add("itemAmount1", model.ValorPagamento.Value.ToString(new CultureInfo("en-US")));
                    postData.Add("itemQuantity1", "1");
                    postData.Add("itemWeight1", "200");
                    postData.Add("reference", model.CodigoReferencia);
                    postData.Add("senderName", pessoa.Nome);
                    postData.Add("senderAreaCode", Funcoes.LimparFormatacao(pessoa.Celular).Substring(0, 2));
                    postData.Add("senderPhone", Funcoes.LimparFormatacao(pessoa.Celular).Substring(2));
                    postData.Add("senderEmail", pessoa.Email);
                    postData.Add("shippingAddressRequired", "false");

                    //String que receberá o XML de retorno.
                    string xmlString = null;

                    //Webclient faz o post para o servidor de pagseguro.
                    using (WebClient wc = new WebClient())
                    {
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        //Informa header sobre URL.
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                        //Faz o POST e retorna o XML contendo resposta do servidor do pagseguro.
                        var result = wc.UploadValues(ConfigurationManager.AppSettings["ApiPagSeguro"], postData);

                        //Obtém string do XML.
                        xmlString = Encoding.ASCII.GetString(result);
                    }

                    //Cria documento XML.
                    XmlDocument xmlDoc = new XmlDocument();

                    //Carrega documento XML por string.
                    xmlDoc.LoadXml(xmlString);

                    //Obtém código de transação (Checkout).
                    var code = xmlDoc.GetElementsByTagName("code")[0];

                    //Obtém data de transação (Checkout).
                    var date = xmlDoc.GetElementsByTagName("date")[0];

                    //Monta a URL para pagamento.
                    //var paymentUrl = string.Concat(ConfigurationManager.AppSettings["RedirectPagSeguro"], code.InnerText);

                    return Json(new { Status = "Success", Data = code.InnerText }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Status = "Error", Data = "Falha ao acessar a PagSeguro. Favor contate o administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Comum.Excluir(pagamento, "Pagamento");
                return Json(new { Status = "Error", Data = string.Format("Falha ao gerar o pagamento: {0}. Favor contate o administrador do sistema!", ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult BuscarInscricao(string inscricao, int idEvento, int idSeletiva)
        {
            string statusInscricao = "Inscrição não encontrada";
            string formPagamento = "";
            string statusPagamento = "";
            string codPagSeguro = "";

            List<ModeloViewModels> mod = new List<ModeloViewModels>();

            List<PagamentoViewModels> pagRet = new List<PagamentoViewModels>();

            var retMod = Comum.ListarPor(new ModeloViewModels { Inscricao = inscricao, Crianca = null }, "Modelo");

            mod = JsonConvert.DeserializeObject<List<ModeloViewModels>>(retMod);

            if (mod != null && mod.Count() > 0)
            {
                List<PagamentoViewModels> pagamento = new List<PagamentoViewModels>();

                PagamentoViewModels pag = new PagamentoViewModels { IdSeletiva = idSeletiva, IdEvento = idEvento };

                var ev = "";

                if (!mod[0].IdPai.HasValue)
                {
                    pag.IdPessoa = mod[0].IdMae;
                    ev = Comum.ListarPor(pag, "Pagamento");
                }
                else
                {
                    pag.IdPessoa = mod[0].IdPai;
                    ev = Comum.ListarPor(pag, "Pagamento");

                    pagRet = JsonConvert.DeserializeObject<List<PagamentoViewModels>>(ev);

                    if (pagRet.Count() == 0)
                    {
                        pag.IdPessoa = mod[0].IdMae;
                        ev = Comum.ListarPor(pag, "Pagamento");
                        pagRet = JsonConvert.DeserializeObject<List<PagamentoViewModels>>(ev);
                    }
                }

                statusInscricao = "Inscrição não concluída";

                if (pagRet != null && pagRet.Count() > 0)
                {
                    formPagamento = Util.RetornarFormaPagamento(pagRet[0].TipoPagamento);
                    statusPagamento = Util.RetornarStatusPagamento(pagRet[0].Status);
                    if (pagRet[0].Status == "3" || pagRet[0].Status == "4")
                        statusInscricao = "Inscrição concluída";

                    if (string.IsNullOrEmpty(formPagamento) && string.IsNullOrEmpty(statusPagamento))
                        statusInscricao = "Inscrição incompleta";

                    codPagSeguro = pagRet[0].CodigoTransacaoPG;
                }
                else
                {
                    statusInscricao = "Inscrição incompleta";
                }
            }

            return Json(new { Status = "Success", Inscricao = statusInscricao, Pagamento = formPagamento, StatusPagamento = statusPagamento, CodigoPagSeguro = codPagSeguro }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BuscarDados(string inscricao, int idEvento, int idSeletiva)
        {
            string statusInscricao = "Cadastro não concluído";
            string formPagamento = "";
            string statusPagamento = "";
            string codPagSeguro = "";

            List<ModeloViewModels> mod = new List<ModeloViewModels>();

            List<PagamentoViewModels> pagRet = new List<PagamentoViewModels>();

            var retMod = Comum.ListarPor(new ModeloViewModels { Inscricao = inscricao, Crianca = null }, "Modelo");
            if (!string.IsNullOrEmpty(retMod))
            {
                mod = JsonConvert.DeserializeObject<List<ModeloViewModels>>(retMod);

                List<PagamentoViewModels> pagamento = new List<PagamentoViewModels>();

                PagamentoViewModels pag = new PagamentoViewModels { IdSeletiva = idSeletiva, IdEvento = idEvento };

                var ev = "";

                if (!mod[0].IdPai.HasValue)
                {
                    pag.IdPessoa = mod[0].IdMae;
                    ev = Comum.ListarPor(pag, "Pagamento");
                }
                else
                {
                    pag.IdPessoa = mod[0].IdMae;
                    ev = Comum.ListarPor(pag, "Pagamento");

                    if (string.IsNullOrEmpty(ev))
                    {
                        pag.IdPessoa = mod[0].IdPai;
                        ev = Comum.ListarPor(pag, "Pagamento");
                    }

                    pagRet = JsonConvert.DeserializeObject<List<PagamentoViewModels>>(ev);
                }

                if (pagRet != null)
                {
                    formPagamento = Util.RetornarFormaPagamento(pagRet[0].TipoPagamento);
                    statusPagamento = Util.RetornarStatusPagamento(pagRet[0].Status);
                    if (pagRet[0].Status == "3")
                        statusInscricao = "Cadastro concluído";

                    if (string.IsNullOrEmpty(formPagamento) && string.IsNullOrEmpty(statusPagamento))
                        statusInscricao = "Cadastro incompleto";

                    codPagSeguro = pagRet[0].CodigoTransacaoPG;
                }
                else
                {
                    statusInscricao = "Cadastro incompleto";
                }
            }

            return Json(new { Status = "Success", Inscricao = statusInscricao, Pagamento = formPagamento, StatusPagamento = statusPagamento, CodigoPagSeguro = codPagSeguro }, JsonRequestBehavior.AllowGet);
        }
    }
}