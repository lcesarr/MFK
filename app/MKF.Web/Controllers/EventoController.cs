﻿using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Controllers
{
    public class EventoController : Controller
    {
        // GET: Inscricao
        public ActionResult Index()
        {
            List<EventoModels> eventos = new List<EventoModels>();

            var ev = Comum.ListarTodosUnion("Eventos", "BuscarTodos");

            eventos = JsonConvert.DeserializeObject<List<EventoViewModels>>(ev.Result);

            return View(eventos.Where(s => s.Status == 0 && s.DataInicio > DateTime.Now).ToList());
        }

        // GET: Inscricao
        public ActionResult Create(int id, string url)
        {
            EventoModels evento = new Models.EventoModels();
            // evento.Modelo.Url = url;
            // SeletivaViewModels seletiva = new Models.SeletivaViewModels();

            var ev = Comum.ListarPorIdUnion(id, "Evento", "BuscarSeletiva");

            evento = JsonConvert.DeserializeObject<EventoModels>(ev.Result);

            var reg = Comum.ListarPorId(evento.Regulamento.Id.Value, "Regulamento");

            //seletiva.Regulamento = JsonConvert.DeserializeObject<RegulamentoViewModels>(reg.Result);
            //seletiva.Regulamento.Dados = seletiva.Regulamento.Dados.Replace("{datainicio}", seletiva.DataInicio.Value.ToString("dd/MM/yyyy HH:mm")).Replace("{localseletiva}", seletiva.Local.LocalSeletiva).Replace("{datafim}", seletiva.DataFim.Value.ToString("dd/MM/yyyy HH:mm"));
            //inscricao.Seletiva = seletiva;

            //var aut = Comum.Listar("", "Regulamento");

            //List<RegulamentoViewModels> ret = JsonConvert.DeserializeObject<List<RegulamentoViewModels>>(aut.Result);
            //inscricao.Modelo.Autorizacao = ret.Where(a => a.Tipo == "I").FirstOrDefault();

            //inscricao.Modelo.SeletivaNome = seletiva.Nome;
            //inscricao.Modelo.IdSeletiva = seletiva.Id;

            //inscricao.Modelo.DataInicioSeletiva = seletiva.DataInicio.Value;
            //inscricao.Modelo.DataFimSeletiva = seletiva.DataFim.Value;
            //inscricao.Modelo.IntervaloApresentacao = seletiva.QtdInscricaoHorario;

            return View(evento);
        }

        // Post: Inscricao
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(InscricaoViewModel model)
        {
            int idCrianca = 0;
            int idContatoCrianca = 0;
            int idRedeSocialCrianca = 0;

            int idResp1 = 0;
            int idResp1Endereco = 0;
            int idResp1ContatoEmail = 0;
            int idResp1Telefone = 0;
            int idResp1RedeSocial = 0;

            int idResp2 = 0;
            int idResp2Endereco = 0;
            int idResp2ContatoEmail = 0;
            int idResp2Telefone = 0;
            int idResp2RedeSocial = 0;


            if (model.Modelo.Id == 0)
            {
                // Grava a criança
                #region
                model.Modelo.Crianca.Tipo = "C";
                idCrianca = Comum.Gravar<PessoaModels>(model.Modelo.Crianca, "Pessoa");
                if (idCrianca > 0)
                {
                    model.Modelo.IdCrianca = idCrianca;
                    if (string.IsNullOrEmpty(model.Modelo.Crianca.Email))
                    {
                        ContatoModels contato = new Models.Comum.ContatoModels { Dado = model.Modelo.Crianca.Email, Tipo = "E", };

                        idContatoCrianca = Comum.Gravar<ContatoModels>(contato, "Contato");

                        if (idContatoCrianca == 0)
                        {
                            Comum.Excluir(idCrianca, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (string.IsNullOrEmpty(model.Modelo.Crianca.Instagram))
                    {
                        RedeSocialModels redesocial = new RedeSocialModels { Tipo = "I", Url = model.Modelo.Crianca.Instagram };

                        idRedeSocialCrianca = Comum.Gravar<RedeSocialModels>(redesocial, "RedeSocial");

                        if (idRedeSocialCrianca == 0)
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idCrianca, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
                #endregion

                // Grava o primeiro responsável
                #region
                idResp1 = Comum.Gravar<PessoaModels>(model.Modelo.Responsavel1, "Pessoa");
                if (idResp1 > 0)
                {
                    model.Modelo.IdMae = idResp1;

                    if (model.Modelo.Responsavel1.Endereco != null && string.IsNullOrEmpty(model.Modelo.Responsavel1.Endereco.Cep))
                    {
                        idResp1Endereco = Comum.Gravar<EnderecoModels>(model.Modelo.Responsavel1.Endereco, "Endereco");

                        if (idResp1Endereco == 0)
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados do endereço do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (string.IsNullOrEmpty(model.Modelo.Responsavel1.Email))
                    {
                        ContatoModels contato = new Models.Comum.ContatoModels { Dado = model.Modelo.Responsavel1.Email, Tipo = "E", };

                        idResp1ContatoEmail = Comum.Gravar<ContatoModels>(contato, "Contato");

                        if (idResp1ContatoEmail == 0)
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");

                            Comum.Excluir(idResp1Endereco, "Endereco");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados do e-mail do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (string.IsNullOrEmpty(model.Modelo.Responsavel1.Telefone))
                    {
                        ContatoModels contato = new Models.Comum.ContatoModels { Dado = model.Modelo.Responsavel1.Telefone, Tipo = "T", };

                        idResp1Telefone = Comum.Gravar<ContatoModels>(contato, "Contato");

                        if (idResp1Telefone == 0)
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");

                            Comum.Excluir(idResp1Endereco, "Endereco");
                            Comum.Excluir(idResp1ContatoEmail, "Contato");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados do telefone do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (string.IsNullOrEmpty(model.Modelo.Responsavel1.Instagram))
                    {
                        RedeSocialModels redesocial = new RedeSocialModels { Tipo = "I", Url = model.Modelo.Responsavel1.Instagram };

                        idResp1RedeSocial = Comum.Gravar<RedeSocialModels>(redesocial, "RedeSocial");

                        if (idResp1RedeSocial == 0)
                        {
                            Comum.Excluir(idContatoCrianca, "Contato");
                            Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                            Comum.Excluir(idCrianca, "Pessoa");

                            Comum.Excluir(idResp1Endereco, "Endereco");
                            Comum.Excluir(idResp1ContatoEmail, "Contato");
                            Comum.Excluir(idResp1Telefone, "Contato");
                            Comum.Excluir(idResp1, "Pessoa");
                            return Json(new { Status = "Error", Data = "Falha ao gravar os dados do instagram do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    Comum.Excluir(idContatoCrianca, "Contato");
                    Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                    Comum.Excluir(idCrianca, "Pessoa");
                    return Json(new { Status = "Error", Data = "Falha ao gravar os dados do primeiro Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }

                #endregion

                // Grava o segundo responsável
                #region
                if (model.Modelo.Responsavel2 != null)
                {
                    idResp2 = Comum.Gravar<PessoaModels>(model.Modelo.Responsavel2, "Pessoa");
                    if (idResp2 > 0)
                    {
                        model.Modelo.IdPai = idResp2;

                        if (model.Modelo.Responsavel2.Endereco != null && string.IsNullOrEmpty(model.Modelo.Responsavel2.Endereco.Cep))
                        {
                            idResp2Endereco = Comum.Gravar<EnderecoModels>(model.Modelo.Responsavel2.Endereco, "Endereco");

                            if (idResp2Endereco == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");

                                Comum.Excluir(idResp1Endereco, "Endereco");
                                Comum.Excluir(idResp1ContatoEmail, "Contato");
                                Comum.Excluir(idResp1Telefone, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idResp1, "Pessoa");

                                Comum.Excluir(idResp2, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do endereço do segundo Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (string.IsNullOrEmpty(model.Modelo.Responsavel2.Email))
                        {
                            ContatoModels contato = new Models.Comum.ContatoModels { Dado = model.Modelo.Responsavel2.Email, Tipo = "E", };

                            idResp2ContatoEmail = Comum.Gravar<ContatoModels>(contato, "Contato");

                            if (idResp2ContatoEmail == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");

                                Comum.Excluir(idResp1Endereco, "Endereco");
                                Comum.Excluir(idResp1ContatoEmail, "Contato");
                                Comum.Excluir(idResp1Telefone, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idResp1, "Pessoa");

                                Comum.Excluir(idResp2Endereco, "Endereco");
                                Comum.Excluir(idResp2, "Pessoa");

                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do e-mail do segundo responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (string.IsNullOrEmpty(model.Modelo.Responsavel2.Telefone))
                        {
                            ContatoModels contato = new Models.Comum.ContatoModels { Dado = model.Modelo.Responsavel2.Telefone, Tipo = "T", };

                            idResp2Telefone = Comum.Gravar<ContatoModels>(contato, "Contato");

                            if (idResp2Telefone == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");

                                Comum.Excluir(idResp1Endereco, "Endereco");
                                Comum.Excluir(idResp1ContatoEmail, "Contato");
                                Comum.Excluir(idResp1Telefone, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idResp1, "Pessoa");

                                Comum.Excluir(idResp2Endereco, "Endereco");
                                Comum.Excluir(idResp2ContatoEmail, "Contato");
                                Comum.Excluir(idResp2, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do telefone do segundo responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        if (string.IsNullOrEmpty(model.Modelo.Responsavel2.Instagram))
                        {
                            RedeSocialModels redesocial = new RedeSocialModels { Tipo = "I", Url = model.Modelo.Responsavel2.Instagram };

                            idResp2RedeSocial = Comum.Gravar<RedeSocialModels>(redesocial, "RedeSocial");

                            if (idResp2RedeSocial == 0)
                            {
                                Comum.Excluir(idContatoCrianca, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idCrianca, "Pessoa");

                                Comum.Excluir(idResp1Endereco, "Endereco");
                                Comum.Excluir(idResp1ContatoEmail, "Contato");
                                Comum.Excluir(idResp1Telefone, "Contato");
                                Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                                Comum.Excluir(idResp1, "Pessoa");

                                Comum.Excluir(idResp2Endereco, "Endereco");
                                Comum.Excluir(idResp2ContatoEmail, "Contato");
                                Comum.Excluir(idResp2Telefone, "Contato");
                                Comum.Excluir(idResp2, "Pessoa");
                                return Json(new { Status = "Error", Data = "Falha ao gravar os dados do instagram do segundo responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        Comum.Excluir(idContatoCrianca, "Contato");
                        Comum.Excluir(idRedeSocialCrianca, "RedeSocial");
                        Comum.Excluir(idCrianca, "Pessoa");

                        Comum.Excluir(idResp1Endereco, "Endereco");
                        Comum.Excluir(idResp1ContatoEmail, "Contato");
                        Comum.Excluir(idResp1Telefone, "Contato");
                        Comum.Excluir(idResp1, "Pessoa");
                        return Json(new { Status = "Error", Data = "Falha ao gravar os dados da Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                    #endregion
                }
            }

            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Pagamento(PagamentoViewModels model)
        {
            var ev = Comum.ListarPorId(model.IdPessoa.Value, "Pessoa");

            PessoaModels pessoa = JsonConvert.DeserializeObject<PessoaModels>(ev.Result);

            var sel = Comum.ListarPorId(model.IdPessoa.Value, "Seletiva");

            SeletivaViewModels seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(sel.Result);

            var prog = Comum.Gravar<PagamentoViewModels>(model, "Pagamento");

            var codref = string.Format("REF{0}", prog.ToString().PadLeft(10, '0'));

            model.CodigoReferencia = codref;
            model.Id = prog;

            if (Comum.Alterar<PagamentoViewModels>(model, "Pagamento"))
            {
                //System.Collections.Specialized.NameValueCollection postData = new System.Collections.Specialized.NameValueCollection();
                //postData.Add("email", ConfigurationManager.AppSettings["Email"]);
                //postData.Add("token", ConfigurationManager.AppSettings["Token"]);
                //postData.Add("currency", "BRL");
                //postData.Add("itemId1", "0001");
                //postData.Add("itemDescription1", seletiva.Nome);
                //postData.Add("itemAmount1", model.ValorPagamento.ToString());
                //postData.Add("itemQuantity1", "1");
                //postData.Add("itemWeight1", "200");
                //postData.Add("reference", "REF1234");
                //postData.Add("senderName", pessoa.Nome);
                //postData.Add("senderAreaCode", Funcoes.LimparFormatacao(pessoa.Celular).Substring(0, 2));
                //postData.Add("senderPhone", Funcoes.LimparFormatacao(pessoa.Celular).Substring(2));
                //postData.Add("senderEmail", pessoa.Email);
                //postData.Add("shippingAddressRequired", "false");

                ////String que receberá o XML de retorno.
                //string xmlString = null;

                ////Webclient faz o post para o servidor de pagseguro.
                //using (WebClient wc = new WebClient())
                //{
                //    //Informa header sobre URL.
                //    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                //    //Faz o POST e retorna o XML contendo resposta do servidor do pagseguro.
                //    var result = wc.UploadValues(ConfigurationManager.AppSettings["ApiPagSeguro"], postData);

                //    //Obtém string do XML.
                //    xmlString = Encoding.ASCII.GetString(result);
                //}

                ////Cria documento XML.
                //XmlDocument xmlDoc = new XmlDocument();

                ////Carrega documento XML por string.
                //xmlDoc.LoadXml(xmlString);

                ////Obtém código de transação (Checkout).
                //var code = xmlDoc.GetElementsByTagName("code")[0];

                ////Obtém data de transação (Checkout).
                //var date = xmlDoc.GetElementsByTagName("date")[0];

                ////Monta a URL para pagamento.
                //var paymentUrl = string.Concat(ConfigurationManager.AppSettings["RedirectPagSeguro"], code.InnerText);

                ////Retorna dados para HTML.
                //System.Diagnostics.Process.Start(paymentUrl);

                return Json(new { Status = "Success", Data = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Status = "Error", Data = "" }, JsonRequestBehavior.AllowGet);
        }
    }
}