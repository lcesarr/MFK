﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MKF.Web.Startup))]
namespace MKF.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
