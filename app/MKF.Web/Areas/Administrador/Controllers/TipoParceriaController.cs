﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class TipoParceriaController : Controller
    {
        // GET: Administrador/TipoParceria
        public ActionResult Index()
        {
            List<TipoParceriaViewModels> parcerias = new List<TipoParceriaViewModels>();

            var parceria = Comum.Listar("", "TipoParceria");

            parcerias = JsonConvert.DeserializeObject<List<TipoParceriaViewModels>>(parceria.Result);

            return View(parcerias);
        }

        // GET: Administrador/TipoParceria
        public ActionResult Create()
        {
            TipoParceriaViewModels model = new Web.Models.TipoParceriaViewModels { Status = 0 };
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(TipoParceriaViewModels model)
        {
            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (model.Id == 0)
            {
                int e = Comum.Gravar<TipoParceriaViewModels>(model, "TipoParceria");

                if (e > 0)
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = "Error", Data = "Falha ao cadastrar o Tipo de parceria. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Comum.Alterar<TipoParceriaViewModels>(model, "TipoParceria"))
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Tipo de parceria. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        private TipoParceriaViewModels PreencherModels(int id, string url)
        {
            TipoParceriaViewModels tipoParceria = new TipoParceriaViewModels { Url = url };

            var ev = Comum.ListarPorId(id, "TipoParceria");

            tipoParceria = JsonConvert.DeserializeObject<TipoParceriaViewModels>(ev.Result);

            return tipoParceria;
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "TipoParceria");

            return Json("");
        }
    }
}