﻿using MKF.Web.Filters;
using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using MKF.Web.Models.Relatorios;
using Newtonsoft.Json;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class RelatorioController : Controller
    {
        //CarregarImagens
        private string caminhoImagensCrianca = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Content\\ImagensCrianca\\";

        // GET: Administrador/Relatorio
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult ListarEventos()
        {
            List<EventoModels> eventos = JsonConvert.DeserializeObject<List<EventoModels>>(EventoSrv.ListarEventoSeletiva().Result);

            return Json(new { Status = "Sucesso", Data = eventos }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Seletiva(int idSeletiva, string nomeSeletiva)
        {
            ViewBag.IdSeletiva = idSeletiva;
            ViewBag.NomeSeletiva = nomeSeletiva;
            //List<RelSeletivaViewModels> lojistas = new List<RelSeletivaViewModels>();

            //var ev = RelSeletivaSrv.ListarInscSeletiva("null", "null", 0, 0, "null");

            //if (!string.IsNullOrEmpty(ev.Result))
            //    lojistas = JsonConvert.DeserializeObject<List<RelSeletivaViewModels>>(ev.Result);

            return View();
        }

        [HttpPost]
        public ActionResult Seletiva(string nome, string inscricao, int? eventoId, int? seletivaId, string status)
        {
            List<RelSeletivaViewModels> lojistas = new List<RelSeletivaViewModels>();

            if (string.IsNullOrEmpty(nome)) nome = "null";
            if (string.IsNullOrEmpty(inscricao)) inscricao = "null";
            if (!eventoId.HasValue) eventoId = 0;
            if (!seletivaId.HasValue) seletivaId = 0;
            if (string.IsNullOrEmpty(status)) status = "null";

            var ev = RelSeletivaSrv.ListarInscSeletiva(nome, inscricao, eventoId.Value, seletivaId.Value, status);

            if (!string.IsNullOrEmpty(ev.Result))
                lojistas = JsonConvert.DeserializeObject<List<RelSeletivaViewModels>>(ev.Result);

            return View(lojistas);
        }

        public ActionResult EditarSeletiva(string id)
        {
            var ids = id.Split(',');
            ModeloSeletivaModels inscrito = new ModeloSeletivaModels();
            var c = InscricaoSeletivaSrv.RecuperarInscrito(int.Parse(ids[0]));

            if (!string.IsNullOrEmpty(c.Result))
                inscrito = JsonConvert.DeserializeObject<ModeloSeletivaModels>(c.Result);

            if (inscrito.Id > 0)
            {
                var reps1 = PessoaSrv.BuscarPessoa(inscrito.IdMae);
                inscrito.Responsavel1 = JsonConvert.DeserializeObject<PessoaModels>(reps1.Result);

                if (inscrito.IdPai > 0)
                {
                    var reps2 = PessoaSrv.BuscarPessoa(inscrito.IdPai);
                    inscrito.Responsavel2 = JsonConvert.DeserializeObject<PessoaModels>(reps2.Result);
                }

                inscrito.Idade = Util.CalculateIdade(inscrito.Crianca.DataNascimento.Value);

                inscrito.Pagamento = inscrito.Pagamentos.FirstOrDefault(p => p.Id.Value == int.Parse(ids[1]));

                if (inscrito.Pagamento != null)
                {
                    var sel = Comum.ListarPorId(inscrito.Pagamento.IdSeletiva.Value, "Seletiva");
                    inscrito.Seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(sel.Result);
                }
                else
                {
                    inscrito.Pagamento = new PagamentoViewModels();

                    var sel = Comum.Listar(string.Empty, "Seletiva");
                    List<SeletivaViewModels> lstSeletiva = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(sel.Result);

                    inscrito.Seletiva = lstSeletiva.Count() > 0 ? lstSeletiva.FirstOrDefault(a => a.Status == 0) : new Models.SeletivaViewModels();
                }

                var img = Comum.ListarPor(new ImagemModels { IdPessoa = inscrito.IdCrianca }, "Imagem");

                inscrito.Crianca.Imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(img);

                if (inscrito.Crianca.Imagens.Count() > 0)
                {
                    ImagemModels imgP = inscrito.Crianca.Imagens.FirstOrDefault(i => i.Tipo == "0");
                    if (imgP != null)
                        inscrito.UrlImagem = imgP.URL;
                    else
                        inscrito.UrlImagem = inscrito.Crianca.Imagens[0].URL;
                }

                if (inscrito.Manequim == null)
                    inscrito.Manequim = new ManequimViewModels { IdModelo = inscrito.IdModelo };
            }

            if (string.IsNullOrEmpty(inscrito.Inscricao))
                inscrito.Inscricao = string.Format("S{0}{1}{2}{3}", inscrito.Id, inscrito.IdCrianca, inscrito.IdMae, inscrito.IdPai);

            return View(inscrito);
        }

        [HttpPost]
        public ActionResult EditarSeletiva(ModeloSeletivaModels model)
        {
            if (model.Crianca != null)
            {
                ModelState.Remove("Crianca.Endereco.Cep");
                ModelState.Remove("Crianca.Endereco.Logradouro");
                ModelState.Remove("Crianca.Endereco.Numero");
                ModelState.Remove("Crianca.Endereco.Bairro");
                ModelState.Remove("Crianca.Endereco.Cidade");
                ModelState.Remove("Crianca.Endereco.Estado");
            }
            else
            {
                ModelState.Remove("NomeArtistico");
                ModelState.Remove("HorarioDesfile");
            }

            if (model.Manequim == null)
            {

            }

            ModelState.Remove("Id");

            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (model.Crianca != null)
            {
                if (Comum.Alterar(model.Crianca, "Pessoa"))
                {
                    if (!string.IsNullOrEmpty(model.Crianca.Endereco.Cep))
                    {
                        var e = 0;
                        if (model.Crianca.Endereco.Id.HasValue && model.Crianca.Endereco.Id.Value > 0)
                        {
                            if (!Comum.Alterar(model.Crianca.Endereco, "Endereco"))
                                return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            model.Crianca.Endereco.IdPessoa = model.Crianca.Id;
                            e = Comum.Gravar(model.Crianca.Endereco, "Endereco");
                            if (e == 0)
                                return Json(new { Status = "Error", Data = "Falha ao cadastrar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);

                        }
                    }

                    if (model.Manequim.Id > 0)
                    {
                        if (!Comum.Alterar(model.Manequim, "Manequim"))
                            return Json(new { Status = "Error", Data = "Falha ao alterar o Manequim. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        model.Manequim.IdModelo = model.IdModelo;
                        var m = Comum.Gravar(model.Manequim, "Manequim");
                        if (m == 0)
                            return Json(new { Status = "Error", Data = "Falha ao cadastrar o Manequim. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.IdModelo > 0)
                    {
                        model.Id = model.IdModelo;
                        if (!Comum.Alterar(model, "Modelo"))
                            return Json(new { Status = "Error", Data = "Falha ao alterar o Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { Status = "Error", Data = "Falha ao alterar a Criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }

            if (model.Responsavel1 != null)
            {
                if (Comum.Alterar(model.Responsavel1, "Pessoa"))
                {
                    if (model.Responsavel1.Endereco.Id.HasValue && model.Responsavel1.Endereco.Id.Value > 0)
                    {
                        if (!Comum.Alterar(model.Responsavel1.Endereco, "Endereco"))
                            return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        model.Responsavel1.Endereco.IdPessoa = model.Responsavel1.Id;
                        var e = Comum.Gravar(model.Responsavel1.Endereco, "Endereco");
                        if (e == 0)
                            return Json(new { Status = "Error", Data = "Falha ao cadastrar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Responsável 1. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }

            if (model.Responsavel2 != null)
            {
                if (model.Responsavel2.Endereco.Id.HasValue && model.Responsavel2.Endereco.Id.Value > 0)
                {
                    if (!Comum.Alterar(model.Responsavel2.Endereco, "Endereco"))
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.Responsavel2.Endereco.IdPessoa = model.Responsavel2.Id;
                    var e = Comum.Gravar(model.Responsavel2.Endereco, "Endereco");
                    if (e == 0)
                        return Json(new { Status = "Error", Data = "Falha ao cadastrar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
            }


            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MapaSeletiva(int? idSeletiva, string nomeSeletiva)
        {
            List<MapaSeletivaModels> mapaSeletiva = new List<MapaSeletivaModels>();

            ViewBag.NomeSeletiva = nomeSeletiva;
            ViewBag.IdSeletiva = idSeletiva.Value;

            var ev = RelSeletivaSrv.ListarMapaSeletiva(idSeletiva.Value);

            if (!string.IsNullOrEmpty(ev.Result))
                mapaSeletiva = JsonConvert.DeserializeObject<List<MapaSeletivaModels>>(ev.Result);

            return View(mapaSeletiva);
        }

        public ActionResult ExportarMapaSeletiva(int idSeletiva)
        {
            List<MapaSeletivaModels> mapaSeletiva = new List<MapaSeletivaModels>();

            var ev = RelSeletivaSrv.ListarMapaSeletiva(idSeletiva);

            if (!string.IsNullOrEmpty(ev.Result))
                mapaSeletiva = JsonConvert.DeserializeObject<List<MapaSeletivaModels>>(ev.Result);

            System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
            var data = mapaSeletiva;

            gv.AutoGenerateColumns = false;
            gv.Columns.Add(new System.Web.UI.WebControls.ImageField { HeaderText = "Imagem", DataImageUrlField = "UrlImagem" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Criança", DataField = "Nome" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Resp 1", DataField = "Resp1" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Resp 2", DataField = "Resp2" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Idade", DataField = "Idade" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Tipo de pagamento", DataField = "TipoPagamento" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Horario de apresentação", DataField = "HorarioDesfile" });

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("UrlImagem");
            dt.Columns.Add("Nome");
            dt.Columns.Add("Resp1");
            dt.Columns.Add("Resp2");
            dt.Columns.Add("Idade");
            dt.Columns.Add("TipoPagamento");
            dt.Columns.Add("HorarioDesfile");

            foreach (var item in data)
            {
                dt.Rows.Add(item.UrlImagem, item.Nome, item.Resp1, item.Resp2, item.Idade, Util.RetornarFormaPagamento(item.TipoPagamento), item.HorarioDesfile);
            }

            gv.DataSource = dt;
            gv.DataBind();

            gv.Columns[0].ItemStyle.Width = 100;
            gv.Columns[0].ItemStyle.Height = 100;

            gv.Columns[0].ControlStyle.Width = 100;

            for (int i = 0; i < data.Count; i++)
            {
                gv.Rows[i].Height = 100;
            }

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
            gv.RenderControl(htw);

            var ret = htw.InnerWriter.ToString().Replace("style=\"width:100px;\"", "width=75 height=78");

            return new ExcelResult
            {
                FileName = string.Format("Mapa_Seletiva_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")),
                Item = ret
            };
        }

        public ActionResult ExportarAprovadosSeletiva(int idSeletiva)
        {
            List<MapaSeletivaModels> mapaSeletiva = new List<MapaSeletivaModels>();
            ViewBag.IdSeletiva = idSeletiva;

            var ev = RelSeletivaSrv.ListarAprovadosSeletiva(idSeletiva, 0, 16);

            if (!string.IsNullOrEmpty(ev.Result))
                mapaSeletiva = JsonConvert.DeserializeObject<List<MapaSeletivaModels>>(ev.Result);

            System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
            var data = mapaSeletiva;

            gv.AutoGenerateColumns = false;
            gv.Columns.Add(new System.Web.UI.WebControls.ImageField { HeaderText = "Imagem", DataImageUrlField = "UrlImagem" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Criança", DataField = "Nome" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Resp 1", DataField = "Resp1" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Resp 2", DataField = "Resp2" });
            gv.Columns.Add(new System.Web.UI.WebControls.BoundField { HeaderText = "Idade", DataField = "Idade" });

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("UrlImagem");
            dt.Columns.Add("Nome");
            dt.Columns.Add("Resp1");
            dt.Columns.Add("Resp2");
            dt.Columns.Add("Idade");

            foreach (var item in data)
            {
                dt.Rows.Add(item.UrlImagem, item.Nome, item.Resp1, item.Resp2, item.Idade);
            }

            gv.DataSource = dt;
            gv.DataBind();

            gv.Columns[0].ItemStyle.Width = 100;
            gv.Columns[0].ItemStyle.Height = 100;

            gv.Columns[0].ControlStyle.Width = 100;

            for (int i = 0; i < data.Count; i++)
            {
                gv.Rows[i].Height = 100;
            }

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
            gv.RenderControl(htw);

            var ret = htw.InnerWriter.ToString().Replace("style=\"width:100px;\"", "width=75 height=78");

            return new ExcelResult
            {
                FileName = string.Format("Aprovados_Seletiva_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")),
                Item = ret
            };
        }

        public ActionResult GerarPagamento(string inscricao, string tipo)
        {
            int idPagamento = 0;
            try
            {
                List<ModeloViewModels> mod = new List<ModeloViewModels>();
                var retMod = Comum.ListarPor(new ModeloViewModels { Inscricao = inscricao, Crianca = null }, "Modelo");

                mod = JsonConvert.DeserializeObject<List<ModeloViewModels>>(retMod);

                if (mod != null && mod.Count() > 0)
                {
                    List<PagamentoViewModels> pagamento = new List<PagamentoViewModels>();

                    PagamentoViewModels pag = new PagamentoViewModels { IdSeletiva = mod[0].IdSeletiva, IdEvento = mod[0].IdEvento, Inscricao = inscricao };

                    var ev = "";

                    if (!mod[0].IdPai.HasValue)
                    {
                        pag.IdPessoa = mod[0].IdMae;
                        ev = Comum.ListarPor(pag, "Pagamento");
                        pagamento = JsonConvert.DeserializeObject<List<PagamentoViewModels>>(ev);
                    }
                    else
                    {
                        pag.IdPessoa = mod[0].IdPai;
                        ev = Comum.ListarPor(pag, "Pagamento");
                        pagamento = JsonConvert.DeserializeObject<List<PagamentoViewModels>>(ev);
                        if (pagamento.Count() == 0)
                        {
                            pag.IdPessoa = mod[0].IdMae;
                            ev = Comum.ListarPor(pag, "Pagamento");
                            pagamento = JsonConvert.DeserializeObject<List<PagamentoViewModels>>(ev);
                        }
                    }

                    PagamentoViewModels model = new Models.PagamentoViewModels();

                    SeletivaViewModels seletiva;

                    if (pagamento.Count() > 0)
                    {
                        var sel = Comum.ListarPorId(pagamento[0].IdSeletiva.Value, "Seletiva");
                        seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(sel.Result);
                    }
                    else
                    {
                        var sel = Comum.Listar(string.Empty, "Seletiva");
                        List<SeletivaViewModels> lstSeletiva = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(sel.Result);
                        seletiva = lstSeletiva.Count() > 0 ? lstSeletiva.FirstOrDefault(a => a.Status == 0) : new Models.SeletivaViewModels();
                    }

                    if (pagamento.Count() == 0)
                    {
                        model.IdSeletiva = seletiva.Id;
                        model.IdEvento = seletiva.IdEvento;
                        model.DataPagamento = DateTime.Now;
                        model.ValorPagamento = seletiva.Valor;
                        model.TipoPagamento = tipo == "1" ? "P" : tipo; ;
                        model.Status = tipo == "1" ? "P" : "3";
                        model.IdPessoa = mod[0].IdMae;
                        model.Inscricao = inscricao;
                        model.CodigoReferencia = Guid.NewGuid().ToString();

                        idPagamento = Comum.Gravar<PagamentoViewModels>(model, "Pagamento");
                    }
                    else if (pagamento.Count(a => a.Status == "7") > 0)
                    {
                        model = pagamento.FirstOrDefault(b => b.Status == "7");
                        model.DataPagamento = DateTime.Now;
                        model.ValorPagamento = seletiva.Valor;
                        model.TipoPagamento = tipo == "1" ? "P" : tipo;
                        model.Status = tipo == "1" ? "P" : "3";
                        model.CodigoReferencia = Guid.NewGuid().ToString();
                        model.Id = 0;

                        idPagamento = Comum.Gravar<PagamentoViewModels>(model, "Pagamento");
                    }
                    else
                    {
                        model = pagamento[0];
                        model.TipoPagamento = tipo == "1" ? "P" : tipo;
                        model.Status = tipo == "1" ? "P" : "3";
                        model.DataAlteracao = DateTime.Now;
                        idPagamento = model.Id.Value;
                        Comum.Alterar<PagamentoViewModels>(model, "Pagamento");
                    }

                    var pes = Comum.ListarPorId(model.IdPessoa.Value, "Pessoa");

                    PessoaModels pessoa = JsonConvert.DeserializeObject<PessoaModels>(pes.Result);

                    if (idPagamento > 0)
                    {
                        if (tipo == "1")
                        {
                            System.Collections.Specialized.NameValueCollection postData = new System.Collections.Specialized.NameValueCollection();
                            postData.Add("email", ConfigurationManager.AppSettings["Email"]);
                            postData.Add("token", ConfigurationManager.AppSettings["Token"]);
                            postData.Add("currency", "BRL");
                            postData.Add("itemId1", "0001");
                            postData.Add("itemDescription1", seletiva.Nome);
                            postData.Add("itemAmount1", model.ValorPagamento.Value.ToString(new CultureInfo("en-US")));
                            postData.Add("itemQuantity1", "1");
                            postData.Add("itemWeight1", "200");
                            postData.Add("reference", model.CodigoReferencia);
                            postData.Add("senderName", pessoa.Nome);
                            postData.Add("senderAreaCode", Funcoes.LimparFormatacao(pessoa.Celular).Substring(0, 2));
                            postData.Add("senderPhone", Funcoes.LimparFormatacao(pessoa.Celular).Substring(2));
                            postData.Add("senderEmail", pessoa.Email);
                            postData.Add("shippingAddressRequired", "false");

                            //String que receberá o XML de retorno.
                            string xmlString = null;

                            //Webclient faz o post para o servidor de pagseguro.
                            using (WebClient wc = new WebClient())
                            {
                                //Informa header sobre URL.
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                                //Faz o POST e retorna o XML contendo resposta do servidor do pagseguro.
                                var result = wc.UploadValues(ConfigurationManager.AppSettings["ApiPagSeguro"], postData);

                                //Obtém string do XML.
                                xmlString = Encoding.ASCII.GetString(result);
                            }

                            //Cria documento XML.
                            XmlDocument xmlDoc = new XmlDocument();

                            //Carrega documento XML por string.
                            xmlDoc.LoadXml(xmlString);

                            //Obtém código de transação (Checkout).
                            var code = xmlDoc.GetElementsByTagName("code")[0];

                            //Obtém data de transação (Checkout).
                            var date = xmlDoc.GetElementsByTagName("date")[0];

                            //Monta a URL para pagamento.
                            //var paymentUrl = string.Concat(ConfigurationManager.AppSettings["RedirectPagSeguro"], code.InnerText);

                            return Json(new { Status = "Success", Data = code.InnerText }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Status = "Success", Data = "3" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { Status = "Error", Data = "Falha ao acessar a PagSeguro. Favor contate o administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Status = "Error", Data = "Cadastro incompleto. Dados da(o) modelo não encontrado. Favor contate o administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Comum.Excluir(idPagamento, "Pagamento");
                return Json(new { Status = "Error", Data = string.Format("Falha ao gerar o pagamento: {0}. Favor contate o administrador do sistema!", ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SeletivaAvaliacao()
        {
            List<SeletivaViewModels> seletivas = new List<SeletivaViewModels>();

            var ev = SeletivaSrv.ListarSeletivas();

            seletivas = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(ev.Result);

            return View(seletivas);
        }

        public ActionResult FiltrarAprovados(int idSeletiva)
        {
            ViewBag.IdSeletiva = idSeletiva;
            return View();
        }

        public ActionResult EscolherAprovados(int idSeletiva, string nomeSeletiva)
        {
            ViewBag.IdSeletiva = idSeletiva;
            ViewBag.NomeSeletiva = nomeSeletiva;
            return View();
        }

        public ActionResult SeletivaAprovados()
        {
            List<SeletivaViewModels> seletivas = new List<SeletivaViewModels>();

            var ev = SeletivaSrv.ListarSeletivas();

            seletivas = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(ev.Result);

            return View(seletivas);
        }

        public ActionResult Aprovados(int idSeletiva)
        {
            ViewBag.IdSeletiva = idSeletiva;
            return View();
        }

        [HttpPost, ActionName("DeleteImagem")]
        public virtual JsonResult DeleteImagem(int id)
        {
            //var domainObj = Comum.Excluir(id, "Imagem");
            string diretorioCrianca = caminhoImagensCrianca;
            var im = Comum.ListarPorId(id, "Imagem");

            ImagemModels img = JsonConvert.DeserializeObject<ImagemModels>(im.Result);

            if (img != null && img.Id.HasValue)
            {
                diretorioCrianca += "\\" + img.IdPessoa + "\\" + img.Nome;
                try
                {
                    System.IO.File.Delete(diretorioCrianca);
                }
                catch (Exception ex)
                {
                    return Json(new { Status = "Error", Data = "Falha ao excluir a imagem " + "Erro: " + ex.Message + ". Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }

                if (Comum.Excluir(id, "Imagem"))
                {
                    var imga = Comum.ListarPor(new ImagemModels { IdPessoa = img.IdPessoa }, "Imagem");
                    List<ImagemModels> imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(imga);
                    return Json(new { Status = "Success", Data = imagens }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { Status = "Error", Data = "Falha ao excluir a imagem. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }

            return Json("");
        }

        [HttpPost, ActionName("ImagemPrincipal")]
        public virtual JsonResult ImagemPrincipal(int id)
        {
            var im = Comum.ListarPorId(id, "Imagem");

            ImagemModels img = JsonConvert.DeserializeObject<ImagemModels>(im.Result);

            if (img != null && img.Id.HasValue)
            {
                var imgAnterio = Comum.ListarPor(new ImagemModels { IdPessoa = img.IdPessoa, Tipo = "0" }, "Imagem");
                List<ImagemModels> imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(imgAnterio);

                foreach (var item in imagens)
                {
                    item.Tipo = "1";
                    Comum.Alterar<ImagemModels>(item, "Imagem");
                }

                img.Tipo = "0";
                if (Comum.Alterar<ImagemModels>(img, "Imagem"))
                {
                    var imga = Comum.ListarPor(new ImagemModels { IdPessoa = img.IdPessoa }, "Imagem");
                    imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(imga);
                    return Json(new { Status = "Success", Data = imagens }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { Status = "Error", Data = "Falha ao alterar imagem inicial. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }

            return Json("");
        }

        [HttpPost, ActionName("MarcarImpressao")]
        public virtual JsonResult MarcarImpressao(int id, string status)
        {
            var im = Comum.ListarPorId(id, "Imagem");

            ImagemModels img = JsonConvert.DeserializeObject<ImagemModels>(im.Result);

            if (img != null && img.Id.HasValue)
            {
                img.Tipo = status;
                if (Comum.Alterar<ImagemModels>(img, "Imagem"))
                {
                    var imga = Comum.ListarPor(new ImagemModels { IdPessoa = img.IdPessoa }, "Imagem");
                    List<ImagemModels> imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(imga);
                    return Json(new { Status = "Success", Data = imagens }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { Status = "Error", Data = "Falha ao alterar imagem inicial. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }

            return Json("");
        }

        public JsonResult CarregarImagens()
        {
            int idCrianca = Convert.ToInt32(Request.Params["idCrianca"]);
            var img = Request.Params["imagem0"];
            bool ok = false;
            // Cria o diretório que irá armazenar as imagens das crianças
            if (!Directory.Exists(caminhoImagensCrianca))
                Directory.CreateDirectory(caminhoImagensCrianca);

            string diretorioCrianca = caminhoImagensCrianca + idCrianca;

            // Cria a pasta epecífica da criança
            if (!Directory.Exists(diretorioCrianca))
                Directory.CreateDirectory(diretorioCrianca);

            int cont = 0;

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase postedFile = Request.Files[fileName];

                if (postedFile.ContentLength > 0)
                {
                    // UnitOfWorkBLL.EmpresaBLL.AlterarLogo(Convert.ToInt32(CookieHelper.CODIGO), postedFile.FileName);

                    // Salva a imagaem da criança em sua respectiva pasta
                    postedFile.SaveAs(diretorioCrianca + "\\" + postedFile.FileName);

                    string url = string.Format("http://www.minasfashionkids.com.br/mfk/Content/ImagensCrianca/{0}/{1}", idCrianca, postedFile.FileName);

                    Comum.Gravar<ImagemModels>(new ImagemModels { Altura = 0, IdPessoa = idCrianca, Largura = 0, Nome = postedFile.FileName, Tipo = cont == 0 ? "1" : "1", URL = url }, "Imagem");
                    cont++;

                    ok = true;
                }
            }

            if (ok)
            {
                var imga = Comum.ListarPor(new ImagemModels { IdPessoa = idCrianca }, "Imagem");
                List<ImagemModels> imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(imga);
                return Json(new { Status = "Success", Data = imagens }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = ok });
        }

        public ActionResult GerarFicha(string id)
        {
            //id = "953,218";
            var ids = id.Split(',');
            var nome = string.Format("{0}.pdf", ids[2]);
            //return new ActionAsPdf("FichaInscricao", new { id = id }) { FileName = nome };         

            //add code to save the pdf to the temp folder and return the filename to client
            var fileName = nome;
            string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);

            var file = new ActionAsPdf("FichaInscricao", new { id = id }) { FileName = nome };
            var byteArray = file.BuildPdf(ControllerContext);
            var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
            return Json(new { fileName = fileName });
        }

        public ActionResult FichaInscricao(string id)
        {
            var ids = id.Split(',');
            ModeloSeletivaModels inscrito = new ModeloSeletivaModels();
            var c = InscricaoSeletivaSrv.RecuperarInscrito(int.Parse(ids[0]));

            if (!string.IsNullOrEmpty(c.Result))
                inscrito = JsonConvert.DeserializeObject<ModeloSeletivaModels>(c.Result);

            if (inscrito.Id > 0)
            {
                if (ids[3].Contains("T") || ids[3].Contains("R"))
                {
                    var reps1 = PessoaSrv.BuscarPessoa(inscrito.IdMae);
                    inscrito.Responsavel1 = JsonConvert.DeserializeObject<PessoaModels>(reps1.Result);

                    if (inscrito.IdPai > 0)
                    {
                        var reps2 = PessoaSrv.BuscarPessoa(inscrito.IdPai);
                        inscrito.Responsavel2 = JsonConvert.DeserializeObject<PessoaModels>(reps2.Result);
                    }
                }

                inscrito.Idade = Util.CalculateIdade(inscrito.Crianca.DataNascimento.Value);

                if (ids[3].Contains("T") || ids[3].Contains("P"))
                {
                    inscrito.Pagamento = inscrito.Pagamentos.FirstOrDefault(p => p.Id.Value == int.Parse(ids[1]));

                    if (inscrito.Pagamento != null)
                    {
                        var sel = Comum.ListarPorId(inscrito.Pagamento.IdSeletiva.Value, "Seletiva");
                        inscrito.Seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(sel.Result);
                    }
                    else
                    {
                        inscrito.Pagamento = new PagamentoViewModels();

                        var sel = Comum.Listar(string.Empty, "Seletiva");
                        List<SeletivaViewModels> lstSeletiva = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(sel.Result);

                        inscrito.Seletiva = lstSeletiva.Count() > 0 ? lstSeletiva.FirstOrDefault(a => a.Status == 0) : new Models.SeletivaViewModels();
                    }
                }

                if (ids[3].Contains("T") || ids[3].Contains("C") || ids[3].Contains("I"))
                {
                    var img = Comum.ListarPor(new ImagemModels { IdPessoa = inscrito.IdCrianca }, "Imagem");

                    List<ImagemModels> imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(img);

                    if (imagens.Count() > 0)
                    {
                        ImagemModels imgP = imagens.FirstOrDefault(i => i.Tipo == "0");
                        inscrito.UrlImagem = imgP.URL;
                    }

                    inscrito.Crianca.Imagens = ids[3].Contains("T") || ids[3].Contains("I") ? imagens : null;
                }

                if (ids[3].Contains("T") || ids[3].Contains("C"))
                {
                    if (inscrito.Manequim == null)
                        inscrito.Manequim = new ManequimViewModels { IdModelo = inscrito.IdModelo };
                }
                else
                {
                    inscrito.Crianca = ids[3].Contains("T") || ids[3].Contains("I") ? new PessoaModels { Imagens = inscrito.Crianca.Imagens } : null;
                    inscrito.Manequim = null;
                }

                if (string.IsNullOrEmpty(inscrito.Inscricao))
                    inscrito.Inscricao = string.Format("S{0}{1}{2}{3}", inscrito.Id, inscrito.IdCrianca, inscrito.IdMae, inscrito.IdPai);
            }

            return View(inscrito);
        }

        public ActionResult GerarPdfAprovados(string id)
        {
            var nome = string.Format("Aprovados_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmss"));
            var fileName = nome;
            string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);

            var file = new ActionAsPdf("PdfAprovados", new { id = id }) { FileName = nome };
            var byteArray = file.BuildPdf(ControllerContext);
            var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
            fileStream.Write(byteArray, 0, byteArray.Length);
            fileStream.Close();
            return Json(new { fileName = fileName });
        }

        public ActionResult PdfAprovados(string id)
        {
            //id = string.IsNullOrEmpty(id) ? "20,Teste" : id;
            var ids = id.Split(',');
            List<ModeloModels> modelos = new List<ModeloModels>();
            var c = RelSeletivaSrv.ListarAprovadosSeletivaPdf(int.Parse(ids[0]), int.Parse(ids[1]), int.Parse(ids[2]));

            if (!string.IsNullOrEmpty(c.Result))
                modelos = JsonConvert.DeserializeObject<List<ModeloModels>>(c.Result);

            return View(modelos);
        }

        [DeleteFileAttribute]
        public ActionResult Download(string file)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/temp"), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/pdf", file);
        }
    }
}