﻿using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class HomeController : Controller
    {
        // GET: Administrador/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}