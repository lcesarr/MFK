﻿using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class ParceiroController : Controller
    {
        // GET: Administrador/Parceiro
        public ActionResult Index()
        {
            ParceiroViewModels Parceiros = new ParceiroViewModels { Parceiros = new List<ParceiroModel>() };
            return View(Parceiros);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Create()
        {
            ParceiroModel lj = new ParceiroModel();
            return View(lj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(ParceiroModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_dadosLoja.cshtml", model);
            }

            return PartialView(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step1(EmpresaModels model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_dadosLoja.cshtml", model);
            }

            ParceiroModel l = new ParceiroModel { Empresa = model };

            var j = JsonConvert.SerializeObject(l);

            return Json(new { Status = "Success", Data = j }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step2(List<EnderecoModels> model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_endereco.cshtml", model);
            }

            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        [HttpGet]
        public ActionResult Step2Voltar(string json)
        {
            ParceiroModel l = JsonConvert.DeserializeObject<ParceiroModel>(json);

            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_endereco.cshtml", l.Enderecos.Where(e => e.Cep != null).ToList());

        }
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step3(List<ResponsavelModels> model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_responsaveisLoja.cshtml", model);
            }

            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step4(List<ImagemModels> model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_imagens.cshtml", model);
            }

            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_imagens.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult NovoEndereco(List<EnderecoModels> model)
        {
            if (model == null)
                model = new List<EnderecoModels>();

            model.Add(new EnderecoModels());
            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_endereco.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult RemoverEndereco(List<EnderecoModels> model)
        {
            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_endereco.cshtml", model.Where(m => m != null).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult NovoResponsavel(List<ResponsavelModels> model)
        {
            model.Add(new ResponsavelModels());
            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_responsaveisLoja.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult RemoverResponsavel(List<ResponsavelModels> model)
        {
            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_responsaveisLoja.cshtml", model.Where(m => m != null).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult NovaImagem(List<ImagemModels> model)
        {
            model.Add(new ImagemModels());
            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_imagens.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult RemoverImagem(List<ImagemModels> model)
        {
            return PartialView("~/Areas/Administrador/Views/Parceiro/Partial/_imagens.cshtml", model.Where(m => m != null).ToList());
        }
    }
}