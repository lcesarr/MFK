﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class TipoPatrocinioController : Controller
    {
        // GET: Administrador/TipoPatrocinio
        public ActionResult Index()
        {
            List<TipoPatrocinioViewModels> parcerias = new List<TipoPatrocinioViewModels>();

            var parceria = Comum.Listar("", "TipoPatrocinio");

            parcerias = JsonConvert.DeserializeObject<List<TipoPatrocinioViewModels>>(parceria.Result);

            return View(parcerias);
        }

        // GET: Administrador/TipoPatrocinio
        public ActionResult Create()
        {
            TipoPatrocinioViewModels model = new Web.Models.TipoPatrocinioViewModels { Status = 0 };
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(TipoPatrocinioViewModels model)
        {
            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (model.Id == 0)
            {
                int e = Comum.Gravar<TipoPatrocinioViewModels>(model, "TipoPatrocinio");

                if (e > 0)
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = "Error", Data = "Falha ao cadastrar o Tipo de parceria. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Comum.Alterar<TipoPatrocinioViewModels>(model, "TipoPatrocinio"))
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Tipo de parceria. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        private TipoPatrocinioViewModels PreencherModels(int id, string url)
        {
            TipoPatrocinioViewModels TipoPatrocinio = new TipoPatrocinioViewModels { Url = url };

            var ev = Comum.ListarPorId(id, "TipoPatrocinio");

            TipoPatrocinio = JsonConvert.DeserializeObject<TipoPatrocinioViewModels>(ev.Result);

            return TipoPatrocinio;
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "TipoPatrocinio");

            return Json("");
        }
    }
}