﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class RegulamentoController : Controller
    {
        public ActionResult Index()
        {
            List<RegulamentoViewModels> regulamentos = new List<RegulamentoViewModels>();

            var regulamento = Comum.Listar("", "Regulamento");

            regulamentos = JsonConvert.DeserializeObject<List<RegulamentoViewModels>>(regulamento.Result);

            return View(regulamentos);
        }

        // GET: Administrador/TipoParceria
        public ActionResult Create()
        {
            RegulamentoViewModels model = new Web.Models.RegulamentoViewModels { Status = 0 };
            return View(model);
        }

        [System.Web.Mvc.ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(RegulamentoViewModels model)
        {
            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (!model.Id.HasValue)
            {
                int e = Comum.Gravar<RegulamentoViewModels>(model, "Regulamento");

                if (e > 0)
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = "Error", Data = "Falha ao cadastrar o Regulamento. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Comum.Alterar<RegulamentoViewModels>(model, "Regulamento"))
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Regulamento. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "Regulamento");

            return Json("");
        }

        private RegulamentoViewModels PreencherModels(int id, string url)
        {
            RegulamentoViewModels regulamento = new RegulamentoViewModels();

            var ev = Comum.ListarPorId(id, "Regulamento");

            regulamento = JsonConvert.DeserializeObject<RegulamentoViewModels>(ev.Result);

            regulamento.Url = url;

            return regulamento;

        }
    }
}