﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class EventoController : Controller
    {
        const string Control = "Evento";
        // GET: Administrador/Evento
        public ActionResult Index()
        {
            List<EventoModels> eventos = new List<EventoModels>();

            var ev = Comum.Listar(string.Empty, Control);

            eventos = JsonConvert.DeserializeObject<List<EventoModels>>(ev.Result);

            return View(eventos);
        }

        // GET: Administrador/Mapa
        public ActionResult Mapa()
        {
            return View();
        }

        public ActionResult Lojista()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View(new EventoModels());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(EventoModels model)
        {
            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (model.Id == 0)
            {
                var en = Comum.Gravar<EnderecoModels>(model.Local, "Endereco");

                if (en > 0)
                {
                    model.IdEndereco = en;
                    var e = Comum.Gravar<EventoModels>(model, "Evento");

                    if (e == 0)
                    {
                        Comum.Excluir(en, "Endereco");
                        return Json(new { Status = "Error", Data = "Falha ao gravar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Status = "Error", Data = "Falha ao gravar o Evento. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (model.Id == model.Local.Id)
                    model.Local.Id = model.IdEndereco;

                var r = Comum.ListarPorId(model.Local.Id.Value, "Endereco");

                if (!string.IsNullOrEmpty(r.ToString()))
                {
                    if (Comum.Alterar<EnderecoModels>(model.Local, "Endereco"))
                    {
                        if (!Comum.Alterar<EventoModels>(model, "Evento"))
                            return Json(new { Status = "Error", Data = "Falha ao alterar o Evento. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);

                        return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var end = Comum.Gravar<EnderecoModels>(model.Local, "Endereco");

                    if (end > 0)
                    {
                        model.IdEndereco = end;
                        if (!Comum.Alterar<EventoModels>(model, "Evento"))
                        {
                            return Json(new { Status = "Error", Data = "Falha ao alterar o Evento. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }

                        return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "Evento");

            return Json("");
        }

        private EventoModels PreencherModels(int id, string url)
        {
            EventoModels evento = new EventoModels();

            var ev = Comum.ListarPorIdUnion(id, Control, "BuscarEvento");

            evento = JsonConvert.DeserializeObject<EventoModels>(ev.Result);

            if (evento.Local == null)
                evento.Local = new Models.Comum.EnderecoModels();

            evento.Url = url;

            return evento;

        }
    }
}