﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class LojistaController : Controller
    {
        // GET: Administrador/Lojista
        public ActionResult Index()
        {
            List<EmpresaModels> lojistas = new List<EmpresaModels>();

            var ev = Comum.Listar(string.Empty, "Lojista");

            lojistas = JsonConvert.DeserializeObject<List<EmpresaModels>>(ev.Result);

            return View(lojistas);
        }

        [HttpPost]
        public ActionResult Index(string tipo)
        {
            List<EmpresaModels> lojistas = new List<EmpresaModels>();

            var ev = Comum.ListarPor<EmpresaModels>(new EmpresaModels { Tipo = tipo }, "Lojista");

            if (!string.IsNullOrEmpty(ev))
                lojistas = JsonConvert.DeserializeObject<List<EmpresaModels>>(ev);

            return View(lojistas);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Create()
        {
            EmpresaModels emp = new EmpresaModels
            {
                Tipo = "L",
                Url = string.Empty,
                Contatos = new List<ContatoModels>(),
                Endereco = new EnderecoModels { Tipo = "C" },
                Responsavel = new PessoaModels { Tipo = "L" }
            };

            emp.Contatos.Add(new Models.Comum.ContatoModels { Tipo = "T" });
            emp.Contatos.Add(new Models.Comum.ContatoModels { Tipo = "C" });
            emp.Contatos.Add(new Models.Comum.ContatoModels { Tipo = "E" });

            return View(emp);
        }

        [HttpPost]
        public ActionResult Create(EmpresaModels model)
        {
            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (!model.Id.HasValue)
            {
                model.CNPJ = Funcoes.LimparFormatacao(model.CNPJ);
                var e = Comum.Gravar<EmpresaModels>(model, "Lojista");

                if (e > 0)
                {
                    model.Endereco.IdEmpresa = e;

                    var en = Comum.Gravar<EnderecoModels>(model.Endereco, "Endereco");

                    if (en == 0)
                    {
                        Comum.Excluir(e, "Lojista");
                        return Json(new { Status = "Error", Data = "Falha ao gravar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    model.Responsavel.IdEmpresa = e;
                    model.Responsavel.Cpf = Funcoes.LimparFormatacao(model.Responsavel.Cpf);
                    var re = Comum.Gravar<PessoaModels>(model.Responsavel, "Pessoa");

                    if (re == 0)
                    {
                        Comum.Excluir(en, "Endereco");
                        Comum.Excluir(e, "Lojista");
                        return Json(new { Status = "Error", Data = "Falha ao gravar o Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    foreach (ContatoModels item in model.Contatos)
                    {
                        item.IdEmpresa = e;

                        var co = Comum.Gravar<ContatoModels>(item, "Contato");

                        if (co == 0)
                        {
                            Comum.Excluir(re, "Endereco");
                            Comum.Excluir(en, "Endereco");
                            Comum.Excluir(e, "Lojista");
                            Comum.Excluir(e, "Contato");
                            return Json(new { Status = "Error", Data = "Falha ao gravar o Contato. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = "Error", Data = "Falha ao gravar o Lojista. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (Comum.Alterar<EmpresaModels>(model, "Lojista"))
                {
                    model.Endereco.IdEmpresa = model.Id.Value;

                    if (!Comum.Alterar<EnderecoModels>(model.Endereco, "Endereco"))
                    {
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    if (!Comum.Alterar<PessoaModels>(model.Responsavel, "Pessoa"))
                    {
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Responsável. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }

                    foreach (ContatoModels item in model.Contatos)
                    {
                        if (!Comum.Alterar<ContatoModels>(item, "Contato"))
                        {
                            return Json(new { Status = "Error", Data = "Falha ao alterar os Contatos. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Lojista. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "Lojista");

            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step1(EmpresaModels model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_dadosLoja.cshtml", model);
            }

            LojistaModel l = new LojistaModel { Empresa = model };

            var j = JsonConvert.SerializeObject(l);

            return Json(new { Status = "Success", Data = j }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step2(List<EnderecoModels> model)
        {
            int cont = model.Count();

            for (int i = 0; i < cont; i++)
            {
                ModelState.Remove("[" + i + "].Id");
            }

            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_endereco.cshtml", model);
            }

            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step3(List<ContatoModels> model)
        {
            model = new List<ContatoModels>();
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_contatosLoja.cshtml", model);
            }

            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Step4(List<PessoaModels> model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_responsaveisLoja.cshtml", model);
            }

            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult NovoEndereco(List<EnderecoModels> model)
        {
            if (model == null)
                model = new List<EnderecoModels> { new Models.Comum.EnderecoModels { Tipo = "C" } };

            model.Add(new EnderecoModels());
            return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_endereco.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult RemoverEndereco(List<EnderecoModels> model)
        {
            return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_endereco.cshtml", model.Where(m => m != null).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult NovoResponsavel(List<PessoaModels> model)
        {
            if (model == null)
                model = new List<PessoaModels> { new PessoaModels { Tipo = "L" } };

            model.Add(new PessoaModels { Tipo = "L" });

            return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_responsaveisLoja.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult RemoverResponsavel(List<PessoaModels> model)
        {
            return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_responsaveisLoja.cshtml", model.Where(m => m != null).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult NovoContato(List<ContatoModels> model)
        {
            if (model == null)
                model = new List<ContatoModels>();

            model.Add(new ContatoModels());
            return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_contatosLoja.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult RemoverContato(List<ContatoModels> model)
        {
            return PartialView("~/Areas/Administrador/Views/Lojista/Partial/_contatosLoja.cshtml", model.Where(m => m != null).ToList());
        }

        private EmpresaModels PreencherModels(int id, string url)
        {
            EmpresaModels empresa = new EmpresaModels { Url = url };

            var ev = LojistaSrv.ListarPorId(id);

            empresa = JsonConvert.DeserializeObject<EmpresaModels>(ev.Result);

            return empresa;
        }
    }
}