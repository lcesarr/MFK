﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class JuradosController : Controller
    {
        // GET: Administrador/Jurados
        public ActionResult Index()
        {
            List<PessoaModels> jurados = new List<PessoaModels>();

            var jurado = Comum.ListarPor<PessoaModels>(new PessoaModels { Tipo = "J" }, "Pessoa");

            if (!string.IsNullOrEmpty(jurado))
                jurados = JsonConvert.DeserializeObject<List<PessoaModels>>(jurado);

            return View(jurados);
        }

        // GET: Administrador/Jurados
        public ActionResult Create()
        {
            JuradoModels jurado = new JuradoModels();
            jurado.Usuario.DadosPessoais.Contatos = new List<ContatoModels>();
            jurado.Usuario.DadosPessoais.Contatos.Add(new ContatoModels { Tipo = "T" });
            jurado.Usuario.DadosPessoais.Contatos.Add(new ContatoModels { Tipo = "C" });
            jurado.Usuario.DadosPessoais.Contatos.Add(new ContatoModels { Tipo = "E" });
            return View(jurado);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(JuradoModels model)
        {
            ContatoModels contE = model.Contatos.Where(m => m.Tipo.Equals("E")).FirstOrDefault();

            if (contE != null && !Funcoes.ValidarEmail(contE.Dado, true))
            {
                ModelState.AddModelError(string.Empty, "E-mail inválido ou não informado");
            }

            ContatoModels contT = model.Contatos.Where(m => m.Tipo.Equals("T")).FirstOrDefault();

            if (contT != null && !Funcoes.ValidarTelefone(contT.Dado, true))
            {
                ModelState.AddModelError(string.Empty, "Telefone inválido ou não informado");
            }

            ContatoModels contC = model.Contatos.Where(m => m.Tipo.Equals("C")).FirstOrDefault();

            if (contC != null && !Funcoes.ValidarTelefone(contC.Dado, true))
            {
                ModelState.AddModelError(string.Empty, "Celular inválido ou não informado");
            }


            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            int pessoa = 0;
            int usuario = 0;
            int endereco = 0;
            int contato = 0;

            if (!model.Usuario.DadosPessoais.Id.HasValue)
            {
                model.Usuario.DadosPessoais.Tipo = "J";
                pessoa = Comum.Gravar<PessoaModels>(model.Usuario.DadosPessoais, "Pessoa");
                if (pessoa > 0)
                {
                    model.Usuario.CodigoAcesso = "002";
                    model.Usuario.Status = 0;
                    model.Usuario.IdPessoa = pessoa;
                    model.Usuario.Password = Util.CriptografaSenha(model.Usuario.Password);
                    usuario = Comum.Gravar<UsuarioModels>(model.Usuario, "Usuario");
                    if (usuario > 0)
                    {
                        model.Usuario.DadosPessoais.Endereco.IdPessoa = pessoa;
                        endereco = Comum.Gravar<EnderecoModels>(model.Usuario.DadosPessoais.Endereco, "Endereco");
                        if (endereco > 0)
                        {
                            foreach (var item in model.Usuario.DadosPessoais.Contatos)
                            {
                                item.IdPessoa = pessoa;

                                contato = Comum.Gravar<ContatoModels>(item, "Contato");

                                if (contato == 0)
                                {
                                    Comum.Excluir(pessoa, "Pessoa");
                                    Comum.Excluir(usuario, "Usuario");
                                    Comum.Excluir(endereco, "Endereco");
                                    return Json(new { Status = "Error", Data = "Falha ao gravar o Contato. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                                }
                            }

                            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            Comum.Excluir(pessoa, "Pessoa");
                            Comum.Excluir(usuario, "Usuario");
                            return Json(new { Status = "Error", Data = "Falha ao gravar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        Comum.Excluir(pessoa, "Pessoa");
                        return Json(new { Status = "Error", Data = "Falha ao criar o Usuário do jurado. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { Status = "Error", Data = "Falha ao gravar o Jurado. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                model.Usuario.DadosPessoais.Tipo = "J";
                if (Comum.Alterar<PessoaModels>(model.Usuario.DadosPessoais, "Pessoa"))
                {
                    model.Usuario.Password = string.IsNullOrEmpty(model.Usuario.Senha) ? model.Usuario.Password : Util.CriptografaSenha(model.Usuario.Senha);
                    if (Comum.Alterar<UsuarioModels>(model.Usuario, "Usuario"))
                    {
                        if (Comum.Alterar<EnderecoModels>(model.Usuario.DadosPessoais.Endereco, "Endereco"))
                        {
                            foreach (var item in model.Contatos)
                            {
                                if (item.Id == 0)
                                {
                                    item.IdPessoa = model.Usuario.DadosPessoais.Id.Value;
                                    contato = Comum.Gravar<ContatoModels>(item, "Contato");

                                    if (contato == 0)
                                        return Json(new { Status = "Error", Data = "Falha ao gravar o novo Contato. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (!Comum.Alterar<ContatoModels>(item, "Contato"))
                                    return Json(new { Status = "Error", Data = "Falha ao alterar o Contato. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                            }

                            return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { Status = "Error", Data = "Falha ao alterar o Endereço. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Usuário. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Jurado. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "Pessoa");

            return Json("");
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        private JuradoModels PreencherModels(int id, string url)
        {
            JuradoModels jurado = new JuradoModels { Url = url };

            var ev = UsuarioSrv.BuscarJurado(id);

            jurado.Usuario = JsonConvert.DeserializeObject<UsuarioModels>(ev.Result);

            return jurado;
        }
    }
}