﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Text;

namespace MKF.Web.Areas.Administrador.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Administrador/TipoParceria
        public ActionResult Index()
        {
            List<UsuarioModels> usuarios = new List<UsuarioModels>();

            var usuario = UsuarioSrv.ListarTodos();

            usuarios = JsonConvert.DeserializeObject<List<UsuarioModels>>(usuario.Result);

            return View(usuarios);
        }

        [HttpPost]
        public ActionResult Index(string nome, string cpf, string user)
        {
            return View(BuscarUsuarios(nome, cpf, user));
        }

        private List<UsuarioModels> BuscarUsuarios(string nome, string cpf, string user)
        {

            nome = string.IsNullOrEmpty(nome) ? "null" : nome;
            cpf = string.IsNullOrEmpty(cpf) ? "null" : Funcoes.LimparFormatacao(cpf);
            user = string.IsNullOrEmpty(user) ? "null" : user;

            var usuario = UsuarioSrv.ListarPor(nome, cpf, user);

            return JsonConvert.DeserializeObject<List<UsuarioModels>>(usuario.Result);
        }

        // GET: Administrador/TipoParceria
        public ActionResult Create()
        {
            UsuarioModels model = new UsuarioModels { Url = string.Empty, CodigoAcesso = "001", TipoAcesso = "A", DadosPessoais = new PessoaModels { IdEmpresa = Util.RecuperarIdEmpresa(), Tipo = "A" } };
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult Create(UsuarioModels model)
        {
            if (model.DadosPessoais != null && string.IsNullOrEmpty(model.DadosPessoais.Cpf))
                ModelState.AddModelError("DadosPessoais.Cpf", "Favor informe o campo Cpf");

            if (model.Id > 0)
                ModelState.Remove("Password");

            if (!model.Id.HasValue)
            {
                var u = BuscarUsuarios(string.Empty, model.DadosPessoais.Cpf, string.Empty);

                if (u != null && u.Count() > 0)
                    ModelState.AddModelError("DadosPessoais.Cpf", "Cpf informado já se encontra cadastrado");

                u = BuscarUsuarios(string.Empty, string.Empty, model.User);

                if (u != null && u.Count() > 0)
                    ModelState.AddModelError("User", "Usuário informado já se encontra cadastrado");
            }

            if (!ModelState.IsValid)
            {
                var errorList = (from item in ModelState.Values
                                 from error in item.Errors
                                 select error.ErrorMessage).ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in errorList)
                {
                    sb.AppendFormat("<p>{0}</p>", item);
                }

                return Json(new { Status = "Warning", Data = sb.ToString() }, JsonRequestBehavior.AllowGet);
            }

            if (model.Id == null || model.Id == 0)
            {
                model.CodigoAcesso = "001";
                model.Password = Util.CriptografaSenha(model.Password);
                int e = Comum.Gravar<PessoaModels>(model.DadosPessoais, "Pessoa/Incluir/Pessoa");

                if (e > 0)
                {
                    model.IdPessoa = e;
                    int u = Comum.Gravar<UsuarioModels>(model, "Usuario");
                    if (u > 0)
                        return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        bool r = Comum.Excluir(e, "Pessoa");
                        return Json(new { Status = "Error", Data = "Falha ao cadastrar o Usuário. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Status = "Error", Data = "Falha ao cadastrar o Usuário. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.Password))
                    model.Password = model.Senha;
                else
                    model.Password = Util.CriptografaSenha(model.Password);

                if (model.CodigoAcesso == null && model.TipoAcesso == "J") model.CodigoAcesso = "002";
                else if (model.CodigoAcesso == null && model.TipoAcesso == "A") model.CodigoAcesso = "001";

                if (Comum.Alterar<PessoaModels>(model.DadosPessoais, "Pessoa"))
                {
                    if (Comum.Alterar<UsuarioModels>(model, "Usuario"))
                        return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        return Json(new { Status = "Error", Data = "Falha ao alterar o Usuário. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Status = "Error", Data = "Falha ao alterar o Usuário. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult Edit(int id, string url)
        {
            return View("Create", PreencherModels(id, url));
        }

        private UsuarioModels PreencherModels(int id, string url)
        {
            UsuarioModels usuario = new UsuarioModels { Url = url };

            var ev = UsuarioSrv.ListarPorId(id);

            usuario = JsonConvert.DeserializeObject<UsuarioModels>(ev.Result);

            usuario.Senha = usuario.Password;

            usuario.Password = string.Empty;

            return usuario;
        }

        [HttpPost, ActionName("Delete")]
        public virtual JsonResult Delete(int id)
        {
            var domainObj = Comum.Excluir(id, "Usuario");

            return Json("");
        }
    }
}