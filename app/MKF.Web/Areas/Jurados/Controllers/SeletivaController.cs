﻿using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using MKF.Web.Models;
using MKF.Web.Models.Comum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Areas.Jurados.Controllers
{
    public class SeletivaController : Controller
    {
        // GET: Administrador/Seletiva
        public ActionResult Index()
        {
            var t = Util.RecuperarIdPessoa();
            List<SeletivaViewModels> seletivas = new List<SeletivaViewModels>();

            var ev = RelJuradosSeletiva.ListarSeletivasJurado(t.Value);

            seletivas = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(ev.Result);

            return View(seletivas);
        }

        public ActionResult Modelos(int idSeletiva)
        {
            ViewBag.IdSeletiva = idSeletiva;
            return View();
        }

        public ActionResult Avaliar(string id)
        {
            var ids = id.Split(',');
            ModeloSeletivaModels inscrito = new ModeloSeletivaModels();
            var c = InscricaoSeletivaSrv.RecuperarInscrito(int.Parse(ids[0]));

            if (!string.IsNullOrEmpty(c.Result))
                inscrito = JsonConvert.DeserializeObject<ModeloSeletivaModels>(c.Result);

            if (inscrito.Id > 0)
            {
                var reps1 = PessoaSrv.BuscarPessoa(inscrito.IdMae);
                inscrito.Responsavel1 = JsonConvert.DeserializeObject<PessoaModels>(reps1.Result);

                if (inscrito.IdPai > 0)
                {
                    var reps2 = PessoaSrv.BuscarPessoa(inscrito.IdPai);
                    inscrito.Responsavel2 = JsonConvert.DeserializeObject<PessoaModels>(reps2.Result);
                }

                inscrito.Idade = Util.CalculateIdade(inscrito.Crianca.DataNascimento.Value);

                inscrito.Pagamento = inscrito.Pagamentos.FirstOrDefault(p => p.Id.Value == int.Parse(ids[1]));

                if (inscrito.Pagamento != null)
                {
                    var sel = Comum.ListarPorIdUnion(inscrito.Pagamento.IdSeletiva.Value, "Seletiva", "BuscarSeletiva");
                    inscrito.Seletiva = JsonConvert.DeserializeObject<SeletivaViewModels>(sel.Result);
                }
                else
                {
                    inscrito.Pagamento = new PagamentoViewModels();

                    var sel = Comum.Listar(string.Empty, "Seletiva");
                    List<SeletivaViewModels> lstSeletiva = JsonConvert.DeserializeObject<List<SeletivaViewModels>>(sel.Result);

                    inscrito.Seletiva = lstSeletiva.Count() > 0 ? lstSeletiva.FirstOrDefault(a => a.Status == 0) : new Models.SeletivaViewModels();
                }

                var img = Comum.ListarPor(new ImagemModels { IdPessoa = inscrito.IdCrianca }, "Imagem");

                inscrito.Crianca.Imagens = JsonConvert.DeserializeObject<List<ImagemModels>>(img);

                if (inscrito.Crianca.Imagens.Count() > 0)
                    inscrito.UrlImagem = inscrito.Crianca.Imagens[0].URL;

                if (inscrito.Manequim == null)
                    inscrito.Manequim = new ManequimViewModels { IdModelo = inscrito.IdModelo };
            }

            if (string.IsNullOrEmpty(inscrito.Inscricao))
                inscrito.Inscricao = string.Format("S{0}{1}{2}{3}", inscrito.Id, inscrito.IdCrianca, inscrito.IdMae, inscrito.IdPai);

            var avJurado = AvaliacaoSrv.ListarAvaliacaoJuradoModelo(inscrito.Id, Util.RecuperarIdPessoa().Value);

            inscrito.Avaliacoes = JsonConvert.DeserializeObject<List<AvaliacaoModel>>(avJurado.Result);

            return View(inscrito);
        }

        [HttpPost]
        public JsonResult SalvarAvaliacao(string id, string resultado, string total, string media)
        {
            string[] notas = resultado.Split(';');
            var i = 0;
            AvaliacaoSrv.DeletarJuradosSeletiva(int.Parse(id), Util.RecuperarIdPessoa().Value);
            foreach (string item in notas)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    string[] itemava = item.Split('-');
                    AvaliacaoModel avaliacao = new Models.AvaliacaoModel
                    {
                        DataAvaliacao = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time")),
                        IdModelo = int.Parse(id),
                        Item = itemava[0],
                        Nota = int.Parse(itemava[1]),
                        IdJurado = Util.RecuperarIdPessoa().Value
                    };

                    i = Comum.Gravar<AvaliacaoModel>(avaliacao, "Avaliacao");

                    if (i == 0) break;
                }
            }

            if (i > 0)
            {
                ModeloViewModels model = new Models.ModeloViewModels();

                var c = Comum.ListarPorId(int.Parse(id), "Modelo");

                if (!string.IsNullOrEmpty(c.Result))
                {
                    model = JsonConvert.DeserializeObject<ModeloViewModels>(c.Result);
                    model.Status = 1;
                    model.Avaliacao = int.Parse(total);
                    model.MediaAvaliacao = decimal.Parse(media.Replace('.', ','));
                    if (Comum.Alterar<ModeloViewModels>(model, "Modelo"))
                        return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        AvaliacaoSrv.DeletarJuradosSeletiva(int.Parse(id), Util.RecuperarIdPessoa().Value);
                        return Json(new { Status = "Error", Data = "Falha ao avaliar a criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    AvaliacaoSrv.DeletarJuradosSeletiva(int.Parse(id), Util.RecuperarIdPessoa().Value);
                    return Json(new { Status = "Error", Data = "Falha ao avaliar a criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { Status = "Error", Data = "Falha ao avaliar a criança. Favor entre em contato junto ao administrador do sistema!" }, JsonRequestBehavior.AllowGet);
        }
    }
}