﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using MKF.Web.Areas.Admin.Models;
using MKF.Web.Infra;
using MKF.Web.Infra.Servicos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View(new LoginViewModels { ModoAcesso = "L" });
        }

        // GET: Login
        [HttpPost]
        public ActionResult Index(LoginViewModels model)
        {
            model.EsquecerSenha = new EsqueciSenhaViewModels();

            List<AcessoViewModels> login = new List<AcessoViewModels>();

            if (ModelState.IsValid)
            {
                model.Acesso.Password = Util.CriptografaSenha(model.Acesso.Password);

                var ev = Comum.ListarPor(model.Acesso, "Usuario");

                login = JsonConvert.DeserializeObject<List<AcessoViewModels>>(ev);

                if (login != null && login.Count > 0)
                {
                    var claims = new List<Claim>();

                    // create required claims
                    claims.Add(new Claim(ClaimTypes.NameIdentifier, login[0].Id.ToString()));
                    claims.Add(new Claim(ClaimTypes.Name, login[0].User));

                    if (login[0].IdEmpresa.HasValue)
                        claims.Add(new Claim(ClaimTypes.Sid, login[0].IdEmpresa.Value.ToString()));
                    else
                        claims.Add(new Claim(ClaimTypes.Sid, "0"));

                    claims.Add(new Claim("IdPessoa", login[0].IdPessoa.ToString()));

                    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

                    AuthenticationManager.SignIn(new AuthenticationProperties()
                    {
                        AllowRefresh = true,
                        IsPersistent = false,
                        ExpiresUtc = DateTime.UtcNow.AddDays(1)
                    }, identity);

                    ClaimsPrincipal principal2 = new ClaimsPrincipal(identity);

                    Thread.CurrentPrincipal = principal2;


                    if (login[0].TipoAcesso == "A")
                        return RedirectToAction("Index", "Home", new { area = "Administrador" });
                    else if (login[0].TipoAcesso == "L")
                        return RedirectToAction("Index", "Home", new { area = "Lojista" });
                    else if (login[0].TipoAcesso == "G")
                        return RedirectToAction("Index", "Home", new { area = "Grife" });
                    else if (login[0].TipoAcesso == "J")
                        return RedirectToAction("Index", "Seletiva", new { area = "Jurados" });
                    else if (login[0].TipoAcesso == "I")
                        return RedirectToAction("Index", "Inscricao", new { area = "" });
                }
                else
                {
                    TempData["mensagemErro"] = "Usuário ou senha inválidos";
                }

                //var user = usuarioAppService.GetBy(new Domain.Entities.Usuario
                //{
                //    Login = model.Acesso.Login,
                //    Senha = Util.CriptografaSenha(model.Acesso.Senha),
                //    FlagMaster = null
                //}).ToList();

                //if (user.Count() > 0)
                //{
                //    var claims = new List<Claim>();

                //    // create required claims
                //    claims.Add(new Claim(ClaimTypes.NameIdentifier, user[0].Id.ToString()));
                //    claims.Add(new Claim(ClaimTypes.Name, user[0].Nome));
                //    claims.Add(new Claim("perfilId", user[0].IdPerfil.ToString()));

                //    var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

                //    AuthenticationManager.SignIn(new AuthenticationProperties()
                //    {
                //        AllowRefresh = true,
                //        IsPersistent = false,
                //        ExpiresUtc = DateTime.UtcNow.AddDays(1)
                //    }, identity);

                //return RedirectToAction("Index", "Home", new { area = "Administrador" });
                //}
                //else
                //{
                //    TempData["mensagemErro"] = "Usuário ou senha inválidos";
                //}
            }

            return View(model);
        }

        // GET: Login/Details/5
        public ActionResult EsqueciSenha()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EsqueciSenha(EsqueciSenhaViewModels model)
        {
            if (ModelState.IsValid)
            {

            }

            LoginViewModels l = new LoginViewModels { EsquecerSenha = model, Acesso = new AcessoViewModels(), ModoAcesso = "E" };

            return View("Index", l);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}
