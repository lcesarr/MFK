﻿using System.ComponentModel.DataAnnotations;

namespace MKF.Web.Areas.Admin.Models
{
    public class LoginViewModels
    {
        public LoginViewModels()
        {
            Acesso = new Models.AcessoViewModels { Status = 0 };
            EsquecerSenha = new Models.EsqueciSenhaViewModels();
        }

        public AcessoViewModels Acesso { get; set; }
        public EsqueciSenhaViewModels EsquecerSenha { get; set; }
        public string ModoAcesso { get; set; }
    }

    public class AcessoViewModels
    {
        [Required(ErrorMessage = "Favor informe o campo Código de acesso")]
        [DataType(DataType.Text)]
        [Display(Name = "Código de acesso")]
        public string CodigoAcesso { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Login")]
        [DataType(DataType.Text)]
        [Display(Name = "Login")]
        public string User { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Senha")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        public int Status { get; set; }
        public string TipoAcesso { get; set; }

        public int? Id { get; set; }
        public int? IdPessoa { get; set; }
        public int? IdEmpresa { get; set; }
    }

    public class EsqueciSenhaViewModels
    {
        [Required(ErrorMessage = "Favor informe o E-mail")]
        [DataType(DataType.Text)]
        [Display(Name = "E-mail")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Favor, informar um e-mail válido")]
        public string Email { get; set; }
    }
}