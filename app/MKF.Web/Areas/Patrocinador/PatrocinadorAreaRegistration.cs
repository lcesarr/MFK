﻿using System.Web.Mvc;

namespace MKF.Web.Areas.Patrocinador
{
    public class PatrocinadorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Patrocinador";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Patrocinador_default",
                "Patrocinador/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}