﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class ModeloViewModels
    {
        public ModeloViewModels()
        {
            Crianca = new PessoaModels();
            Responsavel1 = new PessoaModels { Endereco = new Comum.EnderecoModels { Tipo = "R" } };
            Responsavel2 = new PessoaModels { Endereco = new Comum.EnderecoModels { Tipo = "R" } };
            Manequim = new Models.ManequimViewModels();
        }

        public int? Id { get; set; }
        public int? IdEmpresa { get; set; }
        [DataType(DataType.Text)]
        [Display(Name = "Nome Artístico")]
        public string NomeArtistico { get; set; }
        public int? IdCrianca { get; set; }
        public int? IdMae { get; set; }
        public int? IdPai { get; set; }
        public int? Idade { get; set; }
        [Display(Name = "Nome da seletiva")]
        public string SeletivaNome { get; set; }

        public int Status { get; set; }

        [Display(Name = "Horário apresentação")]
        public DateTime? HorarioDesfile { get; set; }

        [Display(Name = "Nº Inscrição")]
        public string Inscricao { get; set; }

        public int? IdEvento { get; set; }
        public int? IdSeletiva { get; set; }
        public decimal ValorSeletiva { get; set; }
        public string Imagem1 { get; set; }
        public string Imagem2 { get; set; }

        public bool Autorizado { get; set; }

        public string Url { get; set; }

        public int? Avaliacao { get; set; }
        public decimal? MediaAvaliacao { get; set; }

        public DateTime DataInicioSeletiva { get; set; }

        public DateTime DataFimSeletiva { get; set; }
        public int IntervaloApresentacao { get; set; }

        public ManequimViewModels Manequim { get; set; }

        public PessoaModels Crianca { get; set; }

        public PessoaModels Responsavel1 { get; set; }

        public PessoaModels Responsavel2 { get; set; }

        public RegulamentoViewModels Autorizacao { get; set; }
    }
}