﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class PagamentoViewModels
    {
        public int? Id { get; set; }
        public int? IdPessoa { get; set; }
        public int? IdEvento { get; set; }
        public int? IdSeletiva { get; set; }
        public decimal? ValorPagamento { get; set; }
        public string TipoPagamento { get; set; }
        public string Status { get; set; }
        public DateTime? DataPagamento { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public string CodigoTransacaoPG { get; set; }
        public string CodigoReferencia { get; set; }
        public string Inscricao { get; set; }

        public string CpfSelececao { get; set; }
        public int IdResponsavel1 { get; set; }
        public int IdResponsavel2 { get; set; }
    }
}