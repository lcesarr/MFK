﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class SeletivaViewModels
    {
        public SeletivaViewModels()
        {
            Local = new Comum.EnderecoModels { Tipo = "C" };
        }

        public int Id { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Evento")]
        [DataType(DataType.Text)]
        [Display(Name = "Evento")]
        public int IdEvento { get; set; }
        public int IdEndereco { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Descrição")]
        [DataType(DataType.Text)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Regulamento")]
        [DataType(DataType.Text)]
        [Display(Name = "Regulamento")]
        public int IdRegulamento { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Data inicio")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data inicio")]
        public DateTime? DataInicio { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Data fim")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data fim")]
        public DateTime? DataFim { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Valor da inscrição")]
        [DataType(DataType.Currency)]
        [Display(Name = "Valor da inscrição")]
        public decimal Valor { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Quantidade de inscrição por hora")]
        [DataType(DataType.Currency)]
        [Display(Name = "Quantidade de inscrição por hora")]
        public int QtdInscricaoHorario { get; set; }

        public string JuradosSelect { get; set; }

        public int Status { get; set; }
        public string UrlImagem { get; set; }
        public int IntervaloApresentacao { get; set; }
        public string Url { get; set; }

        public EnderecoModels Local { get; set; }

        public EventoModels DadosEvento { get; set; }

        public RegulamentoViewModels Regulamento { get; set; }
    }
}