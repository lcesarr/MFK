﻿using MKF.Web.Models.Comum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MKF.Web.Models
{
    public class LojistaViewModels
    {
        public List<LojistaModel> Lojistas { get; set; }
    }

    public class LojistaModel
    {
        public LojistaModel()
        {
            Empresa = new Comum.EmpresaModels { Tipo = "L" };
            Enderecos = new List<EnderecoModels>();
            Enderecos.Add(new EnderecoModels { });
            //Contatos = new List<ContatoModels>();
            //Contatos.Add(new Comum.ContatosViewModels());
            Socios = new List<PessoaModels>();
            Socios.Add(new PessoaModels { });
            Imagens = new List<ImagemModels>();
            Imagens.Add(new Comum.ImagemModels { });
            RedesSociais = new List<RedeSocialModels>();
            RedesSociais.Add(new Comum.RedeSocialModels { });
        }

        public string Url { get; set; }

        public EmpresaModels Empresa { get; set; }
        public List<EnderecoModels> Enderecos { get; set; }
        //public virtual List<ContatosViewModels> Contatos { get; set; }
        public virtual List<PessoaModels> Socios { get; set; }
        public virtual List<ImagemModels> Imagens { get; set; }
        public virtual List<RedeSocialModels> RedesSociais { get; set; }
        //public virtual List<Modelo> Criancas { get; set; }
    }
}