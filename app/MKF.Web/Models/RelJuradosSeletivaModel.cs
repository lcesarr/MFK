﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class RelJuradosSeletivaModel
    {        
        public int Id { get; set; }

        public int IdSeletiva { get; set; }

        public int IdPessoa { get; set; }
    }
}