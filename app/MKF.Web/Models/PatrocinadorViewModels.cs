﻿using MKF.Web.Models.Comum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MKF.Web.Models
{
    public class ParceiroViewModels
    {
        public List<ParceiroModel> Parceiros { get; set; }
    }

    public class ParceiroModel
    {
        public ParceiroModel()
        {
            Empresa = new Comum.EmpresaModels { Tipo = "L" };
            Enderecos = new List<EnderecoModels>();
            Enderecos.Add(new EnderecoModels { });
            Contatos = new List<ContatoModels>();
            Contatos.Add(new ContatoModels());
            Socios = new List<ResponsavelModels>();
            Socios.Add(new Comum.ResponsavelModels { });
            Imagens = new List<ImagemModels>();
            Imagens.Add(new Comum.ImagemModels { });
            RedesSociais = new List<RedeSocialModels>();
            RedesSociais.Add(new Comum.RedeSocialModels { });

        }

        public EmpresaModels Empresa { get; set; }
        public List<EnderecoModels> Enderecos { get; set; }
        public virtual List<ContatoModels> Contatos { get; set; }
        public virtual List<ResponsavelModels> Socios { get; set; }
        public virtual List<ImagemModels> Imagens { get; set; }
        public virtual List<RedeSocialModels> RedesSociais { get; set; }
        //public virtual List<Modelo> Criancas { get; set; }
    }
}