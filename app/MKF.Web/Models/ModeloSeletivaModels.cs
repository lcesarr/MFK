﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class ModeloSeletivaModels
    {
        public int Id { get; set; }
        public int IdModelo { get; set; }
        public int IdEmpresa { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Nome artístico")]
        [Display(Name = "Nome artístico")]
        public string NomeArtistico { get; set; }
        public int IdCrianca { get; set; }
        public int IdMae { get; set; }
        public int IdPai { get; set; }
        public int Status { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Horário desfile")]
        [Display(Name = "Horário desfile")]
        public DateTime HorarioDesfile { get; set; }
        public string Inscricao { get; set; }
        public int Idade { get; set; }

        public string UrlImagem { get; set; }
        public int? Avaliacao { get; set; }
        public decimal? MediaAvaliacao { get; set; }

        public SeletivaViewModels Seletiva { get; set; }
        public ManequimViewModels Manequim { get; set; }
        public PagamentoViewModels Pagamento { get; set; }
        public List<PagamentoViewModels> Pagamentos { get; set; }
        public PessoaModels Crianca { get; set; }
        public PessoaModels Responsavel1 { get; set; }
        public PessoaModels Responsavel2 { get; set; }
        public List<AvaliacaoModel> Avaliacoes { get; set; }
    }
}