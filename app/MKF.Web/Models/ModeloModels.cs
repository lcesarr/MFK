﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class ModeloModels
    {
        public int? Id { get; set; }
        public int? IdEmpresa { get; set; }
        public string NomeArtistico { get; set; }
        public int? IdCrianca { get; set; }
        public int? IdMae { get; set; }
        public int? IdPai { get; set; }
        public int? Status { get; set; }
        public DateTime? HorarioDesfile { get; set; }
        public string Inscricao { get; set; }
        public int? Avaliacao { get; set; }
        public decimal? MediaAvaliacao { get; set; }

        public PessoaModels Crianca { get; set; }
        public PessoaModels Mae { get; set; }
        public PessoaModels Pai { get; set; }
        public ManequimViewModels Manequins { get; set; }
    }
}