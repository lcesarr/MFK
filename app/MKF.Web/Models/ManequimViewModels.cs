﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class ManequimViewModels
    {
        public int Id { get; set; }
        public int IdModelo { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Altura")]
        [Display(Name = "Altura")]
        public decimal? Altura { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Peso")]
        [Display(Name = "Peso")]
        public decimal? Peso { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Manequim")]
        [Display(Name = "Manequim")]
        public string Tamanho { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Calçado")]
        [Display(Name = "Calçado")]
        public int? Calcado { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Cor da pele")]
        [Display(Name = "Cor da pele")]
        public string CorPele { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Cor do olho")]
        [Display(Name = "Cor do olho")]
        public string CorOlho { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Cor do cabelo")]
        [Display(Name = "Cor do cabelo")]
        public string CorCabelo { get; set; }
        [Display(Name = "Agenciado?")]
        public bool Agenciado { get; set; }
        [Display(Name = "Agência")]
        public string Agencia { get; set; }
        [Display(Name = "Informação adicional")]
        public string Resumo { get; set; }
        public int Status { get; set; }
    }
}