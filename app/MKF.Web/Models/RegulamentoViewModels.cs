﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MKF.Web.Models
{
    public class RegulamentoViewModels
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Favor informe o campo Dados")]
        [DataType(DataType.Text)]
        [Display(Name = "Dados")]
        [AllowHtml]
        public string Dados { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Tipo")]
        [DataType(DataType.Text)]
        [Display(Name = "Tipo")]
        public string Tipo { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Status")]
        [Display(Name = "Status")]
        public int? Status { get; set; }

        public string Url { get; set; }
    }
}