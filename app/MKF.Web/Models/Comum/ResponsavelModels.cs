﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Comum
{
    public class ResponsavelModels
    {
        public int Id { get; set; }
        public int Posicao { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Cpf")]
        [DataType(DataType.Text)]
        [Display(Name = "Cpf")]
        public string Cpf { get; set; }

        public string IdEmpresa { get; set; }
    }
}