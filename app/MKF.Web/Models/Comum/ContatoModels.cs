﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Comum
{
    public class ContatoModels
    {
        public int Id { get; set; }

        public int IdPessoa { get; set; }

        public int IdEmpresa { get; set; }

        public string Tipo { get; set; }

        public string Dado { get; set; }
    }
}