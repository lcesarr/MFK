﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Comum
{
    public class PessoaModels
    {
        public PessoaModels()
        {
            Endereco = new Comum.EnderecoModels();
        }

        public int? Id { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome completo")]
        public string Nome { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "CPF")]
        public string Cpf { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "RG")]
        public string Rg { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Certidão de nascimento")]
        public string Certidao { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Sexo")]
        public string Sexo { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Parentesco")]
        public string Parentesco { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Data de Nascimento")]
        public DateTime? DataNascimento { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Instagram")]
        public string Instagram { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Nacionalidade")]
        public string Nacionalidade { get; set; }

        public string Tipo { get; set; }
        public int Status { get; set; }

        public int? IdEmpresa { get; set; }
        public string Celular { get; set; }

        public string Telefone { get; set; }

        public EmpresaModels DadosEmpresa { get; set; }
        public EnderecoModels Endereco { get; set; }
        public List<ImagemModels> Imagens { get; set; }
        public List<ContatoModels> Contatos { get; set; }
    }
}