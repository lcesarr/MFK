﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Comum
{
    public class RedeSocialModels
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Url { get; set; }
        public int IdEmpresa { get; set; }
        public int IdPessoa { get; set; }
    }
}