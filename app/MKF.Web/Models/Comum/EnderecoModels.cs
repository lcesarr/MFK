﻿using System.ComponentModel.DataAnnotations;

namespace MKF.Web.Models.Comum
{
    /// <summary>
    /// 
    /// </summary>
    public class EnderecoModels
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }

        public int? IdPessoa { get; set; }

        public int? IdEmpresa { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Logradouro")]
        [DataType(DataType.Text)]
        [Display(Name = "Logradouro")]
        public string Logradouro { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Numero")]
        [DataType(DataType.Text)]
        [Display(Name = "Numero")]
        public string Numero { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Complemento")]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Bairro")]
        [DataType(DataType.Text)]
        [Display(Name = "Bairro")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Cidade")]
        [DataType(DataType.Text)]
        [Display(Name = "Cidade")]
        public string Cidade { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Cep")]
        [DataType(DataType.Text)]
        [Display(Name = "Cep")]
        public string Cep { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Estado")]
        [DataType(DataType.Text)]
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        public string Tipo { get; set; }

        [Display(Name = "Local")]
        public string LocalSeletiva { get; set; }
    }
}