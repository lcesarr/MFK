﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Comum
{
    public class EmpresaModels
    {
        public int? Id { get; set; }

        public string CNPJ { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Razão Social")]
        [DataType(DataType.Text)]
        [Display(Name = "Razão Social")]
        public string RazaoSocial { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome Fantasia")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome Fantasia")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "Nome fantasia tem que ter no minimo 20 caracteres e no máximo 100 caracteres")]
        public string NomeFantasia { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Tipo de empresa")]
        [DataType(DataType.Text)]
        public string Tipo { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Ramo de Atividade")]
        [DataType(DataType.Text)]
        [Display(Name = "Ramo de Atividade")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "Ramo de Atividade tem que ter no minimo 10 caracteres e no máximo 50 caracteres")]
        public string RamoAtividade { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Resumo")]
        [DataType(DataType.Text)]
        [Display(Name = "Resumo")]
        public string Resumo { get; set; }

        public string Observacao { get; set; }

        public string Url { get; set; }

        public virtual EnderecoModels Endereco { get; set; }
        public virtual PessoaModels Responsavel { get; set; }
        public virtual List<ContatoModels> Contatos { get; set; }
    }
}