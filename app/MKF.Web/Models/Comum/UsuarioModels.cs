﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Comum
{
    public class UsuarioModels
    {
        public int? Id { get; set; }
        public int? IdPessoa { get; set; }
        public int? IdEmpresa { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Usuário")]
        [DataType(DataType.Text)]
        [Display(Name = "Usuário")]
        public string User { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Senha")]
        [DataType(DataType.Text)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        public string Senha { get; set; }

        public int? Status { get; set; }
        public string TipoAcesso { get; set; }

        public string CodigoAcesso { get; set; }

        public PessoaModels DadosPessoais { get; set; }

        public string Url { get; set; }
    }
}