﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class AvaliacaoModel
    {
        public int Id { get; set; }
        public int IdModelo { get; set; }
        public string Item { get; set; }
        public int Nota { get; set; }
        public DateTime DataAvaliacao { get; set; }
        public int IdJurado { get; set; }
    }
}