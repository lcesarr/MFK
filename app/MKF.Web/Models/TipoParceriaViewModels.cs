﻿using System.ComponentModel.DataAnnotations;

namespace MKF.Web.Models
{
    public class TipoParceriaViewModels
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Status")]
        [DataType(DataType.Text)]
        [Display(Name = "Status")]
        public int Status { get; set; }

        public string Url { get; set; }
    }
}