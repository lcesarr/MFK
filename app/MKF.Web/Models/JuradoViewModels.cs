﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class JuradoViewModels
    {
        public List<JuradoModels> Jurados { get; set; }
        public JuradoModels Jurado { get; set; }
    }

    public class JuradoModels
    {
        public JuradoModels()
        {
            Usuario = new UsuarioModels { TipoAcesso = "J", DadosPessoais = new Comum.PessoaModels { Tipo = "J", Endereco = new EnderecoModels { Tipo = "R" } } };
            Contatos = new List<ContatoModels>();
        }

        public string Url { get; set; }
        public UsuarioModels Usuario { get; set; }
        public List<ContatoModels> Contatos { get; set; }
    }
}