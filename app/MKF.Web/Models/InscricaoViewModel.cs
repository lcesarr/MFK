﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models
{
    public class InscricaoViewModel
    {
        public InscricaoViewModel()
        {
            Modelo = new Models.ModeloViewModels { Autorizacao = new RegulamentoViewModels() };
            Regulamento = new Models.RegulamentoViewModels();
            Pagamento = new PagamentoViewModels();
        }

        public SeletivaViewModels Seletiva { get; set; }
        public ModeloViewModels Modelo { get; set; }
        public RegulamentoViewModels Regulamento { get; set; }

        public string DeAcordoRegulamento { get; set; }
        public PagamentoViewModels Pagamento { get; set; }
        public string Looks { get; set; }        
    }
}