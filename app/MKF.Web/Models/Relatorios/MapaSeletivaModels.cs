﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MKF.Web.Models.Relatorios
{
    public class MapaSeletivaModels
    {
        public DateTime HorarioDesfile { get; set; }
        public string NomeArtistico { get; set; }
        public string Nome { get; set; }
        public string Tamanho { get; set; }
        public decimal Altura { get; set; }
        public decimal Peso { get; set; }
        public int Calcado { get; set; }
        public string CorPele { get; set; }
        public string CorOlho { get; set; }
        public string CorCabelo { get; set; }
        public int Agenciado { get; set; }
        public string Agencia { get; set; }
        public string Resumo { get; set; }
        public string UrlImagem { get; set; }
        public int Idade { get; set; }

        public string Resp1 { get; set; }
        public string Resp2 { get; set; }
        public string TipoPagamento { get; set; }
    }
}