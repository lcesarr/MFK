﻿using MKF.Web.Models.Comum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MKF.Web.Models
{
    public class EventoViewModels
    {
        public List<EventoModels> Eventos = new List<EventoModels>();

        public EventoModels Evento { get; set; }
    }

    public class EventoModels
    {
        public EventoModels()
        {
            Local = new Comum.EnderecoModels { Tipo = "C" };
        }

        public int Id { get; set; }
        public string Url { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Nome")]
        [DataType(DataType.Text)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Edição")]
        [DataType(DataType.Text)]
        [Display(Name = "Edição")]
        public string Edicao { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Descrição")]
        [DataType(DataType.Text)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Data inicio")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data inicio")]
        public DateTime? DataInicio { get; set; }

        [Required(ErrorMessage = "Favor informe o campo Data fim")]
        [DataType(DataType.DateTime)]
        [Display(Name = "Data fim")]
        public DateTime? DataFim { get; set; }

        [Display(Name = "Folder")]
        public Byte[] Imagem { get; set; }

        [Display(Name = "Status")]
        public int Status { get; set; }

        [Display(Name = "Telefone")]
        public string Telefone { get; set; }

        [Display(Name = "Celular")]
        public string Celular { get; set; }

        [Display(Name = "Facebook")]
        public string Facebook { get; set; }

        [Display(Name = "Twitter")]
        public string Twitter { get; set; }

        [Display(Name = "Instagram")]
        public string Instagram { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Imagem")]
        public string ImagemUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdEndereco { get; set; }

        public EnderecoModels Local { get; set; }
        public JuradoViewModels Jurados { get; set; }
        public EventoModels DadosEvento { get; set; }
        public ImagemModels TermoAutorizacao { get; set; }

        public RegulamentoViewModels Regulamento { get; set; }
        public List<SeletivaViewModels> Seletivas { get; set; }
    }
}