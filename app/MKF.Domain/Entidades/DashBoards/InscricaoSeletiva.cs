﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.DashBoards
{
    public class InscricaoSeletiva
    {
        public List<StatusPgInscricaoSeletiva> StatusPagamento { get; set; }
        public List<TipoPgInscricaoSeletiva> TipoPagamento { get; set; }
    }
    public class StatusPgInscricaoSeletiva
    {
        public int IdEvento { get; set; }
        public int IdSeletiva { get; set; }
        public string NomeEvento { get; set; }
        public string NomeSeletiva { get; set; }
        public DateTime DataFim { get; set; }
        public decimal Valor { get; set; }
        public int AguardandoPagamento { get; set; }
        public int EmAnalise { get; set; }
        public int Pagas { get; set; }
        public int NaoConfirmadas { get; set; }
        public int Total { get; set; }
    }

    public class TipoPgInscricaoSeletiva
    {
        public int IdEvento { get; set; }
        public int IdSeletiva { get; set; }
        public string NomeEvento { get; set; }
        public string NomeSeletiva { get; set; }
        public DateTime DataFim { get; set; }
        public decimal Valor { get; set; }
        public int AgCartao { get; set; }
        public int AgBoleto { get; set; }
        public int AgOutros { get; set; }
        public int PgCartao { get; set; }
        public int PgBoleto { get; set; }
        public int PgOutros { get; set; }
        public int EaCartao { get; set; }
        public int EaBoleto { get; set; }
        public int EaOutros { get; set; }
        public int CaCartao { get; set; }
        public int CaBoleto { get; set; }
        public int CaOutros { get; set; }
        public int Total { get; set; }
    }
}
