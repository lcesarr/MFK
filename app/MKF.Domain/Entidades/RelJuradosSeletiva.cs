﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class RelJuradosSeletiva
    {
        public int Id { get; set; }
        public int IdSeletiva { get; set; }
        public int IdPessoa { get; set; }
    }
}
