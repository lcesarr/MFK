﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Patrocinador
    {
        public int Id { get; set; }
        public Empresa Dados { get; set; }
        public decimal Valor { get; set; }
        public int Tipo { get; set; }
        public int Status { get; set; }
        public int FormaPagamento { get; set; }
    }
}
