﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Usuario
    {
        public int? Id { get; set; }
        public int? IdPessoa { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int? Status { get; set; }
        public string TipoAcesso { get; set; }
        public string CodigoAcesso { get; set; }

        public Pessoa DadosPessoais { get; set; }
    }
}
