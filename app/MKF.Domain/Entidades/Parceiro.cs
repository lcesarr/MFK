﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Parceiro
    {
        public int Id { get; set; }
        public Empresa Dados { get; set; }
        public string Tipo { get; set; }
    }
}
