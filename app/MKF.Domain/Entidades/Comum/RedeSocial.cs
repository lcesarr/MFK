﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Comum
{
    public class RedeSocial
    {
        public int Id { get; set; }
        public int Tipo { get; set; }
        public string Url { get; set; }
        public int IdEmpresa { get; set; }
        public int IdPessoa { get; set; }
    }
}
