﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Comum
{
    public class Imagem
    {
        public int? Id { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public int? Altura { get; set; }
        public int? Largura { get; set; }
        public string URL { get; set; }
        public int? IdPessoa { get; set; }
        public int? IdEmpresa { get; set; }
    }
}
