﻿using MKF.Domain.Entidades.InscricaoSeletiva;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Comum
{
    public class Pessoa
    {
        public Pessoa()
        {
            List<Endereco> Enderecos = new List<Endereco>();
            List<Contato> Contatos = new List<Contato>();
            List<RedeSocial> RedesSociais = new List<RedeSocial>();
            List<Imagem> Imagens = new List<Comum.Imagem>();
        }

        public int? Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Rg { get; set; }
        public string Certidao { get; set; }
        public string Sexo { get; set; }
        public string Parentesco { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Tipo { get; set; }
        public int? IdEmpresa { get; set; }
        public string Celular { get; set; }
        public string Nacionalidade { get; set; }
        public string Email { get; set; }

        public Usuario Usuario { get; set; }
        public Endereco Endereco { get; set; }
        public List<Contato> Contatos { get; set; }
        public List<RedeSocial> RedesSociais { get; set; }
        public List<Imagem> Imagens { get; set; }
        public Empresa DadosEmpresa { get; set; }
        public List<Avaliacao> Avaliacoes { get; set; }
    }
}
