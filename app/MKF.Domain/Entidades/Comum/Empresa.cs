﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Comum
{
    public class Empresa
    {
        public Empresa()
        {
            Endereco Enderecos = new Endereco();
            List<Contato> Contatos = new List<Contato>();
            Pessoa Responsavel = new Pessoa();
            List<Imagem> Imagens = new List<Imagem>();
            List<RedeSocial> RedesSociais = new List<RedeSocial>();
            List<Modelo> Criancas = new List<Modelo>();
        }

        public int? Id { get; set; }
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Tipo { get; set; }
        public string RamoAtividade { get; set; }
        public string Resumo { get; set; }
        public string Observacao { get; set; }

        public virtual Endereco Endereco { get; set; }
        public virtual List<Contato> Contatos { get; set; }
        public virtual Pessoa Responsavel { get; set; }
        public virtual List<Imagem> Imagens { get; set; }
        public virtual List<RedeSocial> RedesSociais { get; set; }
        public virtual List<Modelo> Criancas { get; set; }
        //public List<Ingresso> Ingressos { get; set; }
    }
}
