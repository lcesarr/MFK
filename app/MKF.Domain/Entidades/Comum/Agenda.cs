﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Comum
{
    public class Agenda
    {
        public int Id { get; set; }
        public int IdEvento { get; set; }
        public int IdEmpresa { get; set; }
        public int TotalLook { get; set; }
        public int TotalTroca { get; set; }
        public DateTime DataHora { get; set; }
        public int Tempo { get; set; }
        public int TempoEntrada { get; set; }
        public int Ordem { get; set; }

        public Evento Evento { get; set; }
        public Empresa Lojista { get; set; }
    }
}
