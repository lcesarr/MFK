﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Comum
{
    public class Manequim
    {
        public int Id { get; set; }
        public int IdModelo { get; set; }
        public string Tamanho { get; set; }
        public decimal Altura { get; set; }
        public decimal Peso { get; set; }
        public int Calcado { get; set; }
        public string CorPele { get; set; }
        public string CorOlho { get; set; }
        public string CorCabelo { get; set; }
        public bool Agenciado { get; set; }
        public string Agencia { get; set; }
        public string Resumo { get; set; }
        public int Status { get; set; }

        public Modelo Crianca { get; set; }
    }
}
