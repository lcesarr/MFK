﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class TipoParceria
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Status { get; set; }
    }
}
