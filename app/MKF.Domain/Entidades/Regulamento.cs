﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Regulamento
    {
        public int? Id { get; set; }
        public string Dados { get; set; }
        public string Tipo { get; set; }
        public string Nome { get; set; }
        public int? Status { get; set; }
    }
}
