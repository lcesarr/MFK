﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Seletiva
    {
        public int Id { get; set; }
        public int IdEvento { get; set; }
        public int IdEndereco { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int IdRegulamento { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int Status { get; set; }
        public decimal Valor { get; set; }
        public int QtdInscricaoHorario { get; set; }
        public string UrlImagem { get; set; }
        public int IntervaloApresentacao { get; set; }

        public Endereco Local { get; set; }
        public Evento DadosEvento { get; set; }
        public List<Modelo> Criancas { get; set; }
        public List<Pessoa> Jurados { get; set; }
    }
}
