﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Pagamento
    {
        public int? Id { get; set; }
        public int? IdPessoa { get; set; }
        public int? IdEvento { get; set; }
        public int? IdSeletiva { get; set; }
        public decimal? ValorPagamento { get; set; }
        public string TipoPagamento { get; set; }
        public string Status { get; set; }
        public DateTime? DataPagamento { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public string CodigoTransacaoPG { get; set; }
        public string CodigoReferencia { get; set; }
        public string Inscricao { get; set; }
    }
}
