﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.InscricaoSeletiva
{
    public class ResultadoSeletiva
    {
        public int Id { get; set; }
        public string NomeArtistico { get; set; }
        public string Inscricao { get; set; }
        public int Avaliacao { get; set; }
        public string StatusModelo { get; set; }
        public decimal MediaAvaliacao { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public int Idade { get; set; }
        public string Resp1 { get; set; }
        public string Resp2 { get; set; }
        public string Tamanho { get; set; }
        public decimal Altura { get; set; }
        public decimal Peso { get; set; }
        public int Calcado { get; set; }
        public string CorPele { get; set; }
        public string CorCabelo { get; set; }
        public string CorOlho { get; set; }
        public string Agencia { get; set; }
        public string Resumo { get; set; }
        public string UrlImagem { get; set; }
        public int NotaFotografia { get; set; }
        public List<Pessoa> Jurados { get; set; }
    }
}
