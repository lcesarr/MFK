﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.InscricaoSeletiva
{
    public class ModeloSeletiva
    {
        public int Id { get; set; }
        public int IdEmpresa { get; set; }
        public string NomeArtistico { get; set; }
        public int IdCrianca { get; set; }
        public int IdMae { get; set; }
        public int IdPai { get; set; }
        public int Status { get; set; }
        public DateTime HorarioDesfile { get; set; }
        public string Inscricao { get; set; }
        public int? Avaliacao { get; set; }
        public decimal? MediaAvaliacao { get; set; }

        public Manequim Manequim { get; set; }
        public List<Pagamento> Pagamentos { get; set; }
        public PessoaSeletiva Crianca { get; set; }
        public PessoaSeletiva Responsavel1 { get; set; }
        public PessoaSeletiva Responsavel2 { get; set; }
    }
}
