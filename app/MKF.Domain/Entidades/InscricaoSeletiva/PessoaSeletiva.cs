﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.InscricaoSeletiva
{
    public class PessoaSeletiva
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Rg { get; set; }
        public string Certidao { get; set; }
        public string Sexo { get; set; }
        public string Parentesco { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Tipo { get; set; }
        public int IdEmpresa { get; set; }
        public string Celular { get; set; }
        public string Nacionalidade { get; set; }
        public string Email { get; set; }

        public Endereco Endereco { get; set; }
        public List<Imagem> Imagem { get; set; }
        public RedeSocial Redesocial { get; set; }
    }
}
