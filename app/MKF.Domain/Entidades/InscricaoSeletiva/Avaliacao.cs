﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.InscricaoSeletiva
{
    public class Avaliacao
    {
        public int Id { get; set; }
        public int IdModelo { get; set; }
        public string Item { get; set; }
        public int Nota { get; set; }
        public DateTime DataAvaliacao { get; set; }
        public int IdJurado { get; set; }

        public Pessoa Jurado { get; set; }
    }
}
