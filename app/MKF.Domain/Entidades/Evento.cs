﻿using MKF.Domain.Entidades.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Evento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Edicao { get; set; }
        public Endereco Local { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int Status { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string URLImagem { get; set; }
        public int IdEndereco { get; set; }

        public List<Agenda> Participantes { get; set; }
        public List<Ingresso> Ingressos { get; set; }
        public List<Seletiva> Seletivas { get; set; }
        public List<Empresa> Lojistas { get; set; }
        public List<Empresa> Patrocinadores { get; set; }
        public List<Empresa> Parceiros { get; set; }
    }
}
