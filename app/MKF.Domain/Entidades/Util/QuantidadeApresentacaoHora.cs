﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Util
{
    public class QuantidadeApresentacaoHora
    {
        public int IdEvento { get; set; }
        public DateTime HorarioDesfile { get; set; }
        public int Total { get; set; }
    }
}
