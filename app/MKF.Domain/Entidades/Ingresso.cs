﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Ingresso
    {
        public int Id { get; set; }
        public int IdEvento { get; set; }
        public string Tipo { get; set; }
        public decimal Valor { get; set; }
        public int Quantidade { get; set; }
        public int Status { get; set; }
    }
}
