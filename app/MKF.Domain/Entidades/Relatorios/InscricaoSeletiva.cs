﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Relatorios
{
    public class InscricaoSeletiva
    {
        public int IdCrianca { get; set; }
        public string Inscricao { get; set; }
        public string NomeCrianca { get; set; }
        public int Idade { get; set; }
        public string NomeResp1 { get; set; }
        public string NomeResp2 { get; set; }
        public string StatusPagamento { get; set; }
        public string TipoPagamento { get; set; }
        public string CodigoReferencia { get; set; }
        public string UrlImagem { get; set; }
        public int IdPagamento { get; set; }
        public int StatusModelo { get; set; }
        public int Avaliacao { get; set; }
        public decimal MediaAvaliacao { get; set; }
        public int Notas { get; set; }
    }
}
