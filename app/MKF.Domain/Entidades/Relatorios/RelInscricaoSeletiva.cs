﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades.Relatorios
{
    public class RelInscricaoSeletiva
    {
        public int IdModelo { get; set; }
        public string NomeEvento { get; set; }
        public string NomeSeletiva { get; set; }
        public string NomePessoa { get; set; }
        public DateTime HorarioDesfile { get; set; }
        public string Inscricao { get; set; }
        public string TipoPagamento { get; set; }
        public string Status { get; set; }
        public string CodigoTransacaoPG { get; set; }
        public int IdPagamento { get; set; }
    }
}
