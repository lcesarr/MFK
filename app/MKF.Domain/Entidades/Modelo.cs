﻿using MKF.Domain.Entidades.Comum;
using MKF.Domain.Entidades.InscricaoSeletiva;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Domain.Entidades
{
    public class Modelo
    {
        public int? Id { get; set; }
        public int? IdEmpresa { get; set; }
        public string NomeArtistico { get; set; }
        public int? IdCrianca { get; set; }
        public int? IdMae { get; set; }
        public int? IdPai { get; set; }
        public int? Status { get; set; }
        public DateTime? HorarioDesfile { get; set; }
        public string Inscricao { get; set; }
        public int? Avaliacao { get; set; }
        public decimal? MediaAvaliacao { get; set; }

        public Pessoa Crianca { get; set; }
        public Pessoa Mae { get; set; }
        public Pessoa Pai { get; set; }
        public Manequim Manequins { get; set; }
        public List<Avaliacao> Avaliacoes { get; set; }
    }
}
