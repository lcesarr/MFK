﻿using Dapper;
using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Relatorios;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace MKF.Data.Repository
{
    public class ModeloRepository : RepositoryBase<Modelo>
    {
        string inscricaoSeletiva = @"select m.Id as IdModelo, m.Inscricao, c.Id as IdCrianca, c.Nome as NomeCrianca, db_a37cab_mfk.FNC_IDADE(c.DataNascimento) as Idade, ru.Nome as NomeResp1, rd.Nome as NomeResp2, pg.Status as StatusPagamento, pg.TipoPagamento, pg.CodigoReferencia as CodigoReferencia, (select im.Url from db_a37cab_mfk.imagem im where im.IdPessoa = c.Id and im.Tipo = '0') as UrlImagem, pg.Id as IdPagamento, m.Status as StatusModelo, m.Avaliacao, m.MediaAvaliacao,
                                        (select sum(av.Nota) from db_a37cab_mfk.avaliacao av where av.IdJurado = @idJurados and av.IdModelo = m.Id) as Notas
                                        from db_a37cab_mfk.modelo m
                                        inner join db_a37cab_mfk.pessoa c on c.Id = m.IdCrianca
                                        inner join db_a37cab_mfk.pessoa ru on ru.Id = m.IdMae
                                        left join db_a37cab_mfk.pessoa rd on rd.Id = m.IdPai
                                        left join db_a37cab_mfk.pagamento pg on pg.Inscricao = m.Inscricao
                                        left join db_a37cab_mfk.seletiva s on s.Id = pg.IdSeletiva
                                        left join db_a37cab_mfk.manequim ma on ma.IdModelo = m.Id
                                        where pg.Status in ('3', '4', '8')";

        public virtual IEnumerable<InscricaoSeletiva> GetByModeloSeletiva(string nomeCrianca, string nomeResp1, string nomeResp2, int idSeletiva, string dtSeltiva, int idJurados)
        {
            string condicao = string.Empty;

            if (!nomeCrianca.Equals("null") && !string.IsNullOrEmpty(nomeCrianca)) condicao += " and c.Nome like '%" + nomeCrianca + "%'";
            if (!nomeResp1.Equals("null") && !string.IsNullOrEmpty(nomeResp1)) condicao += " and ru.Nome like '%" + nomeResp1 + "%'";
            if (!nomeResp2.Equals("null") && !string.IsNullOrEmpty(nomeResp2)) condicao += " and rd.Nome like '%" + nomeResp2 + "%'";
            if (idSeletiva > 0) condicao += " and s.Id = " + idSeletiva;
            if (!dtSeltiva.Equals("null")) condicao += " and m.HorarioDesfile = '" + DateTime.ParseExact(dtSeltiva, "yyyyMMddHHmmss", null).ToString("yyyy-MM-dd HH:mm:ss") + "'";

            inscricaoSeletiva += condicao + " order by m.HorarioDesfile, m.Status, c.Nome";

            IEnumerable<InscricaoSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<InscricaoSeletiva>(inscricaoSeletiva, new { idJurados = idJurados });
            }

            return r;
        }

        public virtual bool AtualizaStatus(int idModelo, int status)
        {
            int r = 0;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Execute("update db_a37cab_mfk.modelo set Status = @status where Id = @idModelo;", new { idModelo = idModelo, status = status }, null);
            }

            return r > 0;
        }
    }
}
