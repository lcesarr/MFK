﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MKF.Data.Repository.Base
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetBy(TEntity obj);

        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        int Insert(TEntity entity);

        bool Update(TEntity entity);

        bool Delete(int id);

        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate);
    }
}
