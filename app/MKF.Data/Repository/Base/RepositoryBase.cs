﻿using Dapper;
using Dommel;
using Microsoft.Win32.SafeHandles;
using MKF.Data.Infra;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.InteropServices;

namespace MKF.Data.Repository.Base
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public virtual bool Delete(int id)
        {
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                var entity = GetById(id);

                if (entity == null) throw new Exception("Registro não encontrado");

                return db.Delete(entity);
            }
        }

        public virtual IEnumerable<TEntity> GetBy(TEntity obj)
        {
            var q = Util.MontarQuery<TEntity>(obj);
            IEnumerable<TEntity> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<TEntity>(q, null);
            }

            return r;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                return db.GetAll<TEntity>();
            }
        }

        public virtual TEntity GetById(int id)
        {
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                return db.Get<TEntity>(id);
            }
        }

        public virtual IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public virtual int Insert(TEntity entity)
        {
            int id = 0;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                var ret = db.Insert(entity);
                id = int.Parse(ret.ToString());
            }

            return id;
        }

        public virtual bool Update(TEntity entity)
        {
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                return db.Update(entity);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
                handle.Dispose();

            disposed = true;
        }
    }
}
