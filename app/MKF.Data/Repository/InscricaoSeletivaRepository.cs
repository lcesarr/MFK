﻿using Dapper;
using MKF.Data.Infra;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using MKF.Domain.Entidades.InscricaoSeletiva;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MKF.Data.Repository
{
    public class InscricaoSeletivaRepository
    {
        string query = @"select m.*, pc.*, ec.*, cm.*, rs.*, ci.*, pg.* from db_a37cab_mfk.modelo m
                            inner join db_a37cab_mfk.pessoa pc on pc.Id = m.IdCrianca
                            left join db_a37cab_mfk.endereco ec on ec.IdPessoa = pc.Id
                            left join db_a37cab_mfk.manequim cm on cm.IdModelo = m.Id
                            left join db_a37cab_mfk.redesocial rs on rs.IdPessoa = pc.Id
                            left join db_a37cab_mfk.imagem ci on ci.IdPessoa = pc.Id
                            left join db_a37cab_mfk.pagamento pg on pg.Inscricao = m.Inscricao
                            where m.IdCrianca = @idCrianca";

        string queryAvaliado = @"select distinct b.*, ps.*, a.* from (select m.Id, m.NomeArtistico, m.Inscricao, m.Avaliacao, m.Status as StatusModelo, m.MediaAvaliacao, p.Nome, p.DataNascimento, db_a37cab_mfk.fnc_idade(p.DataNascimento) as Idade,
                                (select Nome from db_a37cab_mfk.pessoa where Id = m.IdMae) as Resp1, (select Nome from db_a37cab_mfk.pessoa where Id = m.IdPai) as Resp2,
                                pg.IdSeletiva, ma.Tamanho, ma.Altura, ma.Peso, ma.Calcado, ma.CorPele, ma.CorCabelo, ma.CorOlho, ma.Agencia, ma.Resumo, p.Sexo,
                                (select im.Url from db_a37cab_mfk.imagem im where im.IdPessoa = p.Id and im.Tipo = 0) as UrlImagem
                                 from db_a37cab_mfk.modelo m
                                inner join db_a37cab_mfk.pessoa p on p.Id = m.IdCrianca
                                inner join db_a37cab_mfk.pagamento pg on pg.Inscricao = m.Inscricao
                                left join db_a37cab_mfk.manequim ma on ma.IdModelo = m.Id) b, db_a37cab_mfk.avaliacao a, db_a37cab_mfk.pessoa ps
                                where a.IdModelo = b.Id
                                  and ps.Id = a.IdJurado";

        public virtual ModeloSeletiva GetByModeloInscricaoSeletiva(int idCrianca)
        {
            ModeloSeletiva r = null;
            var lookup = new Dictionary<int, ModeloSeletiva>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<ModeloSeletiva, PessoaSeletiva, Endereco, Manequim, RedeSocial, Imagem, Pagamento, ModeloSeletiva>(query, map: (m, c, e, ma, rs, i, p) =>
                {
                    if (!lookup.TryGetValue(m.Id, out r))
                    {
                        lookup.Add(m.Id, r = m);
                    }

                    if (r.Crianca == null)
                        r.Crianca = c;

                    if (r.Crianca.Endereco == null)
                        r.Crianca.Endereco = e;

                    if (r.Manequim == null)
                        r.Manequim = ma;

                    if (r.Crianca.Redesocial == null)
                        r.Crianca.Redesocial = rs;

                    if (r.Crianca.Imagem == null)
                        r.Crianca.Imagem = new List<Imagem>();

                    if (i != null)
                    {
                        if (r.Crianca.Imagem.Where(a => a.Id == i.Id).FirstOrDefault() == null)
                            r.Crianca.Imagem.Add(i);
                    }

                    if (r.Pagamentos == null)
                        r.Pagamentos = new List<Pagamento>();

                    if (p != null)
                    {
                        if (r.Pagamentos.Where(a => a.Id == p.Id).FirstOrDefault() == null)
                            r.Pagamentos.Add(p);
                    }

                    return r;
                }, param: new { idCrianca = idCrianca }).FirstOrDefault();
            }

            return r;
        }

        public virtual IEnumerable<ResultadoSeletiva> GetByModeloAvaliadaSeletiva(string nomeCrianca, string nomeResp1, string nomeResp2, int idSeletiva, string inscricao, int idadeMin, int idadeMax, string tamanho, double altura, double peso, int calcado, string corCabelo, string sexo)
        {
            string condicao = string.Empty;

            if (!nomeCrianca.Equals("null") && !string.IsNullOrEmpty(nomeCrianca)) condicao += " and b.Nome like '%" + nomeCrianca + "%'";
            if (!nomeResp1.Equals("null") && !string.IsNullOrEmpty(nomeResp1)) condicao += " and b.Resp1 like '%" + nomeResp1 + "%'";
            if (!nomeResp2.Equals("null") && !string.IsNullOrEmpty(nomeResp2)) condicao += " and b.Resp2 like '%" + nomeResp2 + "%'";
            if (idSeletiva > 0) condicao += " and b.IdSeletiva = " + idSeletiva;
            if (!inscricao.Equals("null") && !string.IsNullOrEmpty(inscricao)) condicao += " and b.Inscricao = '" + inscricao + "'";
            if (idadeMin > 0 || idadeMax > 0) condicao += " and b.Idade between " + idadeMin + " and " + idadeMax;
            if (!tamanho.Equals("null") && !string.IsNullOrEmpty(tamanho)) condicao += " and b.Tamanho = '" + tamanho + "'";
            if (altura > 0) condicao += " and b.Altura = " + altura.ToString(new CultureInfo("en-US"));
            if (peso > 0) condicao += " and b.Peso = " + peso.ToString(new CultureInfo("en-US"));
            if (calcado > 0) condicao += " and b.Calcado = " + calcado;
            if (!corCabelo.Equals("null") && !string.IsNullOrEmpty(corCabelo)) condicao += " and b.CorCabelo = '" + corCabelo + "'";
            if (!sexo.Equals("null") && !string.IsNullOrEmpty(sexo)) condicao += " and b.Sexo = '" + sexo + "'";

            queryAvaliado += condicao + " order by b.Id";
            IEnumerable<ResultadoSeletiva> r = null;
            ResultadoSeletiva rs = null;

            var lookup = new Dictionary<int, ResultadoSeletiva>();
            var sum = 0;
            bool novo = false;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<ResultadoSeletiva, Pessoa, Avaliacao, ResultadoSeletiva>(queryAvaliado, map: (b, ps, a) =>
                {
                    if (!lookup.TryGetValue(b.Id, out rs))
                    {
                        novo = true;
                        lookup.Add(b.Id, rs = b);
                    }
                    else
                        novo = false;

                    if (rs.Jurados == null)
                        rs.Jurados = new List<Pessoa>();

                    if (ps != null)
                    {
                        var jur = rs.Jurados.Where(j => j.Id == ps.Id).FirstOrDefault();
                        if (jur == null)
                        {
                            var p = ps;
                            if (p.Avaliacoes == null)
                                p.Avaliacoes = new List<Avaliacao>();

                            p.Avaliacoes.Add(a);
                            rs.Jurados.Add(p);
                            if (novo)
                            {
                                sum = 0;
                                sum += a.Nota;
                            }
                            else sum += a.Nota;

                            if (a.Item == "Fotogenia") rs.NotaFotografia += a.Nota;

                            rs.Avaliacao = sum;
                        }
                        else
                        {
                            if (novo)
                            {
                                sum = 0;
                                sum += a.Nota;
                            }
                            else sum += a.Nota;

                            jur.Avaliacoes.Add(a);
                            rs.Avaliacao = sum;
                            if (a.Item == "Fotogenia") rs.NotaFotografia += a.Nota;
                        }
                    }

                    return rs;
                });
            }

            return lookup.Select(a => a.Value).OrderByDescending(m => m.Avaliacao).ThenByDescending(n => n.NotaFotografia);
        }
    }
}
