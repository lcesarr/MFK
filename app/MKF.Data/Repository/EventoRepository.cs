﻿using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MKF.Data.Repository
{
    public class EventoRepository : RepositoryBase<Evento>
    {
        public virtual Evento GetByUnionId(int id)
        {
            var q = @"select e.*, en.* from evento e left join endereco en on e.IdEndereco = en.Id where e.Id = @id";
            Evento evento = null;

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                evento = db.Query<Evento, Endereco, Evento>(q, map: (e, en) =>
                {
                    e.Local = en;

                    return e;
                },
                    param: new { id = id }).FirstOrDefault();
            }

            return evento;
        }

        public virtual IEnumerable<Evento> GetByEventosSeletiva()
        {
            var q = @"select e.*, s.* from db_a37cab_mfk.evento e, db_a37cab_mfk.seletiva s
                       where e.Id = s.IdEvento
                         and e.Status = 0
                         and s.Status = 0;";

            IEnumerable<Evento> eventos = null;

            Evento ev = null;

            var lookup = new Dictionary<int, Evento>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                eventos = db.Query<Evento, Seletiva, Evento>(q, map: (e, s) =>
                {
                    if (!lookup.TryGetValue(e.Id, out ev))
                    {
                        lookup.Add(e.Id, ev = e);
                    }

                    if (ev.Seletivas == null)
                        ev.Seletivas = new List<Seletiva>();

                    if (ev.Seletivas.Where(m => m.Id == s.Id).FirstOrDefault() == null)
                        ev.Seletivas.Add(s);

                    return e;
                });
            }

            return eventos;
        }
    }
}
