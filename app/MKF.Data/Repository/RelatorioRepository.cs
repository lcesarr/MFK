﻿using Dapper;
using MKF.Data.Infra;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using MKF.Domain.Entidades.Relatorios;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MKF.Data.Repository
{
    public class RelatorioRepository
    {
        string relSeletiva = @"select m.Id as IdModelo, e.Nome as NomeEvento, s.Nome as NomeSeletiva, pe.Nome as NomePessoa, m.HorarioDesfile, 
                                m.Inscricao, p.TipoPagamento, p.Status, p.CodigoTransacaoPG, p.Id as IdPagamento 
                                from db_a37cab_mfk.modelo m  
                                left join db_a37cab_mfk.pagamento p on p.Inscricao = m.Inscricao
                                left join db_a37cab_mfk.pessoa pe on pe.Id = m.IdCrianca
                                left join db_a37cab_mfk.evento e on e.Id = p.IdEvento
                                left join db_a37cab_mfk.seletiva s on s.Id = p.IdSeletiva
                                where 1 = 1";

        string inscricaoSeletiva = @"select m.Inscricao, c.Id as IdCrianca, c.Nome as NomeCrianca, db_a37cab_mfk.FNC_IDADE(c.DataNascimento) as Idade, ru.Nome as NomeResp1, rd.Nome as NomeResp2, pg.Status as StatusPagamento, pg.TipoPagamento, pg.CodigoReferencia as CodigoReferencia, (select im.Url from db_a37cab_mfk.imagem im where im.IdPessoa = c.Id and im.Tipo = '0') as UrlImagem, pg.Id as IdPagamento, m.Status as StatusModelo
                                        from db_a37cab_mfk.modelo m
                                        inner join db_a37cab_mfk.pessoa c on c.Id = m.IdCrianca
                                        inner join db_a37cab_mfk.pessoa ru on ru.Id = m.IdMae
                                        left join db_a37cab_mfk.pessoa rd on rd.Id = m.IdPai
                                        left join db_a37cab_mfk.pagamento pg on pg.Inscricao = m.Inscricao
                                        left join db_a37cab_mfk.seletiva s on s.Id = pg.IdSeletiva
                                        left join db_a37cab_mfk.manequim ma on ma.IdModelo = m.Id";

        string mapaSeletiva = @"select m.HorarioDesfile, m.NomeArtistico, ps.Nome, db_a37cab_mfk.FNC_IDADE(ps.DataNascimento) as Idade, ma.Tamanho, ma.Altura, ma.Peso, ma.Calcado, ma.CorPele, ma.CorOlho, ma.CorCabelo, ma.Agenciado, ma.Agencia, ma.Resumo, (select im.Url from db_a37cab_mfk.imagem im where im.IdPessoa = ps.Id and im.Tipo = '0') as UrlImagem,
                                (select Nome from db_a37cab_mfk.pessoa where Id = m.IdMae) as Resp1, (select Nome from db_a37cab_mfk.pessoa where Id = m.IdPai) as Resp2, p.TipoPagamento
                                  from db_a37cab_mfk.pagamento p
                                 inner join db_a37cab_mfk.modelo m on m.Inscricao = p.Inscricao
                                 inner join db_a37cab_mfk.pessoa ps on ps.Id = m.IdCrianca
                                 left join db_a37cab_mfk.manequim ma on ma.IdModelo = m.Id
                                 where p.Status in ('3', '4', '8')
                                   and p.IdSeletiva = @idSeletiva
                                 order by m.HorarioDesfile, ps.Nome";

        string aprovadosSeletiva = @"select m.HorarioDesfile, m.NomeArtistico, ps.Nome, db_a37cab_mfk.FNC_IDADE(ps.DataNascimento) as Idade, ma.Tamanho, ma.Altura, ma.Peso, ma.Calcado, ma.CorPele, ma.CorOlho, ma.CorCabelo, ma.Agenciado, ma.Agencia, ma.Resumo, (select im.Url from db_a37cab_mfk.imagem im where im.IdPessoa = ps.Id and im.Tipo = '0') as UrlImagem,
                                (select Nome from db_a37cab_mfk.pessoa where Id = m.IdMae) as Resp1, (select Nome from db_a37cab_mfk.pessoa where Id = m.IdPai) as Resp2, p.TipoPagamento, ps.Sexo
                                  from db_a37cab_mfk.pagamento p
                                 inner join db_a37cab_mfk.modelo m on m.Inscricao = p.Inscricao
                                 inner join db_a37cab_mfk.pessoa ps on ps.Id = m.IdCrianca
                                 left join db_a37cab_mfk.manequim ma on ma.IdModelo = m.Id
                                 where m.Status = '3'
                                   and p.IdSeletiva = @idSeletiva
                                   and db_a37cab_mfk.FNC_IDADE(ps.DataNascimento) between @idademin and @idademax
                                 order by ps.Nome, Idade";

        string aprovadosSeletivaPdf = @"select m.*, pc.*, pm.*, ma.*, pp.*, im.* from db_a37cab_mfk.modelo m
                                        inner join db_a37cab_mfk.pessoa pc on pc.Id = m.IdCrianca
                                        inner join db_a37cab_mfk.pessoa pm on pm.Id = m.IdMae
                                        inner join db_a37cab_mfk.manequim ma on ma.IdModelo = m.Id
                                        inner join db_a37cab_mfk.pagamento pg on pg.Inscricao = m.Inscricao
                                        left join db_a37cab_mfk.pessoa pp on pp.Id = m.IdPai
                                        left join db_a37cab_mfk.imagem im on im.IdPessoa = pc.Id and im.Tipo = '0'
                                        where m.Status = '3'
                                          and pg.IdSeletiva = @idSeletiva
                                          and db_a37cab_mfk.FNC_IDADE(pc.DataNascimento) between @idademin and @idademax
                                        order by db_a37cab_mfk.FNC_IDADE(pc.DataNascimento), pc.Nome";

        public virtual IEnumerable<RelInscricaoSeletiva> GetByInscSeletiva(string nome, string inscricao, int evento, int seletiva, string status)
        {
            if (!nome.Equals("null")) relSeletiva += " and pe.Nome like '%" + nome + "%'";
            if (!inscricao.Equals("null")) relSeletiva += " and m.Inscricao = '" + inscricao + "'";
            if (evento > 0) relSeletiva += " and e.Id = " + evento;
            if (seletiva > 0) relSeletiva += " and s.Id = " + seletiva;
            if (!status.Equals("null")) relSeletiva += " and p.Status = '" + status + "'";

            relSeletiva += " order by pe.Nome";

            IEnumerable<RelInscricaoSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<RelInscricaoSeletiva>(relSeletiva, null);
            }

            return r;
        }

        public virtual IEnumerable<InscricaoSeletiva> GetByInscricaoSeletiva(string nomeCrianca, string nomeResp1, string nomeResp2, int idSeletiva, string inscricao, int idadeMin, int idadeMax, string codigoReferencia, string statusPagamento, string tipoPagamento, string tamanho, double altura, double peso, int calcado, string corCabelo, string dtSeltiva, string sexo)
        {

            string condicao = string.Empty;

            if (!nomeCrianca.Equals("null") && !string.IsNullOrEmpty(nomeCrianca)) condicao += string.IsNullOrEmpty(condicao) ? " where c.Nome like '%" + nomeCrianca + "%'" : " and c.Nome like '%" + nomeCrianca + "%'";
            if (!nomeResp1.Equals("null") && !string.IsNullOrEmpty(nomeResp1)) condicao += string.IsNullOrEmpty(condicao) ? " where ru.Nome like '%" + nomeResp1 + "%'" : " and ru.Nome like '%" + nomeResp1 + "%'";
            if (!nomeResp2.Equals("null") && !string.IsNullOrEmpty(nomeResp2)) condicao += string.IsNullOrEmpty(condicao) ? " where rd.Nome like '%" + nomeResp2 + "%'" : " and rd.Nome like '%" + nomeResp2 + "%'";
            if (idSeletiva > 0) condicao += string.IsNullOrEmpty(condicao) ? " where s.Id = " + idSeletiva : " and s.Id = " + idSeletiva;
            if (!inscricao.Equals("null") && !string.IsNullOrEmpty(inscricao)) condicao += string.IsNullOrEmpty(condicao) ? " where m.Inscricao = '" + inscricao + "'" : " and m.Inscricao = '" + inscricao + "'";
            if (idadeMin > 0 || idadeMax > 0) condicao += string.IsNullOrEmpty(condicao) ? " where db_a37cab_mfk.FNC_IDADE(c.DataNascimento) between " + idadeMin + " and " + idadeMax : " and db_a37cab_mfk.FNC_IDADE(c.DataNascimento) between " + idadeMin + " and " + idadeMax;
            if (!codigoReferencia.Equals("null") && !string.IsNullOrEmpty(codigoReferencia)) condicao += string.IsNullOrEmpty(condicao) ? " where pg.CodigoReferencia = '" + codigoReferencia + "'" : " and pg.CodigoReferencia = '" + codigoReferencia + "'";
            if (!statusPagamento.Equals("null") && !string.IsNullOrEmpty(statusPagamento))
            {
                if (statusPagamento.Equals("3") || statusPagamento.Equals("4"))
                    condicao += string.IsNullOrEmpty(condicao) ? " where pg.Status in ('3', '4')" : " and pg.Status in ('3', '4')";
                else
                    condicao += string.IsNullOrEmpty(condicao) ? " where pg.Status = '" + statusPagamento + "'" : " and pg.Status = '" + statusPagamento + "'";
            }

            if (!tipoPagamento.Equals("null") && !string.IsNullOrEmpty(tipoPagamento)) condicao += string.IsNullOrEmpty(condicao) ? " where pg.TipoPagamento = '" + tipoPagamento + "'" : " and pg.TipoPagamento = '" + tipoPagamento + "'";
            if (!tamanho.Equals("null") && !string.IsNullOrEmpty(tamanho)) condicao += string.IsNullOrEmpty(condicao) ? " where ma.Tamanho = '" + tamanho + "'" : " and ma.Tamanho = '" + tamanho + "'";
            if (altura > 0) condicao += string.IsNullOrEmpty(condicao) ? " where ma.Altura = " + altura.ToString(new CultureInfo("en-US")) + "'" : " and ma.Altura = " + altura.ToString(new CultureInfo("en-US"));
            if (peso > 0) condicao += string.IsNullOrEmpty(condicao) ? " where ma.Peso = " + peso.ToString(new CultureInfo("en-US")) + "'" : " and ma.Peso = " + peso.ToString(new CultureInfo("en-US"));
            if (calcado > 0) condicao += string.IsNullOrEmpty(condicao) ? " where ma.Calcado = " + calcado + "'" : " and ma.Calcado = " + calcado;
            if (!corCabelo.Equals("null") && !string.IsNullOrEmpty(corCabelo)) condicao += string.IsNullOrEmpty(condicao) ? " where ma.CorCabelo = '" + corCabelo + "'" : " and ma.CorCabelo = '" + corCabelo + "'";
            if (!dtSeltiva.Equals("null")) condicao += string.IsNullOrEmpty(condicao) ? " where m.HorarioDesfile = '" + DateTime.ParseExact(dtSeltiva, "yyyyMMddHHmmss", null).ToString("yyyy-MM-dd HH:mm:ss") + "'" : " and m.HorarioDesfile = '" + DateTime.ParseExact(dtSeltiva, "yyyyMMddHHmmss", null).ToString("yyyy-MM-dd HH:mm:ss") + "'";
            if (!sexo.Equals("null") && !string.IsNullOrEmpty(sexo)) condicao += string.IsNullOrEmpty(condicao) ? " where c.Sexo = '" + sexo + "'" : " and c.Sexo = '" + sexo + "'";

            //inscricaoSeletiva += condicao + " order by db_a37cab_mfk.FNC_IDADE(c.DataNascimento)";

            inscricaoSeletiva += condicao + " order by c.Nome";

            IEnumerable<InscricaoSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<InscricaoSeletiva>(inscricaoSeletiva, null);
            }

            return r;
        }

        public virtual IEnumerable<MapaSeletiva> GetByMapaSeletiva(int idSeletiva)
        {

            IEnumerable<MapaSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<MapaSeletiva>(mapaSeletiva, new { idSeletiva = idSeletiva });
            }

            return r;
        }

        public virtual IEnumerable<MapaSeletiva> GetByAprovadosSeletiva(int idSeletiva, int idademin, int idademax)
        {

            IEnumerable<MapaSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<MapaSeletiva>(aprovadosSeletiva, new { idSeletiva = idSeletiva, idademin = idademin, idademax = idademax }).OrderBy(i => i.Idade);
            }

            return r;
        }

        public virtual IEnumerable<Modelo> GetByAprovadosSeletivaPdf(int idSeletiva, int idademin, int idademax)
        {
            IEnumerable<Modelo> r = null;
            Modelo mod = null;

            var lookup = new Dictionary<int, Modelo>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<Modelo, Pessoa, Pessoa, Manequim, Pessoa, Imagem, Modelo>(aprovadosSeletivaPdf, map: (m, pc, pm, ma, pp, im) =>
                {
                    if (!lookup.TryGetValue(m.Id.Value, out mod))
                    {
                        lookup.Add(m.Id.Value, mod = m);
                    }

                    if (mod.Crianca == null)
                    {
                        mod.Crianca = pc;
                        if (im != null)
                        {
                            if (mod.Crianca.Imagens == null)
                                mod.Crianca.Imagens = new List<Imagem>();

                            mod.Crianca.Imagens.Add(im);
                        }
                    }

                    if (mod.Mae == null)
                        mod.Mae = pm;

                    if (mod.Manequins == null)
                        mod.Manequins = ma;

                    if (mod.Pai == null)
                        mod.Pai = pp;

                    return mod;
                }, param: new { idSeletiva = idSeletiva, idademin = idademin, idademax = idademax }, splitOn: "Id");
            }

            return r;
        }
    }
}
