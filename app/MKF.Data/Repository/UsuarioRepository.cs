﻿using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using Dapper;
using MKF.Domain.Entidades.Comum;
using System.Linq;

namespace MKF.Data.Repository
{
    public class UsuarioRepository : RepositoryBase<Usuario>
    {
        public virtual Usuario GetByUnionId(int id)
        {
            var q = @"select u.*, p.*, e.* from usuario u left join pessoa p on p.Id = u.IdPessoa left join empresa e on e.Id = p.IdEmpresa
                      where u.Id = @id";
            Usuario r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<Usuario, Pessoa, Empresa, Usuario>(q, map: (u, p, e) =>
                    {
                        u.DadosPessoais = p;
                        u.DadosPessoais.DadosEmpresa = e;
                        return u;
                    },
                    param: new { id = id }).FirstOrDefault();

            }

            return r;
        }

        public virtual IEnumerable<Usuario> GetByUnionAll()
        {
            var q = @"select u.*, p.*, e.* from usuario u left join pessoa p on p.Id = u.IdPessoa left join empresa e on e.Id = p.IdEmpresa";
            IEnumerable<Usuario> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<Usuario, Pessoa, Empresa, Usuario>(q, map: (u, p, e) =>
                {
                    if (p != null)
                        u.DadosPessoais = p;
                    if (e != null)
                        u.DadosPessoais.DadosEmpresa = e;
                    return u;
                });
            }

            return r;
        }

        public IEnumerable<Usuario> GetUserBy(string nome, string cpf, string user)
        {
            var q = @"select u.*, p.* from db_a37cab_mfk.usuario u, db_a37cab_mfk.pessoa p
                    where u.IdPessoa = p.Id";

            if (!string.IsNullOrEmpty(user) && !user.Equals("null")) q += " and u.User like '%" + user + "%'";
            if (!string.IsNullOrEmpty(cpf) && !cpf.Equals("null")) q += " and p.Cpf = '" + cpf + "'";
            if (!string.IsNullOrEmpty(nome) && !nome.Equals("null")) q += " and p.Nome like '%" + nome + "%'";

            IEnumerable<Usuario> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<Usuario, Pessoa, Usuario>(q, map: (u, p) =>
                {
                    u.DadosPessoais = p;
                    return u;
                });
            }

            return r;
        }

        public Usuario GetUserJurados(int id)
        {
            var q = @"select u.*, p.*, e.*, c.* from db_a37cab_mfk.pessoa p
                        inner join db_a37cab_mfk.endereco e on e.IdPessoa = p.Id
                        left join db_a37cab_mfk.contato c on c.IdPessoa = p.Id
                        left join db_a37cab_mfk.usuario u on u.IdPessoa = p.Id
                        where p.Tipo = 'J'
                          and p.id = " + id;

            IEnumerable<Usuario> r = null;

            Usuario us = null;

            var lookup = new Dictionary<int, Usuario>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<Usuario, Pessoa, Endereco, Contato, Usuario>(q, map: (u, p, e, c) =>
                {
                    if (!u.Id.HasValue)
                        u = new Usuario { Id = 0, Status = 0, TipoAcesso = "J", CodigoAcesso = "002", IdPessoa = p.Id };

                    if (!lookup.TryGetValue(u.Id.Value, out us))
                    {
                        lookup.Add(u.Id.Value, us = u);
                    }

                    if (us.DadosPessoais == null)
                        us.DadosPessoais = p;

                    if (us.DadosPessoais.Endereco == null)
                        us.DadosPessoais.Endereco = e;

                    if (us.DadosPessoais.Contatos == null)
                        us.DadosPessoais.Contatos = new List<Contato>();

                    if (us.DadosPessoais.Contatos.Where(m => m.Id == c.Id).FirstOrDefault() == null)
                        us.DadosPessoais.Contatos.Add(c);

                    return u;
                });

                return r.FirstOrDefault();
            }
        }
    }
}