﻿using Dapper;
using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades.Comum;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;

namespace MKF.Data.Repository
{
    public class EmpresaRepository : RepositoryBase<Empresa>
    {
        public virtual Empresa GetByUnionId(int id)
        {
            var q = @"select e.*, en.*, p.*, c.* from empresa e left join endereco en on e.Id = en.IdEmpresa left join pessoa p on p.IdEmpresa = e.Id left join contato c on c.IdEmpresa = e.Id
                    where e.Id = @id";
            Empresa empresa = null;

            var lookup = new Dictionary<int, Empresa>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                empresa = db.Query<Empresa, Endereco, Pessoa, Contato, Empresa>(q, map: (e, en, p, c) =>
                {
                    if (!lookup.TryGetValue(e.Id.Value, out empresa))
                    {
                        lookup.Add(e.Id.Value, empresa = e);
                    }

                    if (empresa.Endereco == null)
                        empresa.Endereco = en;

                    if (empresa.Responsavel == null)
                        empresa.Responsavel = p;

                    if (empresa.Contatos == null)
                        empresa.Contatos = new List<Contato>();

                    if (empresa.Contatos.Where(m => m.Id == c.Id).FirstOrDefault() == null)
                        empresa.Contatos.Add(c);

                    return e;
                },
                    param: new { id = id }).FirstOrDefault();
            }

            return empresa;
        }

        public virtual IEnumerable<Empresa> GetByUnionAll()
        {
            var q = @"select e.*, en.*, p.*, c.* from empresa e left join endereco en on e.Id = en.IdEmpresa left join pessoa p on p.IdEmpresa = e.Id left join contato c on c.IdEmpresa = e.Id";
            IEnumerable<Empresa> empresa = null;

            Empresa emp = null;

            var lookup = new Dictionary<int, Empresa>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                empresa = db.Query<Empresa, Endereco, Pessoa, Contato, Empresa>(q, map: (e, en, p, c) =>
                {
                    if (!lookup.TryGetValue(e.Id.Value, out emp))
                    {
                        lookup.Add(e.Id.Value, emp = e);
                    }

                    if (emp.Endereco == null)
                        emp.Endereco = en;

                    if (emp.Responsavel == null)
                        emp.Responsavel = p;

                    if (emp.Contatos == null)
                        emp.Contatos = new List<Contato>();

                    if (emp.Contatos.Where(m => m.Id == c.Id).FirstOrDefault() == null)
                        emp.Contatos.Add(c);

                    return e.Endereco != null && e.Responsavel != null ? e : null;
                });
            }

            return empresa;
        }
    }
}
