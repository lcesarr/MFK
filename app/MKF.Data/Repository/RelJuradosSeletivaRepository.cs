﻿using MKF.Data.Infra;
using MKF.Domain.Entidades;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Repository
{
    public class RelJuradosSeletivaRepository : RepositoryBase<RelJuradosSeletiva>
    {
        string juradosSeletiva = @"select * from db_a37cab_mfk.rel_seletiva_jurado s
                                    where s.IdSeletiva = @idSeletiva";

        string seletivaJurados = @"select s.*, e.*, en.* from db_a37cab_mfk.rel_seletiva_jurado r, db_a37cab_mfk.seletiva s, db_a37cab_mfk.evento e, db_a37cab_mfk.endereco en
                                    where r.IdPessoa = @idJurados
                                      and r.IdSeletiva = s.Id
                                      and e.Id = s.IdEvento
                                      and en.Id = s.IdEndereco;";

        public virtual IEnumerable<RelJuradosSeletiva> GetByJuradosSeletiva(int idSeletiva)
        {
            IEnumerable<RelJuradosSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<RelJuradosSeletiva>(juradosSeletiva, new { IdSeletiva = idSeletiva }, null);
            }

            return r;
        }

        public virtual int DeleteJuradosSeletiva(int idSeletiva)
        {
            int r = 0;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Execute("delete from db_a37cab_mfk.rel_seletiva_jurado where IdSeletiva = @idSeletiva;", new { idSeletiva = idSeletiva }, null);
            }

            return r;
        }

        public virtual IEnumerable<Seletiva> GetBySeletivaJurados(int idJurados)
        {
            IEnumerable<Seletiva> r = null;

            Seletiva sel = null;

            var lookup = new Dictionary<int, Seletiva>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<Seletiva, Evento, Endereco, Seletiva>(seletivaJurados, map: (s, e, en) =>
                {
                    if (!lookup.TryGetValue(s.Id, out sel))
                    {
                        lookup.Add(s.Id, sel = s);
                    }

                    if (sel.DadosEvento == null)
                        sel.DadosEvento = e;

                    if (sel.Local == null)
                        sel.Local = en;

                    return sel;
                }, param: new { IdJurados = idJurados });

                return lookup.Select(a => a.Value);
            }
        }
    }
}
