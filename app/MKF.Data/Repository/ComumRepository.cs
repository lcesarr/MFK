﻿using MKF.Domain.Entidades.Util;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MKF.Data.Infra;

namespace MKF.Data.Repository
{
    public class ComumRepository
    {
        string validarInsclicao = @"SELECT p.IdSeletiva, p.IdEvento, HorarioDesfile, count(0) as Total FROM db_a37cab_mfk.modelo m, db_a37cab_mfk.pagamento p
                                            where (m.IdMae = p.IdPessoa or m.IdPai = p.IdPessoa)
                                              and p.Status in ('1', '3', '2')
                                              and p.IdSeletiva = {0}
                                              and p.IdEvento = {1}
                                            group by p.IdSeletiva, p.IdEvento, HorarioDesfile;";

        public virtual IEnumerable<QuantidadeApresentacaoHora> GetByInscricaoHora(int idSeletiva, int idEvento)
        {
            IEnumerable<QuantidadeApresentacaoHora> r = null;

            validarInsclicao = string.Format(validarInsclicao, idSeletiva, idEvento);
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<QuantidadeApresentacaoHora>(validarInsclicao, null);
            }

            return r;
        }
    }
}
