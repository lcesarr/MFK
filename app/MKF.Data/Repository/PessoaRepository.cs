﻿using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades.Comum;
using MySql.Data.MySqlClient;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace MKF.Data.Repository
{
    public class PessoaRepository : RepositoryBase<Pessoa>
    {
        public virtual Pessoa GetByUnionId(int id)
        {
            var q = @"select p.*, e.* from db_a37cab_mfk.pessoa p
                        left join db_a37cab_mfk.endereco e on e.IdPessoa = p.Id
                      where p.Id = @id";
            Pessoa pessoa = null;

            var lookup = new Dictionary<int, Pessoa>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                pessoa = db.Query<Pessoa, Endereco, Pessoa>(q, map: (p, en) =>
                {
                    if (!lookup.TryGetValue(p.Id.Value, out pessoa))
                    {
                        lookup.Add(p.Id.Value, pessoa = p);
                    }

                    if (pessoa.Endereco == null)
                        pessoa.Endereco = en;                    

                    return p;
                },
                    param: new { id = id }).FirstOrDefault();
            }

            return pessoa;
        }

        public virtual bool DeleteRelSeletivaPessoa(int idSeletiva)
        {
            int ret = 0;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                db.Execute("DELETE FROM rel_seletiva_jurado WHERE IdSeletiva=@idSeletiva", new { idSeletiva = idSeletiva });
                return ret > 0 ? true : false;
            }
        }

        public virtual bool IncluirRelSeletivaPessoa(int idSeletiva, int idPessoa)
        {
            int ret = 0;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                db.Execute("insert into rel_seletiva_jurado values(@idSeletiva, @idPessos)", new { idSeletiva = idSeletiva, idPessos = idPessoa });
                return ret > 0 ? true : false;
            }
        }
    }
}
