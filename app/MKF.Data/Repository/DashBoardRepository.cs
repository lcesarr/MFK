﻿using Dapper;
using MKF.Data.Infra;
using MKF.Domain.Entidades.DashBoards;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace MKF.Data.Repository
{
    public class DashBoardRepository
    {
        string dash1 = @"select a.IdEvento, a.IdSeletiva, a.NomeEvento, a.NomeSeletiva, a.DataFim, a.Valor,
	                           sum(case when a.status = '1' then a.Total else 0 end) as 'AguardandoPagamento',
                               sum(case when a.status = '2' then a.Total else 0 end) as 'EmAnalise',
	                           sum(case when a.status in ('3', '4', '8') then a.Total else 0 end) as 'Pagas',
                               sum(case when a.status not in ('3', '4', '1', '2', '8')  then a.Total else 0 end) as 'NaoConfirmadas',
                               sum(a.Total) as 'Total' 
                               from (select p.IdEvento, e.Nome as NomeEvento, p.IdSeletiva, s.Nome as NomeSeletiva, s.DataFim, s.Valor, p.Status, count(0) as Total from db_a37cab_mfk.pagamento p, db_a37cab_mfk.evento e, db_a37cab_mfk.seletiva s
				                        where p.IdEvento = e.Id
				                          and s.IdEvento = e.Id
                                          and s.DataFim > sysdate()-7
			                         group by p.IdEvento, e.Nome, p.IdSeletiva, s.Nome, s.DataFim, s.Valor, p.Status
                                     ) as a
                        group by a.IdEvento, a.IdSeletiva, a.NomeEvento, a.NomeSeletiva, a.DataFim, a.Valor";

        string dash2 = @"select a.IdEvento, a.IdSeletiva, a.NomeEvento, a.NomeSeletiva, a.DataFim, a.Valor,
	                           sum(case when a.status = '1' and a.TipoPagamento = '1' then a.Total else 0 end) as 'AgCartao',
                               sum(case when a.status = '1' and a.TipoPagamento = '2' then a.Total else 0 end) as 'AgBoleto',
                               sum(case when a.status = '1' and a.TipoPagamento not in ('1', '2') then a.Total else 0 end) as 'AgOutros',
                               sum(case when a.status in ('3', '4') and a.TipoPagamento = '1' then a.Total else 0 end) as 'PgCartao',
                               sum(case when a.status in ('3', '4') and a.TipoPagamento = '2' then a.Total else 0 end) as 'PgBoleto',
                               sum(case when a.status in ('3', '4') and a.TipoPagamento not in ('1', '2') then a.Total else 0 end) as 'PgOutros',
                               sum(case when a.status = '2' and a.TipoPagamento = '1' then a.Total else 0 end) as 'EaCartao',
                               sum(case when a.status = '2' and a.TipoPagamento = '2' then a.Total else 0 end) as 'EaBoleto',
                               sum(case when a.status = '2' and a.TipoPagamento not in ('1', '2') then a.Total else 0 end) as 'EaOutros',
	                           sum(case when a.status not in ('1', '2', '3', '4', '8') and a.TipoPagamento = '1' then a.Total else 0 end) as 'CaCartao',
                               sum(case when a.status not in ('1', '2', '3', '4', '8') and a.TipoPagamento = '2' then a.Total else 0 end) as 'CaBoleto',
                               sum(case when a.status not in ('1', '2', '3', '4', '8') and a.TipoPagamento not in ('1', '2') then a.Total else 0 end) as 'CaOutros',                               
                               sum(a.Total) as 'Total' 
                               from (select p.IdEvento, e.Nome as NomeEvento, p.IdSeletiva, s.Nome as NomeSeletiva, s.DataFim, s.Valor, p.Status, p.TipoPagamento, count(0) as Total from db_a37cab_mfk.pagamento p, db_a37cab_mfk.evento e, db_a37cab_mfk.seletiva s
				                        where p.IdEvento = e.Id
				                          and s.IdEvento = e.Id
                                          and s.DataFim > sysdate()-7
			                         group by p.IdEvento, e.Nome, p.IdSeletiva, s.Nome, s.DataFim, s.Valor, p.Status, p.TipoPagamento
                                     ) as a                            
                        group by a.IdEvento, a.IdSeletiva, a.NomeEvento, a.NomeSeletiva, a.DataFim, a.Valor;";

        public virtual IEnumerable<StatusPgInscricaoSeletiva> GetByDashSeletiva()
        {
            IEnumerable<StatusPgInscricaoSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<StatusPgInscricaoSeletiva>(dash1, null);
            }

            return r;
        }

        public virtual IEnumerable<TipoPgInscricaoSeletiva> GetByDashSeletivaTpPg()
        {
            IEnumerable<TipoPgInscricaoSeletiva> r = null;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Query<TipoPgInscricaoSeletiva>(dash2, null);
            }

            return r;
        }
    }
}
