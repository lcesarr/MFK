﻿using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades.InscricaoSeletiva;
using MySql.Data.MySqlClient;
using Dapper;
using System.Collections.Generic;

namespace MKF.Data.Repository
{
    public class AvaliacaoRepository : RepositoryBase<Avaliacao>
    {
        public virtual int DeleteAvaliacaoModelo(int idModelo, int idJurado)
        {
            int r = 0;
            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                r = db.Execute("delete from db_a37cab_mfk.avaliacao where IdModelo = @idModelo and IdJurado = @idJurado;", new { idModelo = idModelo, idJurado = idJurado }, null);
            }

            return r;
        }

        public virtual IEnumerable<Avaliacao> GetByAvaliacaoModelo(int idModelo)
        {
            var q = @"select * from avaliacao where IdModelo = @idModelo";
            IEnumerable<Avaliacao> avaliacao = null;

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                avaliacao = db.Query<Avaliacao>(q, new { idModelo = idModelo });
            }

            return avaliacao;
        }

        public virtual IEnumerable<Avaliacao> GetByAvaliacaoJuradoModelo(int idModelo, int idJurado)
        {
            var q = @"select * from avaliacao where IdModelo = @idModelo and IdJurado = @idJurado";
            IEnumerable<Avaliacao> avaliacao = null;

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                avaliacao = db.Query<Avaliacao>(q, new { idModelo = idModelo, idJurado = idJurado });
            }

            return avaliacao;
        }
    }
}
