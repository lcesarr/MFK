﻿using Dapper;
using MKF.Data.Infra;
using MKF.Data.Repository.Base;
using MKF.Domain.Entidades;
using MKF.Domain.Entidades.Comum;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;

namespace MKF.Data.Repository
{
    public class SeletivaRepository : RepositoryBase<Seletiva>
    {
        public virtual Seletiva GetByUnionId(int id)
        {
            var q = @"select s.*, en.*, p.*, ev.* from seletiva s left join endereco en on en.Id = s.IdEndereco left join rel_seletiva_jurado j on j.IdSeletiva = s.Id left join pessoa p on p.Id = j.IdPessoa inner join evento ev on ev.Id = s.IdEvento where s.Id = @id";
            Seletiva seletiva = null;

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                seletiva = db.Query<Seletiva, Endereco, Pessoa, Evento, Seletiva>(q, map: (e, en, p, ev) =>
                {
                    e.Local = en;
                    e.DadosEvento = ev;
                    return e;
                },
                    param: new { id = id }).FirstOrDefault();
            }

            return seletiva;
        }

        public virtual IEnumerable<Seletiva> GetByUnionAll()
        {
            var q = @"select s.*, en.*, ev.* from seletiva s left join endereco en on en.Id = s.IdEndereco inner join evento ev on ev.Id = s.IdEvento";
            IEnumerable<Seletiva> seletiva = null;

            Seletiva sel = null;

            var lookup = new Dictionary<int, Seletiva>();

            using (var db = new MySqlConnection(LeitorConfiguracao.ConectionString()))
            {
                seletiva = db.Query<Seletiva, Endereco, Evento, Seletiva>(q, map: (s, en, ev) =>
                {
                    if (!lookup.TryGetValue(s.Id, out sel))
                    {
                        lookup.Add(s.Id, sel = s);
                    }

                    if (sel.Local == null)
                        sel.Local = en;

                    if (sel.DadosEvento == null)
                        sel.DadosEvento = ev;

                    return sel;
                });
            }

            return seletiva;
        }
    }
}
