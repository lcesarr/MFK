﻿using Dapper.FluentMap;
using Dapper.FluentMap.Dommel;
using MKF.Data.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Data
{
    public class RegisterMappings
    {
        public static void Register()
        {
            FluentMapper.Initialize(config =>
            {
                config.AddMap(new AgendaMap());
                config.AddMap(new ContatoMap());
                config.AddMap(new EmpresaMap());
                config.AddMap(new EnderecoMap());
                config.AddMap(new EventoMap());
                config.AddMap(new ImagemMap());
                config.AddMap(new IngressoMap());
                config.AddMap(new ManequimMap());
                config.AddMap(new ModeloMap());
                config.AddMap(new ParceiroMap());
                config.AddMap(new PatrocinadorMap());
                config.AddMap(new PessoaMap());
                config.AddMap(new RedeSocialMap());
                config.AddMap(new SeletivaMap());
                config.AddMap(new UsuarioMap());
                config.AddMap(new TipoParceriaMap());
                config.AddMap(new TipoPatrocinioMap());
                config.AddMap(new RegulamentoMap());
                config.AddMap(new PagamentoMap());
                config.AddMap(new RelJuradosSeletivaMap());
                config.AddMap(new AvaliacaoMap());
                config.ForDommel();
            });
        }
    }
}
