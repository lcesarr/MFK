﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace MKF.Data.Infra
{
    public static class Util
    {
        public static string MontarQuery<T>(T obj)
        {
            var lista = new List<string>();
            var p = obj.GetType().GetProperties();

            int conta = 0;

            foreach (PropertyInfo pop in p)
            {
                var valor = pop.GetValue(obj, null);
                if (valor != null)
                {
                    if (pop.PropertyType.Name.Equals("Guid"))
                    {
                        if (!valor.Equals(default(Guid)))
                            lista.Add(string.Format("{0} = '{1}'", pop.Name, valor.ToString()));
                    }
                    else if (pop.PropertyType.Name.Equals("String"))
                        lista.Add(string.Format("{0} like '%{1}%'", pop.Name, valor.ToString()));
                    else if (pop.PropertyType.Name.Equals("TimeSpan"))
                    {
                        if (!valor.Equals(default(TimeSpan)))
                            lista.Add(string.Format("{0} = {1}", pop.Name, valor.ToString()));
                    }
                    else if (pop.PropertyType.Name.Equals("DateTime"))
                    {
                        if (!valor.Equals(default(DateTime)))
                            lista.Add(string.Format("{0} = {1}", pop.Name, valor.ToString()));
                    }
                    else
                        lista.Add(string.Format("{0} = {1}", pop.Name, valor.ToString()));
                }
            }

            var condicao = "";

            foreach (var item in lista)
            {
                condicao += conta == 0 ? string.Format("where {0}", item) : string.Format(" and {0}", item);
                conta++;
            }

            return string.Format("select * from {0} {1}", obj.GetType().Name, condicao);
        }
    }
}
