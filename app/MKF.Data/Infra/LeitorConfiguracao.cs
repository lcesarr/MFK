﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Data.Infra
{
    public class LeitorConfiguracao
    {
        public static string ConectionString()
        {
            return ConfigurationManager.ConnectionStrings["RIEntities"].ConnectionString;
        }

        public static string EmailPagSeguro()
        {
            return ConfigurationManager.AppSettings.Get("Email");
        }

        public static string TokenPagSeguro()
        {
            return ConfigurationManager.AppSettings.Get("Token");
        }

        public static string UrlPagSeguro()
        {
            return ConfigurationManager.AppSettings.Get("Url");
        }        
    }
}
