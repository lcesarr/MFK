﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;

namespace MKF.Data.Mappers
{
    public class ModeloMap : DommelEntityMap<Modelo>
    {
        public ModeloMap()
        {
            ToTable("Modelo");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
