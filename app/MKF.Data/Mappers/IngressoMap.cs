﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;

namespace MKF.Data.Mappers
{
    public class IngressoMap : DommelEntityMap<Ingresso>
    {
        public IngressoMap()
        {
            ToTable("Ingresso");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
