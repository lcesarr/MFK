﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class PessoaMap : DommelEntityMap<Pessoa>
    {
        public PessoaMap()
        {
            ToTable("Pessoa");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
