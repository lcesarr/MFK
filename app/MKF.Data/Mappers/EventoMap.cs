﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;

namespace MKF.Data.Mappers
{
    public class EventoMap : DommelEntityMap<Evento>
    {
        public EventoMap()
        {
            ToTable("Evento");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
