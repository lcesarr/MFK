﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class ContatoMap : DommelEntityMap<Contato>
    {
        public ContatoMap()
        {
            ToTable("Contato");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
