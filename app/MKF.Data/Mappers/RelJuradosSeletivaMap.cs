﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Data.Mappers
{
    public class RelJuradosSeletivaMap : DommelEntityMap<RelJuradosSeletiva>
    {
        public RelJuradosSeletivaMap()
        {
            ToTable("rel_seletiva_jurado");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
