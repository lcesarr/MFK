﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class ManequimMap : DommelEntityMap<Manequim>
    {
        public ManequimMap()
        {
            ToTable("Manequim");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
