﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class AgendaMap : DommelEntityMap<Agenda>
    {
        public AgendaMap()
        {
            ToTable("Agenda");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
