﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKF.Data.Mappers
{
    public class PagamentoMap : DommelEntityMap<Pagamento>
    {
        public PagamentoMap()
        {
            ToTable("Pagamento");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
