﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;

namespace MKF.Data.Mappers
{
    public class ParceiroMap : DommelEntityMap<Parceiro>
    {
        public ParceiroMap()
        {
            ToTable("Parceiro");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
