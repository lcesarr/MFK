﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class RedeSocialMap : DommelEntityMap<RedeSocial>
    {
        public RedeSocialMap()
        {
            ToTable("RedeSocial");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
