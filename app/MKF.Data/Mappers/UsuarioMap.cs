﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;

namespace MKF.Data.Mappers
{
    public class UsuarioMap : DommelEntityMap<Usuario>
    {
        public UsuarioMap()
        {
            ToTable("Usuario");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
