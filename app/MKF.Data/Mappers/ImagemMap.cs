﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class ImagemMap : DommelEntityMap<Imagem>
    {
        public ImagemMap()
        {
            ToTable("Imagem");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
