﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class EmpresaMap : DommelEntityMap<Empresa>
    {
        public EmpresaMap()
        {
            ToTable("Empresa");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
