﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades;

namespace MKF.Data.Mappers
{
    public class SeletivaMap : DommelEntityMap<Seletiva>
    {
        public SeletivaMap()
        {
            ToTable("Seletiva");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
