﻿using Dapper.FluentMap.Dommel.Mapping;
using MKF.Domain.Entidades.Comum;

namespace MKF.Data.Mappers
{
    public class EnderecoMap : DommelEntityMap<Endereco>
    {
        public EnderecoMap()
        {
            ToTable("Endereco");
            Map(x => x.Id).ToColumn("Id").IsKey();
        }
    }
}
